# DiplomskiRad

**Naslov:** Vizualizacija geoprostornih lokacija temeljena na stvarnim podacima

**Sažetak:** U ovome je diplomskom radu opisana vizualizacija geoprostornog područja na temelju javno
dostupnih kartografskih podataka. Programsko rješenje koje prikazuje rad opisanih
postupaka ostvareno je korištenjem pokretača Unity 3D gdje je geoprostorno područje
prikazano je u obliku trodimenzionalne karte. Prikazan je postupak dohvaćanja i obrade
informacija iz četiri javno dostupna izvora podataka. Glavni izvor podataka koji je korišten
je OpenStreetMap te su iz njega raznim opisanim postupcima prikazane ceste, površine,
građevine i ostali 3D objekti u virtualnoj sceni. U svrhu oblikovanja terena dohvaćeni su
podaci o visinama iz izvora Google Maps Elevation API. Radi realističnijeg prikaza grada
Zagreba korišteni su podaci o grmlju, stablima i urbanoj opremi dobiveni iz izvora GIS
Zrinjevac. Na kraju je razmotren utjecaj količine dohvaćenih podataka na vrijeme izvođenja
i frekvenciju osvježavanja izrađenog programskog rješenja.

**Ključne riječi:** vizualizacija podataka, kartografski podaci, virtualno okruženje, Unity 3D,
OpenStreetMap, GIS, GIS Zrinjevac, Google Maps Elevation API 
