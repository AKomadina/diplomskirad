﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    //Građevine sa staklenim ulaznim vratima
    public static HashSet<string> glassDoorBuildings = new HashSet<string>() { "hotel", "government","university", "school", "commercial" , "police", "embassy", "townhall"};

    //Građevine sa duplim ulaznim vratima
    public static HashSet<string> doubleDoorBuildings = new HashSet<string>() { "residential", "apartments", "public" , "train_station", "music_venue", "social_facility", "kindergarten","post_office","theatre" };

    //Građevine sa duplim velikim ulaznim vratima
    public static HashSet<string> bigOldDoorBuildings = new HashSet<string>() { "church" ,"place_of_worship"};



    //Građevine sa različitim fasadama
    public static HashSet<string> facadeBuildings = new HashSet<string>() { "hotel", "government", "university", "school", "commercial", "police", "embassy", "townhall" ,"residential", "apartments", "public" , "train_station", "music_venue", "social_facility", "kindergarten","post_office","theatre"};



    //Zastupljene vrste cesta
    public static HashSet<string> roadOptions = new HashSet<string>() { "motorway", "trunk", "primary", "secondary", "tertiary", "residential", "unclassified", "service", "footway", "path", "road", "living_street", "track", "pedestrian", "primary_link", "secondary_link", "tertiary_link", "cycleway" };

    //Opcije za širinu ceste
    public static Dictionary<string, float> pathWidthOptions = new Dictionary<string, float>() { 
        {"unclassified", 2.75f},
        {"service", 2.5f},
        {"footway", 1f},
        {"path", 0.8f},
        {"road", 2.75f},
        {"living_street", 2.75f},
        {"track", 2f},
        {"pedestrian", 2.5f},
        {"primary_link", 3f},
        {"secondary_link", 2.75f},
        {"tertiary_link", 2.5f},
        {"cycleway", 1f},  
    };

    //Opcije za širinu prometnog traka ceste na temelju OSM podataka o njezinoj vrsti
    public static Dictionary<string, float> roadLanesWidthOptions = new Dictionary<string, float>(){
        {"motorway", 4f},
        {"trunk", 4f},
        {"primary", 3.75f},
        {"secondary", 3.5f},
        {"tertiary", 3.25f},
        {"residential", 3f},

    };

    //Opcije za broj traka svake vrste ceste
    public static Dictionary<string, int> roadLanesCount = new Dictionary<string, int>(){
        {"motorway", 6},
        {"trunk", 4},
        {"primary", 4},
        {"secondary", 4},
        {"tertiary", 2},
        {"residential", 2},
    };


    //Opcije ja vrstu površine ceste na temelju OSM podataka
    public static Dictionary<string, float[]> roadSurfaceOptions = new Dictionary<string, float[]>(){
        {"grass",             new float[]{ 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f } },
        {"asphalt",           new float[]{ 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f } },
        {"paved",             new float[]{ 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f } },
        {"line",              new float[]{ 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f } },
        {"gravel",            new float[]{ 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f } },
        {"fine_gravel",       new float[]{ 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f } },
        {"unpaved",           new float[]{ 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f } },
        {"compacted",         new float[]{ 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f } },
        {"concrete",          new float[]{ 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f } },
        {"ground",            new float[]{ 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f } },
        {"dirt",              new float[]{ 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f } },
        {"sand",              new float[]{ 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f, 0f } },
        {"rubber",            new float[]{ 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f, 0f } },
        {"asphaltResidential",new float[]{ 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 0f } },
        {"cycleway",          new float[]{ 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f } },
        {"sett",              new float[]{ 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f } },
        {"paving_stones",     new float[]{ 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f } },
        {"map",               new float[]{ 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f, 0f } },
        {"yellowLine",        new float[]{ 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 1f } },

    };

    //Koje vrste cesta imaju središnju bijelu liniju
    public static HashSet<string> roadsWithWhiteLine = new HashSet<string>() { "motorway", "trunk", "primary", "secondary", "tertiary", "residential" };

    //Koje vrste cesta imaju graničnu bijelu liniju
    public static HashSet<string> roadsWithWhiteBound = new HashSet<string>() { "motorway", "trunk", "primary", "secondary", "tertiary", "residential", "primary_link", "secondary_link", "tertiary_link", "cycleway" };

    //Koje vrste cesta imaju središnju bijelu liniju
    public static HashSet<string> roadsWithTrafficObjects = new HashSet<string>() { "motorway", "trunk", "primary", "secondary", "tertiary", "residential"};


    //Koje vrste ograda imaju otvor na cesti
    public static HashSet<string> barrierHasGate = new HashSet<string>() { "fence", "wall", "hedge", "retaining_wall" };


    //Raspored trokuta za poligon s 4 točke
    public static int[] quadTrianglesClockwise = new int[]
        {
            0,1,2,
            2,3,0
        };
    public static int[] quadTrianglesCounterClockwise = new int[]
        {
            0,3,2,
            2,1,0
        };

    //Raspored trokuta za poligon s 3 točke
    public static int[] trianglesClockwise = new int[]
        {
            0,1,2,
        };
    public static  int[] trianglesCounterClockwise = new int[]
        {
            2,1,0
        };

    public static List<string> flowerOptions = new List<string> { "tulipan", "narcisa", "šafran", "visibaba", "anemone" };

    public static  List<string> treeTypes = new List<string> { "breza", "platana", "čempres", "kesten", "hrast", "bor", "grab", "javor", "smreka", "jasen", "jablan", "brijest", "lijeska", "tuja", "breskva", "jabuka", "kruška", "klen", "omorika", "cedar", "topola" };

}
