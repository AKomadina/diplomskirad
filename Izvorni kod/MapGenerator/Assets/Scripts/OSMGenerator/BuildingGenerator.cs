﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Linq;

public class BuildingGenerator : MonoBehaviour
{


    public GameObject MeshObject;
    public GameObject NodeObject;
    public GameObject WindowObject;
    public GameObject DoorObject;
    public GameObject DomeObject;
    public GameObject DoubleDoorObject;
    public GameObject BigOldDoorObject;
    public GameObject GlassDoorObject;
    public GameObject OldDoorObject;
    public GameObject GarageDoorObject;
    public GameObject Natpis;
    public GameObject NatpisText;
    public GameObject KucniBroj;
    public GameObject KucniBrojText;

    public GameObject GovernmentSign;
    public GameObject HotelSign;
    public GameObject UniversitySign;
    public GameObject SchoolSign;
    public GameObject TrainSign;
    public GameObject ChurchSign;
    public GameObject PublicSign;
    public GameObject ServiceSign;
    public GameObject MusicSign;
    public GameObject PoliceSign;
    public GameObject SocialSign;
    public GameObject KinderSign;
    public GameObject PostSign;
    public GameObject TheatreSign;
    public GameObject ToiletsSign;



    Dictionary<string, GameObject> buildingSignDict;


    private TerrainData terrainData;
    private List<Node> nodes = new List<Node>();
    private List<Node> allRoadNodes = new List<Node>();
    private List<Way> buildings = new List<Way>();
    private double terrainScaleFactor;
    private double floorHeight = 3f; //Visina jednog kata = 3 metra
    private List<GameObject> buildingGameObjects;
    private Terrain terrain;

    //Vste fasada
    public static List<string> facadeOptions = new List<string>() { "Facade1Mat", "Facade3Mat", "Facade4Mat", "Facade5Mat", "Facade6Mat", "Facade7Mat" };

    public void Generate(GameObject terrain, List<Node> nodes, List<Way> ways, float terrainScaleFactor, List<Node> allRoadNodes)
    {
        buildingSignDict = new Dictionary<string, GameObject>()
        {
            {"hotel", HotelSign },
            {"government", GovernmentSign },
            {"university", UniversitySign },
            {"school", SchoolSign },
            {"train_station", TrainSign },
            {"church", ChurchSign },
            {"public", PublicSign },
            {"service", ServiceSign },
            {"music_venue", MusicSign },
            {"police", PoliceSign },
            {"social_facility", SocialSign },
            {"kindergarten", KinderSign },
            {"embassy", GovernmentSign },
            {"post_office", PostSign },
            {"townhall", PublicSign },
            {"theatre", TheatreSign },
            {"place_of_worship", ChurchSign },
            {"toilets", ToiletsSign },
        };

        this.nodes = nodes;
        this.buildings = ways;
        buildingGameObjects = new List<GameObject>();
        this.terrainScaleFactor = terrainScaleFactor;
        this.terrain = terrain.GetComponent<Terrain>();
        this.allRoadNodes = allRoadNodes;

        //Stvaranje zgrada
        foreach (Way building in buildings)
        {
            
            //Ima li informacije o visini zgrade
            if (building.buildingHeight == "unknown" || building.buildingHeight == "")
            {

                //Ima li informacije o broju katova
                if (building.buildingLevels == "unknown" || building.buildingLevels == "")
                {
                    bool facadeTex = false;
                    if (Constants.facadeBuildings.Contains(building.buildingType))
                    {
                        facadeTex = true;
                    }
                    if (building.buildingType == "garages")
                    {
                        InstanceBuilding(building, (this.floorHeight + 1.5f) * terrainScaleFactor, facadeTex);
                    }
                    else
                    {
                        InstanceBuilding(building, (3 * this.floorHeight + 1.5f) * terrainScaleFactor, facadeTex);
                    }
                    
                }
                else
                {
                    bool facadeTex = false;
                    if(Double.Parse(building.buildingLevels) > 6 || Constants.facadeBuildings.Contains(building.buildingType))
                    {
                        facadeTex = true;
                    }
                    double height = Double.Parse(building.buildingLevels) * this.floorHeight;
                    InstanceBuilding(building, (height + 1.5f) * terrainScaleFactor, facadeTex);
                }


            }
            else
            {
                string heightNum = new String(building.buildingHeight.Where(Char.IsDigit).ToArray());
                double height = Double.Parse(heightNum);
                bool facadeTex = false;
                if (height > 20 || Constants.facadeBuildings.Contains(building.buildingType))
                {
                    facadeTex = true;
                }
                InstanceBuilding(building, height * terrainScaleFactor, facadeTex);
            }



        }

    }

    //Metoda koja stvara zgradu
    void InstanceBuilding(Way building, double height, bool facadeTex)
    {
        
        HashSet<Node> buildingNodes = new HashSet<Node>();
        bool flagFail = false; //Ako se nađe zgrada čiji čvor nije u skupu podataka
        foreach (long nodeID in building.refNodes)
        {
            Node n = findNode(nodeID);
            if (n == null)
            {
                flagFail = true;
                break;
            }

            buildingNodes.Add(n);
        }
        if(buildingNodes.Count < 4)
        {
            return;
        }
        bool quad = true;
        if (buildingNodes.Count > 4)
        {
            quad = false;
        }

        if (!flagFail)
        {
            bool clockWise = false;
            Vector3[] vertices = new Vector3[buildingNodes.Count];
            int i = 0;
            foreach (Node n in buildingNodes)
            {
                vertices[i] = n.position;
                ++i;
            }

            if (Methods.isPolygonClockwise(new List<Vector3>(vertices))) clockWise = true;


            Color color = Methods.getWallColor();

            double largest1 = double.MinValue;
            double largest2 = double.MinValue;
            int largestInd1 = 0;
            int largestInd2 = 0;
            double closest1 = double.MaxValue;
            double closest2 = double.MaxValue;
            int closestInd1 = 0;
            int closestInd2 = 0;
            List<Vector3[]> normals = new List<Vector3[]>();
            List<Vector3[]> wallVertices = new List<Vector3[]>();
            for (int j = 0; j < vertices.Length; ++j)
            {

                Vector3[] sideVertices = new Vector3[4];
                sideVertices[0] = vertices[j];
                sideVertices[0].y = this.terrain.SampleHeight(vertices[j]);
                sideVertices[1] = vertices[(j + 1) % vertices.Length];
                sideVertices[1].y = this.terrain.SampleHeight(vertices[(j + 1) % vertices.Length]);
                sideVertices[2] = vertices[(j + 1) % vertices.Length];
                sideVertices[2].y = this.terrain.SampleHeight(vertices[(j + 1) % vertices.Length]) + (float)height;
                sideVertices[3] = vertices[j];
                sideVertices[3].y = this.terrain.SampleHeight(vertices[j]) + (float)height;

                double length = Methods.Distance3D(sideVertices[0], sideVertices[1]);
                if (length > largest1)
                {
                    largest2 = largest1;
                    largestInd2 = largestInd1;
                    largest1 = length;
                    largestInd1 = j;

                }
                else if (length > largest2)
                {
                    largest2 = length;
                    largestInd2 = j;
                }
                double distance = Methods.DistanceToWay(sideVertices, allRoadNodes);
                if (distance < closest1)
                {
                    closest2 = closest1;
                    closestInd2 = closestInd1;
                    closest1 = distance;
                    closestInd1 = j;

                }
                else if (distance < closest2)
                {
                    closest2 = distance;
                    closestInd2 = j;
                }
                normals.Add(CreateMesh(sideVertices, clockWise, quad, true, height, building.id,  true, color, building.buildingType, facadeTex));
                wallVertices.Add(sideVertices);

            }

            int doorInd = Methods.getDoorIndex(largestInd1, largestInd2, closestInd1, closestInd2);

            for (int j = 0; j < vertices.Length; ++j)
            {
                if (building.buildingType == "garages")
                {

                    addDoor(wallVertices[j], normals[j],  building.id, building.buildingName, building.buildingType, building.buildingHouseNumber);
                }
                else
                {
                    if (doorInd == j)
                    {
                        
                        addDoor(wallVertices[doorInd], normals[doorInd], building.id, building.buildingName, building.buildingType, building.buildingHouseNumber);
                        if (!facadeTex)
                        {
                            addWindows(wallVertices[j], normals[j], height, building.id, true);
                        }
                        
                    }
                    else
                    {
                        if (!facadeTex)
                        {
                            addWindows(wallVertices[j], normals[j], height, building.id, false);
                        }
                    }
                }
                
            }

            
            //Stvaranje krova ovisno o izgledu poligona
            if (quad)
            {
                InstanceRoof(vertices, building.id,  height, building.buildingRoof, clockWise, color, facadeTex);
            }
            else
            {
                //Vector3[] newVertices = this.getQuadFromMulti(vertices);
                
                //InstanceMultiRoof(newVertices, building.id, heightPosition, height, building.buildingRoof, clockWise, color);
                if(building.buildingType == "church")
                {
                    InstanceMultiRoof(vertices, building.id, height,"dome", clockWise, color);
                }
                else
                {
                    InstanceMultiRoof(vertices, building.id, height, building.buildingRoof, clockWise, color);
                }
                
            }



        }
    }

    //Stvaranje mesh objekta koji predstavlja jedan zid zgrade
    Vector3[] CreateMesh(Vector3[] vertices, bool clockWise, bool quad, bool side,  double height, double id,  bool main, Color color, string type, bool facadeTex)
    {
        int[] triangles;

        //Odabir načina triangularizacije
        if (quad || side)
        {
            if (clockWise)
            {
                triangles = Constants.quadTrianglesClockwise;
            }
            else
            {
                triangles = Constants.quadTrianglesCounterClockwise;
            }
        }
        else
        {
            triangles = Methods.makeTriangles(new List<Vector3>(vertices));
        }


        //Stvaranje mesh objekta
        GameObject meshObj = Instantiate(MeshObject);
        Mesh mesh = new Mesh();
        meshObj.GetComponent<MeshFilter>().mesh = mesh;
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        buildingGameObjects.Add(meshObj);

        //Postavljanje pozicije mesh objekta
        Transform meshObjT = meshObj.transform;
        Vector3 position = meshObjT.localPosition;


        //Postavljanje teksture
        if (side)
        {
            mesh.uv = Methods.generateQuadUVs(vertices, 6f);
            Material mat;
            if(type == "abandoned")
            {
                mat = Resources.Load("BrickMat") as Material;
                meshObj.GetComponent<MeshRenderer>().material = mat;

            }
            else if(facadeTex)
            {
                System.Random rnd = new System.Random();
                int randomType = rnd.Next(0, facadeOptions.Count);
                string facadeType = facadeOptions[randomType];
                mat = Resources.Load(facadeType) as Material;
                meshObj.GetComponent<MeshRenderer>().material = mat;
            }
            else
            {
                mat = Resources.Load("facadeMat") as Material;
                meshObj.GetComponent<MeshRenderer>().material = mat;
                var renderer = meshObj.GetComponent<Renderer>();
                renderer.material.SetColor("_Color", color);
            }
            
        }
        else
        {

            double area = Math.Abs(Methods.signedAreaOfPolygon(new List<Vector3>(vertices)));
            double uvDivider = Math.Sqrt(area);
            mesh.uv = Methods.generateUVs(vertices, (float)uvDivider);
            Material mat = Resources.Load("flatRoofMat") as Material;
            meshObj.GetComponent<MeshRenderer>().material = mat;
        }



        Vector3[] normals = mesh.normals;

        meshObjT.position = position;
        meshObj.name = "Building: " + id.ToString();
        return normals;

    }

    //Stvaranje jednostavnog mesh objekta
    void CreateSimpleRoofMesh(Vector3[] vertices, bool quad, bool clockWise, string name, string tiling, Color color)
    {


        GameObject meshObj = Instantiate(MeshObject);
        Mesh mesh = new Mesh();
        meshObj.GetComponent<MeshFilter>().mesh = mesh;
        mesh.Clear();
        mesh.vertices = vertices;
        if (quad)
        {
            if (clockWise)
            {
                mesh.triangles = Constants.quadTrianglesClockwise;
            }
            else
            {
                mesh.triangles = Constants.quadTrianglesCounterClockwise;

            }
        }
        else
        {
            if (clockWise)
            {
                mesh.triangles = Constants.trianglesClockwise;
            }
            else
            {
                mesh.triangles = Constants.trianglesCounterClockwise;

            }
        }


        double area = Math.Abs(Methods.signedAreaOfPolygon(new List<Vector3>(vertices)));
        double uvDivider = Math.Sqrt(area);


        if (tiling == "roofTri")
        {

            Material mat = Resources.Load("roofMatTri") as Material;
            meshObj.GetComponent<MeshRenderer>().material = mat;
        }
        else if (tiling == "roofHipped")
        {

            string dir = Methods.findRoofDirection(vertices);
            if (dir == "x")
            {
                Material mat = Resources.Load("roofMat") as Material;
                meshObj.GetComponent<MeshRenderer>().material = mat;
            }
            else if (dir == "z")
            {
                Material mat = Resources.Load("roofMatInv") as Material;
                meshObj.GetComponent<MeshRenderer>().material = mat;
            }
            uvDivider = 100f / Math.Sqrt(area);
        }
        else if (tiling == "roofGabled")
        {
            string dir = Methods.findRoofDirection(vertices);
            if (dir == "x")
            {
                Material mat = Resources.Load("roofMat") as Material;
                meshObj.GetComponent<MeshRenderer>().material = mat;
            }
            else if (dir == "z")
            {
                Material mat = Resources.Load("roofMatInv") as Material;
                meshObj.GetComponent<MeshRenderer>().material = mat;
            }
            uvDivider = 100f / Math.Sqrt(area);

        }
        else if (tiling == "facade")
        {
            Material mat = Resources.Load("facadeMat") as Material;
            meshObj.GetComponent<MeshRenderer>().material = mat;
            var renderer = meshObj.GetComponent<Renderer>();
            renderer.material.SetColor("_Color", color);
        }
        else if (tiling == "flatRoof")
        {
            Material mat = Resources.Load("flatRoofMat") as Material;
            meshObj.GetComponent<MeshRenderer>().material = mat;
        }
        mesh.uv = Methods.generateUVs(vertices, (float)uvDivider);

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();


        meshObj.name = name;

    }


    //Metoda za stvaranje objekta koji predstavlja čvor
    void InstantiateNode(Node node)
    {
        GameObject nodeObj = Instantiate(NodeObject);

        Transform nodeObjT = nodeObj.transform;
        nodeObjT.localPosition = node.position;

        nodeObj.name = "BBB " + node.ToString();

    }
    //Metoda za stvaranje objekta koji predstavlja čvor
    void InstantiateNode2(Vector3 vec, string id)
    {
        GameObject nodeObj = Instantiate(NodeObject);

        Transform nodeObjT = nodeObj.transform;
        nodeObjT.localPosition = vec;

        nodeObj.name = "BBB "+id;

    }

    //Metoda za stvaranje objekta koji predstavlja krov (za poligone koji su quad)
    void InstanceRoof(Vector3[] vertices, double id,  double height, string roofType, bool clockWise, Color color, bool facadeTex)
    {
        if (facadeTex)
        {
            color = Color.gray;
        }

        //Točke na krovu
        Vector3 p0 = vertices[0];
        Vector3 p1 = vertices[1];
        Vector3 p2 = vertices[2];
        Vector3 p3 = vertices[3];
        Vector3 m1 = Methods.getMiddlePoint(vertices[1], vertices[2]);
        Vector3 m2 = Methods.getMiddlePoint(vertices[3], vertices[0]);
        Vector3 m3 = Methods.getMiddlePoint(vertices[2], vertices[3]);
        Vector3 m4 = Methods.getMiddlePoint(vertices[0], vertices[1]);
        Vector3 centroid = Methods.getCentroid(vertices);
        Vector3 m5 = Methods.getMiddlePoint(m1, centroid);
        Vector3 m6 = Methods.getMiddlePoint(m2, centroid);
        Vector3 m7 = Methods.getMiddlePoint(m3, centroid);
        Vector3 m8 = Methods.getMiddlePoint(m4, centroid);
        Vector3 w1 = vertices[2];
        Vector3 w2 = vertices[3];
        Vector3 w3 = vertices[0];
        Vector3 w4 = vertices[3];
        Vector3[] roofVertices = new Vector3[] { };

        //Visina krova = 3 metra default
        float roofHeight = 3f * (float)this.terrainScaleFactor;

        //Ako nema podatka o obliku krova uzmi neki random oblik
        if (roofType == "unknown" || roofType == "")
        {
            System.Random rnd = new System.Random();
            int x = rnd.Next(0, 5);
            if (x == 0)
            {
                roofType = "gabled";
            }
            else if (x == 1)
            {
                roofType = "pyramidal";
            }
            else if (x == 2)
            {
                roofType = "hipped";
            }
            else if (x == 3)
            {
                roofType = "skillion";
            }
            else if (x == 4)
            {
                roofType = "flat";
            }
            else
            {
                roofType = "flat";
            }
        }

        string roofName = "Roof " + id + " " + roofType;


        //Izračun dužine, širine i površine
        double dist1 = Methods.Distance2D(p0, p1);
        double dist2 = Methods.Distance2D(p1, p2);
        double s = dist1 * dist2;

        //Ako je površina krova veća od 600 m^2 dodaj jos jedan krovni kat 
        double limit = 600 * this.terrainScaleFactor;
        if (s > limit)
        {
            roofHeight = roofHeight + (3f * (float)this.terrainScaleFactor);
        }

        //Postavi visine točaka krova
        p0.y = this.terrain.SampleHeight(p0) + (float)height;
        p1.y = this.terrain.SampleHeight(p1) + (float)height;
        p2.y = this.terrain.SampleHeight(p2) + (float)height;
        p3.y = this.terrain.SampleHeight(p3) + (float)height;
        m1.y = this.terrain.SampleHeight(m1) + (float)height + roofHeight;
        m2.y = this.terrain.SampleHeight(m2) + (float)height + roofHeight;
        m3.y = this.terrain.SampleHeight(m3) + (float)height + roofHeight;
        m4.y = this.terrain.SampleHeight(m4) + (float)height + roofHeight;
        m5.y = this.terrain.SampleHeight(m5) + (float)height + roofHeight;
        m6.y = this.terrain.SampleHeight(m6) + (float)height + roofHeight;
        m7.y = this.terrain.SampleHeight(m7) + (float)height + roofHeight;
        m8.y = this.terrain.SampleHeight(m8) + (float)height + roofHeight;
        w1.y = this.terrain.SampleHeight(w1) + (float)height + roofHeight;
        w2.y = this.terrain.SampleHeight(w2) + (float)height + roofHeight;
        w3.y = this.terrain.SampleHeight(w3) + (float)height + roofHeight;
        w4.y = this.terrain.SampleHeight(w4) + (float)height + roofHeight;
        centroid.y = this.terrain.SampleHeight(centroid) + (float)height + roofHeight;




        if (dist1 > dist2)
        {

            if (roofType == "gabled")
            {

                //Uzdužni dio krova
                roofVertices = new Vector3[] { p0, p1, m1, m2 };
                CreateSimpleRoofMesh(roofVertices, true, clockWise, roofName, "roofGabled", color);
                roofVertices = new Vector3[] { p2, p3, m2, m1 };
                CreateSimpleRoofMesh(roofVertices, true, clockWise, roofName, "roofGabled", color);
                //Poprečni dio krova
                roofVertices = new Vector3[] { p0, m2, p3 };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "facade", color);
                roofVertices = new Vector3[] { p1, p2, m1 };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "facade", color);


            }
            else if (roofType == "pyramidal")
            {
                roofVertices = new Vector3[] { p0, p1, centroid };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "roofTri", color);
                roofVertices = new Vector3[] { p1, p2, centroid };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "roofTri", color);
                roofVertices = new Vector3[] { p2, p3, centroid };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "roofTri", color);
                roofVertices = new Vector3[] { p3, p0, centroid };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "roofTri", color);
            }
            else if (roofType == "hipped" || roofType == "half-hipped")
            {

                //Uzdužni dio krova
                roofVertices = new Vector3[] { p0, p1, m5, m6 };
                CreateSimpleRoofMesh(roofVertices, true, clockWise, roofName, "roofHipped", color);
                roofVertices = new Vector3[] { p2, p3, m6, m5 };
                CreateSimpleRoofMesh(roofVertices, true, clockWise, roofName, "roofHipped", color);
                //Poprečni dio krova
                roofVertices = new Vector3[] { p1, p2, m5 };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "roofTri", color);
                roofVertices = new Vector3[] { p3, p0, m6 };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "roofTri", color);


            }
            else if (roofType == "skillion")
            {

                //Uzdužni dio krova
                roofVertices = new Vector3[] { p0, p1, w1, w2 };
                CreateSimpleRoofMesh(roofVertices, true, clockWise, roofName, "roofGabled", color);
                roofVertices = new Vector3[] { p2, p3, w2, w1 };
                CreateSimpleRoofMesh(roofVertices, true, clockWise, roofName, "facade", color);
                //Poprečni dio krova
                roofVertices = new Vector3[] { p1, p2, w1 };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "facade", color);
                roofVertices = new Vector3[] { p0, w2, p3 };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "facade", color);


            }
            else if (roofType == "flat")
            {
                roofVertices = new Vector3[] { p1, p2, p3, p0 };
                CreateSimpleRoofMesh(roofVertices, true, clockWise, roofName, "flatRoof", color);
            }
        }
        else
        {
            if (roofType == "gabled")
            {

                //Uzdužni dio krova
                roofVertices = new Vector3[] { p1, p2, m3, m4 };
                CreateSimpleRoofMesh(roofVertices, true, clockWise, roofName, "roofGabled", color);
                roofVertices = new Vector3[] { p3, p0, m4, m3 };
                CreateSimpleRoofMesh(roofVertices, true, clockWise, roofName, "roofGabled", color);
                //Poprečni dio krova
                roofVertices = new Vector3[] { p2, p3, m3 };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "facade", color);
                roofVertices = new Vector3[] { p0, p1, m4 };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "facade", color);

            }
            else if (roofType == "pyramidal")
            {
                roofVertices = new Vector3[] { p0, p1, centroid };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "roofTri", color);
                roofVertices = new Vector3[] { p1, p2, centroid };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "roofTri", color);
                roofVertices = new Vector3[] { p2, p3, centroid };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "roofTri", color);
                roofVertices = new Vector3[] { p3, p0, centroid };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "roofTri", color);
            }
            else if (roofType == "hipped" || roofType == "half-hipped")
            {
                //Uzdužni dio krova
                roofVertices = new Vector3[] { p1, p2, m7, m8 };
                CreateSimpleRoofMesh(roofVertices, true, clockWise, roofName, "roofHipped", color);
                roofVertices = new Vector3[] { p3, p0, m8, m7 };
                CreateSimpleRoofMesh(roofVertices, true, clockWise, roofName, "roofHipped", color);
                //Poprečni dio krova
                roofVertices = new Vector3[] { p0, p1, m8 };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "roofTri", color);
                roofVertices = new Vector3[] { p2, p3, m7 };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "roofTri", color);

            }
            else if (roofType == "skillion")
            {

                //Uzdužni dio krova
                roofVertices = new Vector3[] { p1, p2, w4, w3 };
                CreateSimpleRoofMesh(roofVertices, true, clockWise, roofName, "roofGabled", color);
                roofVertices = new Vector3[] { p0, w3, w4, p3 };
                CreateSimpleRoofMesh(roofVertices, true, clockWise, roofName, "facade", color);
                //Poprečni dio krova
                roofVertices = new Vector3[] { p0, p1, w3 };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "facade", color);
                roofVertices = new Vector3[] { p3, w4, p2 };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "facade", color);



            }
            else if (roofType == "flat")
            {
                roofVertices = new Vector3[] { p0, p1, p2, p3 };
                CreateSimpleRoofMesh(roofVertices, true, clockWise, roofName, "flatRoof", color);
            }
        }

    }


    //Metoda za stvaranje objekta koji predstavlja krov (za poligone koji nisu quad)
    void InstanceMultiRoof(Vector3[] vertices, double id,  double height, string roofType, bool clockWise, Color color)
    {


        //Točke na krovu
        Vector3[] ps = new Vector3[vertices.Length];
        for (int i = 0; i < vertices.Length; ++i)
        {
            Vector3 v = vertices[i];
            Vector3 p = v;
            p.y = this.terrain.SampleHeight(v) + (float)height;
            ps[i] = p;
        }

        //Visina krova = 3 metra default
        float roofHeight = 3f * (float)this.terrainScaleFactor;


        double s = Math.Abs(Methods.signedAreaOfPolygon(new List<Vector3>(vertices)));

        //Ako je površina krova veća od 600 m^2 dodaj jos jedan krovni kat 
        double limit = 600 * this.terrainScaleFactor;
        if (Math.Abs(s) > limit)
        {
            roofHeight = roofHeight + (3f * (float)this.terrainScaleFactor);
        }


        Vector3 centroid = Methods.getCentroid(vertices);
        centroid.y = this.terrain.SampleHeight(centroid) + (float)height + roofHeight;

        //Ispitaj konveksnost
        bool convexFlag = Methods.isPolygonConvex(new List<Vector3>(vertices), true);
        if (!convexFlag && roofType != "dome")
        //Krov je konkavni - stavi ga da je ravan
        {
            roofType = "flat";
        }
        else
        //Krov je konveksni
        {
            //Ako nema podatka o obliku krova uzmi piramidalni oblik
            if (roofType == "unknown" || roofType == "")
            {

                roofType = "pyramidal";
            }
           
        }


        string roofName = "Roof " + id + " " + roofType;
        Vector3[] roofVertices = new Vector3[] { };

        //Kupola
        if (roofType == "dome")
        {
            Vector3[] newVertices = this.getQuadFromMulti(vertices);
            Vector3 cent = Methods.getCentroid(newVertices);
            double area1 = Math.Abs(Methods.signedAreaOfPolygon(new List<Vector3>(vertices)));
            double area2 = Math.Abs(Methods.signedAreaOfPolygon(new List<Vector3>(newVertices)));
            GameObject nodeObj = Instantiate(DomeObject);
            Transform nodeObjT = nodeObj.transform;
            
            nodeObjT.localScale = new Vector3((float)area2 * (float)terrainScaleFactor,  (float)area2 * (float)terrainScaleFactor,  (float)area2 * (float)terrainScaleFactor);
            float heightY = this.terrain.SampleHeight(cent);
            nodeObjT.localPosition = new Vector3(centroid.x, heightY + (float)height, centroid.z);
            nodeObj.name = "Dome " + id;

            CreateMesh(ps, clockWise, false, false, height, id, false, color, "standard", false);

        }
        //Ravni krov
        else if (roofType == "flat" || roofType == "skillion")
        {
            
          
            CreateMesh(ps, clockWise, false, false,  height, id,  false, color, "standard", false) ;
        }
        //Piramidalni krov
        else
        {
            for (int i = 0; i < ps.Length; ++i)
            {
                roofVertices = new Vector3[] { ps[i % ps.Length], ps[(i + 1) % ps.Length], centroid };
                CreateSimpleRoofMesh(roofVertices, false, clockWise, roofName, "roofTri", color);
            }
        }

    }





    //Metoda za pronalazak objekta čvora na temelju njegovog id-a
    Node findNode(long nodeId)
    {
        foreach (Node n in nodes)
        {
            if (n.id == nodeId)
            {
                return n;
            }
        }
        return null;
    }



    //Stvaranje prozora na bočnim zidovima
    void addWindows(Vector3[] vertices, Vector3[] normals, double wallHeight, double id, bool main)
    {
        //Duljina zida
        double lengthX = vertices[1].x - vertices[0].x;
        double lengthZ = vertices[1].z - vertices[0].z;
        double length = Methods.Distance2D(vertices[1], vertices[0]);

        //Ako ne stane niti jedan prozor niti jedna vrata po širini
        if (length < 1f * terrainScaleFactor)
        {
            return;
        }

        if (normals.Length > 0)
        {

            //Broj prozora u stupcu(po visini), oduzmi 1.5m od dna, između svakog prozora 1.5m razmaka
            int rowsNum = (int)((wallHeight - 1.5f * terrainScaleFactor) / (1.5f * terrainScaleFactor + 1.5f * terrainScaleFactor));

            //Broj prozora u redu(po dužini), oduzmi 2m od rubova, između svakog prozora 3m razmaka
            int columnsNum = (int)((length - 4 * terrainScaleFactor) / (0.8f * terrainScaleFactor + 3f * terrainScaleFactor));



            //Razmaci po svakoj osi
            double incX = lengthX / columnsNum;
            double incY = 3f * terrainScaleFactor;
            double incZ = lengthZ / columnsNum;

            for (int j = 0; j < rowsNum; ++j)
            {
                
                //Postavi prozore
                for (int i = 0; i < columnsNum - 1; ++i)
                {
                    if (main)
                    {
                        if (columnsNum % 2 == 0 && i == (columnsNum - 1) / 2 && j == 0)
                        {
                            continue;
                        }
                        else if (columnsNum % 2 != 0 && (i == (columnsNum - 1) / 2 || i == (columnsNum - 1) / 2 - 1) && j == 0)
                        {
                            continue;
                        }
                    }

                    GameObject obj = Instantiate(WindowObject);
                    Transform objT = obj.transform;
                    objT.localScale = new Vector3(1.15f * (float)terrainScaleFactor, 0.85f * (float)terrainScaleFactor, 0.85f * (float)terrainScaleFactor); //Dimenzije prozora (sirina = 1m, visina = 2m)
                    objT.rotation = Quaternion.LookRotation(normals[0]);

                    float posX = vertices[0].x + (float)(i * incX) + (float)incX;
                    float posZ = vertices[0].z + (float)(i * incZ) + (float)incZ;
                    Vector3 pos = new Vector3(posX, 0f, posZ);
                    pos.y = (float)(incY * j) + this.terrain.SampleHeight(pos) + (float)(1.5f * terrainScaleFactor) + (float)(0.75f * terrainScaleFactor);
                    objT.localPosition = pos;

                    obj.name = "Window " + id;
                }


            }
            //Ako ne stane samo jedan stupac ili nijedan sa razmacima (stavi jedan stupac na centar)
            if (columnsNum <= 1)
            {
                for (int j = 1; j < rowsNum; ++j)
                {
                    
                    GameObject obj = Instantiate(WindowObject);
                    Transform objT = obj.transform;
                    objT.localScale = new Vector3(1.15f * (float)terrainScaleFactor, 0.85f * (float)terrainScaleFactor, 0.85f * (float)terrainScaleFactor); //Dimenzije prozora (sirina = 1m, visina = 2m)
                    objT.rotation = Quaternion.LookRotation(normals[0]);


                    float posX = vertices[0].x + (float)lengthX / 2;
                    float posZ = vertices[0].z + (float)lengthZ / 2;
                    Vector3 pos = new Vector3(posX, 0f, posZ);
                    pos.y = (float)(incY * j) + this.terrain.SampleHeight(pos) + (float)(1.5f * terrainScaleFactor) + (float)(0.75f * terrainScaleFactor);
                    objT.localPosition = pos;

                    obj.name = "Window " + id;

                }
            }
        }
    }




    //Stvaranje vrata na zgradi
    void addDoor(Vector3[] vertices, Vector3[] normals,  double id, string buildingName, string buildingType, string houseNumber)
    {
        if (normals.Length > 0)
        {
            Vector3 centroid = Methods.getCentroid(vertices);
            Vector3 pos = new Vector3(centroid.x, 0f, centroid.z);
            pos.y = this.terrain.SampleHeight(pos);

            Quaternion rotation = Quaternion.LookRotation(normals[0]);
            Vector3 transVectorTable = generateTextTransVector(rotation.eulerAngles.y) * 0.5f;
            Vector3 transVectorText = generateTextTransVector(rotation.eulerAngles.y);

            Vector3 centroidR = Methods.getCentroid(new Vector3[] { vertices[0], centroid });
            Vector3 centroidR2 = Methods.getCentroid(new Vector3[] { centroidR, centroid });
            Vector3 centroidR3 = Methods.getCentroid(new Vector3[] { centroidR2, centroid });
            Vector3 centroidL = Methods.getCentroid(new Vector3[] { vertices[1], centroid });
            Vector3 centroidL2 = Methods.getCentroid(new Vector3[] { centroidL, centroid });
            Vector3 centroidL3 = Methods.getCentroid(new Vector3[] { centroidL2, centroid });
            Vector3 posR = new Vector3(0f, 0f, 0f);
            Vector3 posL = new Vector3(0f, 0f, 0f);
            if (buildingName != "")
            {
                posR = new Vector3(centroidR2.x, 0f, centroidR2.z);
                posR.y = this.terrain.SampleHeight(posR);
                posL = new Vector3(centroidL2.x, 0f, centroidL2.z);
                posL.y = this.terrain.SampleHeight(posL);
            }
            else
            {
                posR = new Vector3(centroidR3.x, 0f, centroidR3.z);
                posR.y = this.terrain.SampleHeight(posR);
                posL = new Vector3(centroidL3.x, 0f, centroidL3.z);
                posL.y = this.terrain.SampleHeight(posL);
            }
           

            if (Constants.glassDoorBuildings.Contains(buildingType))
            {
                GameObject obj = Instantiate(GlassDoorObject);
                Transform objT = obj.transform;
                objT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                objT.rotation = rotation;
                objT.localPosition = pos;
                obj.name = "Glass door " + id + " " + buildingType;
            }
            else if (Constants.doubleDoorBuildings.Contains(buildingType))
            {
                GameObject obj = Instantiate(DoubleDoorObject);
                Transform objT = obj.transform;
                objT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                objT.rotation = rotation;
                objT.localPosition = pos;
                obj.name = "Double door " + id + " " + buildingType;
            }
            else if(buildingType == "abandoned")
            {
                GameObject obj = Instantiate(OldDoorObject);
                Transform objT = obj.transform;
                objT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                objT.rotation = rotation;
                objT.localPosition = pos;
                obj.name = "Abandoned door " + id;
            }
            else if (buildingType == "garages")
            {
                GameObject obj = Instantiate(GarageDoorObject);
                Transform objT = obj.transform;
                objT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                objT.rotation = rotation;
                objT.eulerAngles = new Vector3(
                  objT.eulerAngles.x ,
                  objT.eulerAngles.y + 90f,
                  objT.eulerAngles.z
               );
                objT.localPosition = pos;
                objT.position += transVectorTable;  
                obj.name = "Garage door " + id;
            }
            else if (Constants.bigOldDoorBuildings.Contains(buildingType))
            {
                GameObject obj = Instantiate(BigOldDoorObject);
                Transform objT = obj.transform;
                objT.localScale = new Vector3(10f * (float)terrainScaleFactor, 10f * (float)terrainScaleFactor, 10f * (float)terrainScaleFactor);
                objT.rotation = rotation;
                objT.localPosition = pos;
                obj.name = "Big old door " + id;
                posR = new Vector3(centroidR.x, 0f, centroidR.z);
                posR.y = this.terrain.SampleHeight(posR);
                posL = new Vector3(centroidL.x, 0f, centroidL.z);
                posL.y = this.terrain.SampleHeight(posL);
                posR.y += 1.5f * (float)terrainScaleFactor;
                posL.y += 1.5f * (float)terrainScaleFactor;
                pos.y += 1.5f * (float)terrainScaleFactor;
                
            }
            else
            {
                GameObject obj = Instantiate(DoorObject);
                Transform objT = obj.transform;
                objT.localScale = new Vector3(0.011f * (float)terrainScaleFactor, 0.011f * (float)terrainScaleFactor, 0.011f * (float)terrainScaleFactor); //Dimenzije vrata (sirina = 1m, visina = 2.5m)
                objT.rotation = rotation;
                objT.localPosition = pos;
                obj.name = "Door " + id +" "+buildingType;
            }

            if(buildingName != "")
            {
                GameObject nodeObj = Instantiate(Natpis);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3(0.2f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 0.1f * (float)terrainScaleFactor);
                nodeObjT.rotation = rotation;
                nodeObjT.eulerAngles = new Vector3(
                   nodeObjT.eulerAngles.x +90f,
                   nodeObjT.eulerAngles.y,
                   nodeObjT.eulerAngles.z
                );
                nodeObjT.localPosition = new Vector3(pos.x, pos.y + 3.5f * (float)terrainScaleFactor, pos.z);
                nodeObjT.position += transVectorTable;
                nodeObj.name = "Natpis " + id + " " + buildingName;


                GameObject nodeObj2 = Instantiate(NatpisText);
                Transform nodeObjT2 = nodeObj2.transform;
                nodeObjT2.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                nodeObjT2.rotation = rotation;
                nodeObjT2.eulerAngles = new Vector3(
                  nodeObjT.eulerAngles.x +90f,
                  nodeObjT.eulerAngles.y ,
                  nodeObjT.eulerAngles.z +180f 
               );
                nodeObjT2.localPosition = new Vector3(pos.x, pos.y + 3.5f*(float) terrainScaleFactor, pos.z);
                nodeObjT2.position += transVectorText;
                TMPro.TextMeshProUGUI text = nodeObj2.GetComponent<TMPro.TextMeshProUGUI>();
                text.text = buildingName;
                nodeObj2.name = "Natpis text " + id +" "+buildingName;
            }
            if(houseNumber != "unknown")
            {
                GameObject nodeObj = Instantiate(KucniBroj);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3(0.04f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 0.04f * (float)terrainScaleFactor);
                nodeObjT.rotation = rotation;
                nodeObjT.eulerAngles = new Vector3(
                   nodeObjT.eulerAngles.x + 90f,
                   nodeObjT.eulerAngles.y,
                   nodeObjT.eulerAngles.z
                );
                nodeObjT.localPosition = new Vector3(posR.x , posR.y + 3.5f * (float)terrainScaleFactor, posR.z);
                nodeObjT.position += transVectorTable;
                nodeObj.name = "Kucni broj " + id + " " + houseNumber;


                GameObject nodeObj2 = Instantiate(KucniBrojText);
                Transform nodeObjT2 = nodeObj2.transform;
                nodeObjT2.localScale = new Vector3(2f * (float)terrainScaleFactor, 2f * (float)terrainScaleFactor, 2f * (float)terrainScaleFactor);
                nodeObjT2.rotation = rotation;
                nodeObjT2.eulerAngles = new Vector3(
                  nodeObjT.eulerAngles.x + 90f,
                  nodeObjT.eulerAngles.y,
                  nodeObjT.eulerAngles.z + 180f
               );
                nodeObjT2.localPosition = new Vector3(posR.x, posR.y + 3.5f * (float)terrainScaleFactor, posR.z);
                nodeObjT2.position += transVectorText;
                TMPro.TextMeshProUGUI text = nodeObj2.GetComponent<TMPro.TextMeshProUGUI>();
                text.text = houseNumber;
                nodeObj2.name = "Kucni broj text " + id + " " + houseNumber;
            }
            if (buildingSignDict.ContainsKey(buildingType))
            {
                GameObject nodeObj = Instantiate(buildingSignDict[buildingType]);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3(0.1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 0.1f * (float)terrainScaleFactor);
                nodeObjT.rotation = rotation;
                nodeObjT.eulerAngles = new Vector3(
                   nodeObjT.eulerAngles.x + 90f,
                   nodeObjT.eulerAngles.y,
                   nodeObjT.eulerAngles.z
                );
                nodeObjT.localPosition = new Vector3(posL.x, posL.y + 3.5f * (float)terrainScaleFactor, posL.z);
                nodeObjT.position += transVectorTable;
                nodeObj.name = "Oznaka " + id + " " + buildingType;
            }

        }
    }


    //Dohvat vektora pomaka text objekta na znaku
    public Vector3 generateTextTransVector( float orientation)
    {
       
            if ((orientation >= 0f && orientation <= 90f) )
            {
                return (+1f * new Vector3(1f, 0f, 1f) * 0.05f * (float)terrainScaleFactor);

            }
            else if((orientation >= 180f && orientation <= 270f))
            {
            return (-1f * new Vector3(1f, 0f, 0f) * 0.05f * (float)terrainScaleFactor);
            }
            else if ((orientation >= 90 && orientation <= 180))
            {
                return (-1f * new Vector3(0f, 0f, 1f) * 0.05f * (float)terrainScaleFactor);
            }
            else
            {
                return (1f * new Vector3(0f, 0f, 1f) * 0.05f * (float)terrainScaleFactor);

            } 

    }


    //Metoda za stvaranje poligona sa 4 točke od poligona sa >4 točke
    public Vector3[] getQuadFromMulti(Vector3[] vertices)
    {
        List<Vector2> polVertices = new List<Vector2>();
        for (int i = 0; i < vertices.Length; ++i)
        {
            polVertices.Add(new Vector2(vertices[i].x, vertices[i].z));
        }
        List<int> minBoundingBox = Methods.getMinBoundingBox(polVertices, (float)terrainScaleFactor);
        int minX = minBoundingBox[0];
        int maxX = minBoundingBox[1];
        int minZ = minBoundingBox[2];
        int maxZ = minBoundingBox[3];
        Vector3 vec1 = new Vector3(minX, 0f, minZ);
        Vector3 vec2 = new Vector3(minX, 0f, maxZ);
        Vector3 vec3 = new Vector3(maxX, 0f, minZ);
        Vector3 vec4 = new Vector3(maxX, 0f, maxZ);
        float heightY = this.terrain.SampleHeight(vec1);
        vec1.y = heightY;
        heightY = this.terrain.SampleHeight(vec2);
        vec2.y = heightY;
        heightY = this.terrain.SampleHeight(vec3);
        vec3.y = heightY;
        heightY = this.terrain.SampleHeight(vec4);
        vec4.y = heightY;
        return new Vector3[] { vec1, vec2, vec4, vec3 };
    }



}
