﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine.Networking;

public class TerrainGenerator : MonoBehaviour
{
    
    public GameObject terrainPrefab;
    private GameObject terrainGameObject;
    public GameObject sceneCamera;
    public string ElevationAPI_KEY = "AIzaSyC6nFu9kXr75LBehml_W0j0iB6XctEWUe0";
    public string MapBoxKey = "pk.eyJ1IjoiYXN0ZXJtaXgiLCJhIjoiY2pxMmRxc3R5MTNzaDQyb2J2bTExaWVucSJ9.JexCZBrcLkcoQE6lVkqzmg";

    public void Start()
    {
        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
    }

    public GameObject BuildTerrain(string externalDir, List<double> geoInfo, float terrainScaleFactor, bool sateliteImage)
    {
        //Instanciranje objekta terena
        GameObject terrainObj = Instantiate(terrainPrefab);
        terrainObj.name = "Terrain";
        terrainGameObject = terrainObj;

        
        //Google elevation API zahtjev na temelju koordinata granica
        List<List<ElevationObject>> elevations = new List<List<ElevationObject>>();
        int terrainRes = terrainObj.GetComponent<Terrain>().terrainData.heightmapResolution;
        double diffLat = geoInfo[0] - geoInfo[1];
        double inc = diffLat / (terrainRes-1);
        double maxElevation = Double.MinValue;
        double minElevation = Double.MaxValue;
        
        
        for(int n = 0; n < terrainRes; ++n)
        {
            List<ElevationObject> elevationRow = new List<ElevationObject>();
            double lat1 = geoInfo[1] + (inc * n);

            string elevationAPIUrl = "https://maps.googleapis.com/maps/api/elevation/json?path=" + lat1 + "," + geoInfo[3] + "|" + lat1 + "," + geoInfo[2] + "&samples=" + terrainRes + "&key=" + this.ElevationAPI_KEY;
           
            var response = new WebClient().DownloadString(elevationAPIUrl);
            

            //Parsiranje odgovora
            
            string[] results = Regex.Split(response, "elevation\" : ");
            results = results.Skip(1).ToArray();
            foreach (string result in results)
            {
                string[] parts = Regex.Split(result, "location\" : ");

                string[] elevationString = Regex.Split(parts[0], ",");
                double elevation = Double.Parse(elevationString[0]);

                string[] latString = Regex.Split(parts[1], "lat\" : ");
                string[] lngString = Regex.Split(parts[1], "lng\" : ");

                string[] latString2 = Regex.Split(latString[1], ",");
                double latitude = Double.Parse(latString2[0]);

                string[] lngString2 = Regex.Split(lngString[1], " ");
                double longitude = Double.Parse(lngString2[0]);

                ElevationObject elevationObj = new ElevationObject(elevation, latitude, longitude);
                elevationRow.Add(elevationObj);

                if (elevation > maxElevation) maxElevation = elevation;
                if (elevation < minElevation) minElevation = elevation;

            }
            
            elevations.Add(elevationRow);

            
        }

        /*
        maxElevation = 128f;
        minElevation = 114f;
        */


        //Postavljanje visinske mape pomocu google API-ja
        ApplyGoogleElevationAPI(terrainObj.GetComponent<Terrain>().terrainData, terrainRes, elevations,maxElevation);

        //Izračun veličine terena temeljem zadanih koordinata
        double sizeX = Methods.SphericalCosinus(geoInfo[1], geoInfo[3], geoInfo[1], geoInfo[2]);
        double sizeZ = Methods.SphericalCosinus(geoInfo[0], geoInfo[3], geoInfo[1], geoInfo[3]);

        UnityEngine.Debug.Log("Area size: "+sizeX * sizeZ + " m");
        double virtualArea = (float)sizeZ * terrainScaleFactor * (float)sizeX * terrainScaleFactor;
        double tileSizeX = sizeX / 4096f;
        double tileSizeZ = sizeZ / 4096f;

        terrainObj.GetComponent<Terrain>().terrainData.size = new Vector3((float)sizeX * terrainScaleFactor, (float)maxElevation * terrainScaleFactor, (float)sizeZ * terrainScaleFactor);

        //Postavljanje veličine tileova za layere
        TerrainLayer[] allLayers = terrainObj.GetComponent<Terrain>().terrainData.terrainLayers;
        for (int i = 0; i < allLayers.Length; ++i)
        {
            TerrainLayer layer = allLayers[i];
            float tileSize = 0.00166f * (float)Math.Sqrt(virtualArea) + 2.00222f;
            layer.tileSize = new Vector2(tileSize,tileSize);
        }

        //Postavljanje kamere na sredinu terena
        Vector3 center = new Vector3((float)sizeX/2 * terrainScaleFactor, 0f, (float)sizeZ/2 * terrainScaleFactor) ;
        center.y = terrainObj.GetComponent<Terrain>().SampleHeight(center) + 10f* terrainScaleFactor;
        sceneCamera.transform.position = center;


        //Dohvaćanje teksture mape za teren
        int x = (int)( (float)sizeX * terrainScaleFactor);
        int y = (int)((float)sizeZ * terrainScaleFactor);
        if(sizeX > sizeZ)
        {
            x = 1280;
            double coef = sizeX / sizeZ;
            y = (int)Math.Round( 1280 / coef );
            
        }
        else
        {
            y = 1280;
            double coef = sizeZ / sizeX;
            x = (int)Math.Round(1280 / coef); ;
        }

        if (sateliteImage)
        {
            string mapBoxURL = "http://api.mapbox.com/styles/v1/mapbox/satellite-v9/static/[" + geoInfo[3] + "," + geoInfo[1] + "," + geoInfo[2] + "," + geoInfo[0] + "]/" + x + "x" + y + "@2x?access_token=" + MapBoxKey;

          
            using (WebClient client = new WebClient())
            {
                client.DownloadFile(new Uri(mapBoxURL), "./External/MapBox/image.jpg");

            }


            string currDir = Directory.GetCurrentDirectory();
            string texturePath = currDir + "\\External\\MapBox\\image.jpg";
            StartCoroutine(applyTextureImage(texturePath));

        }


        TerrainLayer[] tlayers = Terrain.activeTerrain.terrainData.terrainLayers; 
        x = (int)((float)sizeX * terrainScaleFactor);
        y = (int)((float)sizeZ * terrainScaleFactor);
        tlayers[13].tileSize = new Vector2(x, y);
       


        return terrainObj;
    }
  

    //Metoda za postavljanje visinske mape na teren temeljem podataka iz Google API-ja
    void ApplyGoogleElevationAPI(TerrainData terrain, int terrainRes, List<List<ElevationObject>> elevations, double maxElev)
    {
        float[,] heights = terrain.GetHeights(0, 0, terrainRes, terrainRes);
        double[,] array = new double[terrainRes, terrainRes];
        
        for (int i = 0; i < terrainRes; ++i)
        {
            List<ElevationObject> elevationRow = elevations[i];
            for (int j = 0; j < terrainRes; ++j)
            {
                double elev = elevationRow[j].elevation;
                double normElev = elev/maxElev;
                array[i, j] = normElev;
            }
        }
     
        
        for (int x = 0; x < terrainRes; x++)
        {

            for (int y = 0; y < terrainRes; y++)
            {
                heights[x, y] = (float)array[x,y];
              
            }
        }
        
        
        terrain.SetHeights(0, 0, heights);
    }

    IEnumerator applyTextureImage(string texturePath)
    {
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(texturePath))
        {
            
          
               yield return uwr.SendWebRequest(); 
            

            Texture2D mapTexture = DownloadHandlerTexture.GetContent(uwr);
            TerrainLayer[] tlayers = Terrain.activeTerrain.terrainData.terrainLayers;
            tlayers[13].diffuseTexture = mapTexture;

        }
    }




}
