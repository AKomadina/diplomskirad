﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;



public class TerrainPainter : MonoBehaviour
{

    public GameObject NodeObject;
    public GameObject SquareObject;

    public GameObject ArrowObject;
    private List<Node> nodes = new List<Node>();
    private List<Way> ways = new List<Way>();
    private List<MultiPolygon> amenitys = new List<MultiPolygon>();
    private List<MultiPolygon> landuses = new List<MultiPolygon>();
    private List<MultiPolygon> leisures = new List<MultiPolygon>();
    private List<MultiPolygon> manMade = new List<MultiPolygon>();
    private List<Vector2> roadBoundPoints = new List<Vector2>();
    private List<Vector2> allRoadBoundPoints = new List<Vector2>();
    private List<Vector2> wayBoundPoints = new List<Vector2>();
    private HashSet<Vector2> allBoundHashSet = new HashSet<Vector2>();
    private HashSet<Vector2> roadBoundHashSet = new HashSet<Vector2>();
    private HashSet<Vector2> wayBoundHashSet = new HashSet<Vector2>();
    private bool sateliteImage = false;
    List<Vector2> allRoadPoints = new List<Vector2>();
    List<Vector2> trafficRoadPoints = new List<Vector2>();
    List<Node> WayNodes = new List<Node>();
    private float[,,] splatmapData;
    private TerrainData terrainData;
    private float terrainScaleFactor;
    private double tileSizeX;
    private double tileSizeZ;
    private Terrain terrain;
    Dictionary<Vector2, float> roadNodeWidthDict = new Dictionary<Vector2, float>();
    Dictionary<long, List<Vector2>> trafficBoundPointsDict = new Dictionary<long, List<Vector2>>();
    Dictionary<long, List<Vector2>> allBoundPointsDict = new Dictionary<long, List<Vector2>>();

    //Dohvaćanje širine čvora ceste
    public Dictionary<Vector2, float> getNodesWidth()
    {
        return roadNodeWidthDict;

    }

    //Dohvaćanje točaka koje čine rubove cesta
    public List<Vector2> getRoadBoundPoints(string type)
    {
        if (type == "all")
        {
            return this.allRoadBoundPoints;
        }
        else if (type == "traffic")
        {
            return this.roadBoundPoints;
        }
        else if (type == "rest")
        {
            return this.wayBoundPoints;
        }
        else
        {
            return this.allRoadBoundPoints;
        }

    }

    //Dohvaćanje točaka koje čine rubove cesta s pripadnim id-jem ceste
    public Dictionary<long, List<Vector2>> getRoadBoundPointsDict(string type)
    {
        if (type == "all")
        {
            return this.allBoundPointsDict;
        }
        else if (type == "traffic")
        {
            return this.trafficBoundPointsDict;
        }
        else
        {
            return this.allBoundPointsDict;
        }

    }

    //Vraćanje svih glavnih čvorova koje čine ceste
    public List<Vector2> getRoadPoints(string type)
    {
        if (type == "all")
        {
            return this.allRoadPoints;
        }
        else if (type == "traffic")
        {
            return this.trafficRoadPoints;
        }
        else
        {
            return this.allRoadPoints;
        }

    }


    public void Generate(GameObject terrain, float terrainScaleFactor, List<Node> nodes, List<Way> ways, List<MultiPolygon> amenitys, List<MultiPolygon> landuses, List<MultiPolygon> leisures, List<MultiPolygon> manMade, bool sateliteImage)
    {
        this.nodes = nodes;
        this.ways = ways;
        this.landuses = landuses;
        this.leisures = leisures;
        this.amenitys = amenitys;
        this.manMade = manMade;
        this.terrainScaleFactor = terrainScaleFactor;
        this.terrain = terrain.GetComponent<Terrain>();
        this.sateliteImage = sateliteImage;

        //Postavke terena
        terrainData = terrain.GetComponent<Terrain>().terrainData;
        splatmapData = new float[terrainData.alphamapWidth, terrainData.alphamapHeight, terrainData.alphamapLayers];


        Vector3 terrainSize = terrain.GetComponent<Terrain>().terrainData.size;
        double tileSizeX = terrainSize.x / 4096f;
        double tileSizeZ = terrainSize.z / 4096f;

        this.tileSizeX = tileSizeX;
        this.tileSizeZ = tileSizeZ;

        //Bojanje terena teksturom
        paintTerrain();

    }



    //Metoda za stvaranje objekta koji predstavlja čvor
    void InstantiateNode(Node node, string name)
    {
        GameObject nodeObj = Instantiate(NodeObject);

        Transform nodeObjT = nodeObj.transform;
        nodeObjT.localPosition = node.position;

        nodeObj.name = name;

        //node.addObject(nodeObj);
        //Destroy(nodeObj);

    }


    //Algoritam za bojanje terena zadanim teksturama
    void paintTerrain()
    {

        //Prekrij prvo cijeli teren sa zadanom teksturom
        for (int y = 0; y < terrainData.alphamapHeight; y++)
        {
            for (int x = 0; x < terrainData.alphamapWidth; x++)
            {
                string tex = "";
                if (sateliteImage)
                {
                    tex = "map";
                }
                else
                {
                    tex = "grass";
                }
                float[] splatWeights = Constants.roadSurfaceOptions[tex];

                for (int i = 0; i < terrainData.alphamapLayers; i++)
                {
                    splatmapData[x, y, i] = splatWeights[i];
                }
            }
        }



        foreach (MultiPolygon landuse in landuses)
        {

            //Građevinsko zemljište
            if (landuse.type == "brownfield" || landuse.type == "construction")
            {
                float[] splatWeights = Constants.roadSurfaceOptions["concrete"];
                paintPolygon(landuse, splatWeights);
            }

            //Travnata podloga
            else if (landuse.type == "grass")
            {
                float[] splatWeights = Constants.roadSurfaceOptions["grass"];
                paintPolygon(landuse, splatWeights);
            }

            //Polje sadnica
            else if (landuse.type == "plant_nursery")
            {
                float[] splatWeights = Constants.roadSurfaceOptions["ground"];
                paintPolygon(landuse, splatWeights);
            }

            //Građanska podloga
            else if (landuse.type == "residential")
            {
                float[] splatWeights = Constants.roadSurfaceOptions["asphaltResidential"];
                paintPolygon(landuse, splatWeights);
            }


        }
        foreach (MultiPolygon leisure in leisures)
        {
            //Dječje igralište
            if (leisure.type == "playground")
            {
                float[] splatWeights = Constants.roadSurfaceOptions["rubber"];
                paintPolygon(leisure, splatWeights);
            }

        }


        //Dohvati čvorove koji predstavljaju križanje ceste
        List<Vector3> crossingPositions = new List<Vector3>();
        foreach (Node n in nodes)
        {
            if (n.type == "crossing" || n.type == "traffic_signals")
            {
                crossingPositions.Add(n.position);
            }
        }


        //Obojaj točke između čvorova u teksturu ceste
        foreach (Way w in ways)
        {
            string roadSurface = w.roadSurface;
            string roadType = w.roadType;


            if (!Constants.roadOptions.Contains(w.roadType))
            {
                continue;
            }
            List<Vector3> points = new List<Vector3>();
            Node prevNode = null;



            if (!Constants.roadSurfaceOptions.ContainsKey(roadSurface))
            {
                roadSurface = "asphalt";
            }

            if (roadType == "cycleway")
            {
                roadSurface = "cycleway";
            }

            int lanesCnt = 1;
            if (w.roadLanes != "unknown")
            {
                lanesCnt = Int32.Parse(w.roadLanes);
            }
            else if (Constants.roadLanesCount.ContainsKey(roadType))
            {
                lanesCnt = Constants.roadLanesCount[roadType];
            }

            float roadWidthFactor = this.generateRoadWidthFactor(roadType, lanesCnt);




            foreach (long nodeId in w.refNodes)
            {

                Node n = findNode(nodeId);
                if (n != null)
                {

                    WayNodes.Add(n);
                    points.Add(n.position);
                    Vector2 nodeVector = new Vector2(n.position.x, n.position.z);

                    if (Constants.roadsWithTrafficObjects.Contains(roadType))
                    {
                        if (roadNodeWidthDict.ContainsKey(nodeVector))
                        {
                            roadNodeWidthDict[nodeVector] = roadWidthFactor;
                        }
                        else
                        {
                            roadNodeWidthDict.Add(nodeVector, roadWidthFactor);

                        }

                    }

                    //InstantiateNode(n, roadWidthFactor.ToString());

                    if (prevNode != null)
                    {
                        //Generiranje međutočaka
                        Vector3 start = new Vector3((float)prevNode.terrainPosition.x, 0f, (float)prevNode.terrainPosition.y);
                        Vector3 end = new Vector3((float)n.terrainPosition.x, 0f, (float)n.terrainPosition.y);
                        List<Vector2> wayPoints = Bresenham(start, end, 1, true);
                        Vector3 startN = new Vector3((float)prevNode.position.x, 0f, (float)prevNode.position.z);
                        Vector3 endN = new Vector3((float)n.position.x, 0f, (float)n.position.z);
                        List<Vector2> wayPointsN = Bresenham(startN, endN, 1, true);

                        //Spremanje međutočaka
                        allRoadPoints.AddRange(wayPointsN);
                        if (Constants.roadsWithTrafficObjects.Contains(roadType))
                        {
                            trafficRoadPoints.AddRange(wayPointsN);
                        }

                        //Bojanje ceste i rubnika
                        string directionWay = Methods.findDirection(n, prevNode);
                        paintRoadPoints(wayPoints, roadType, Constants.roadSurfaceOptions[roadSurface], directionWay, roadWidthFactor);

                        //Bojanje crta
                        if (Constants.roadsWithWhiteLine.Contains(roadType))
                        {

                            paintCenterRoadLinePoints(wayPoints, directionWay, roadType, w.roadOneWay, roadWidthFactor, lanesCnt);

                        }

                    }
                }
                prevNode = n;
            }

            if (points.Count > 1)
            {
                List<Vector2> boundPts = new List<Vector2>();
                List<Vector3> boundPoints = Methods.generateBoundPoints(points, roadWidthFactor, terrainScaleFactor, this.terrain, roadType);
                foreach (Vector3 vec in boundPoints)
                {



                    if (Methods.isInBoundsFloat(0, terrain.terrainData.size.x, 0, terrain.terrainData.size.z, vec.x, vec.z) == true)
                    {
                        bool flag = false;


                       

                        foreach (Vector3 pos in crossingPositions)
                        {
                            double dist = Methods.Distance3D(new Vector3(vec.x, 0f, vec.z), new Vector3(pos.x, 0f, pos.z));
                            double radius = 0;
                            if (Constants.roadsWithTrafficObjects.Contains(roadType))
                            {
                                if (roadWidthFactor >= 6)
                                {
                                    radius = roadWidthFactor * (float)terrainScaleFactor;
                                }
                                else
                                {
                                    radius = 2 * roadWidthFactor * (float)terrainScaleFactor;
                                }
                            }
                            else
                            {
                                radius = 2 * roadWidthFactor * (float)terrainScaleFactor;
                            }


                            if (dist <= radius)
                            {
                                flag = true;
                                break;
                            }
                        }


                        if (!flag)
                        {
                            if (Constants.roadsWithTrafficObjects.Contains(roadType))
                            {


                                this.roadBoundHashSet.Add(new Vector2(vec.x, vec.z));

                            }
                            else
                            {
                                this.wayBoundHashSet.Add(new Vector2(vec.x, vec.z));
                            }


                            boundPts.Add(new Vector2(vec.x, vec.z));
                            this.allBoundHashSet.Add(new Vector2(vec.x, vec.z));

                        }

                    }

                }
                allBoundPointsDict[w.id] = boundPts;
                if (Constants.roadsWithTrafficObjects.Contains(roadType))
                {
                    trafficBoundPointsDict[w.id] = boundPts;
                }
            }



        }


        Debug.Log("Road bound hash points: " + this.allBoundHashSet.Count);

        this.allRoadBoundPoints = new List<Vector2>(allBoundHashSet);
        this.roadBoundPoints = new List<Vector2>(roadBoundHashSet);
        this.wayBoundPoints = new List<Vector2>(wayBoundHashSet);


        //Multi poligoni ostalo
        foreach (MultiPolygon amenity in amenitys)
        {
            //Auto praonica, pumpa za gorivo
            if (amenity.type == "car_wash" || amenity.type == "fuel")
            {
                float[] splatWeights = Constants.roadSurfaceOptions["asphalt"];
                paintPolygon(amenity, splatWeights);
            }
            else if (amenity.type == "bicycle_parking")
            {
                float[] splatWeights = Constants.roadSurfaceOptions["asphaltResidential"];
                paintPolygon(amenity, splatWeights);
            }


            else if (amenity.type == "parking")
            {

                float[] splatWeights1 = Constants.roadSurfaceOptions["asphalt"];
                float[] splatWeights2 = Constants.roadSurfaceOptions["line"];
                paintParking(amenity, splatWeights1, splatWeights2, amenity.id);
            }
            else if (amenity.type == "taxi")
            {

                float[] splatWeights1 = Constants.roadSurfaceOptions["asphalt"];
                float[] splatWeights2 = Constants.roadSurfaceOptions["yellowLine"];
                paintParking(amenity, splatWeights1, splatWeights2, amenity.id);
            }
        }

        /*
        foreach(Vector3 v in this.allRoadPoints)
        {
            GameObject nodeObj = Instantiate(NodeObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(0.3f * (float)terrainScaleFactor, 0.3f * (float)terrainScaleFactor, 0.3f * (float)terrainScaleFactor);
            Vector3 pos = new Vector3(v.x, 0f, v.y);
            pos.y = this.terrain.SampleHeight(pos);
            nodeObjT.localPosition = pos;

        }
        foreach (Vector3 v in this.trafficRoadPoints)
        {
            GameObject nodeObj = Instantiate(NodeObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            Vector3 pos = new Vector3(v.x, 0f, v.y);
            pos.y = this.terrain.SampleHeight(pos);
            nodeObjT.localPosition = pos;

        }
       
        
        foreach (Vector3 v in crossingPositions)
        {
            GameObject nodeObj = Instantiate(NodeObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            Vector3 pos = new Vector3(v.x, 0f, v.z);
            pos.y = this.terrain.SampleHeight(pos);
            nodeObjT.localPosition = pos;
            nodeObj.name = "crossing";

        }
      
        foreach (Vector3 v in this.wayBoundPoints)
        {
            GameObject nodeObj = Instantiate(SquareObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(0.3f * (float)terrainScaleFactor, 0.3f * (float)terrainScaleFactor, 0.3f * (float)terrainScaleFactor);
            Vector3 pos = new Vector3(v.x, 0f, v.y);
            pos.y = this.terrain.SampleHeight(pos);
            nodeObjT.localPosition = pos;

        }
  
        foreach (Vector3 v in this.roadBoundPoints)
        {
            GameObject nodeObj = Instantiate(SquareObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            Vector3 pos = new Vector3(v.x, 0f, v.y);
            pos.y = this.terrain.SampleHeight(pos);
            nodeObjT.localPosition = pos;

        }
        */

        //Postavi mješavinu teksture na površinu terena
        terrainData.SetAlphamaps(0, 0, splatmapData);
    }


    //Metoda koja iz niza točaka(koordinata) boja površinu terena teksturom ceste
    void paintRoadPoints(List<Vector2> points, string roadType, float[] splatWeights, string direction, float roadWidthFactor)
    {


        List<Vector2> newPoints = points;

        float[] splatWeightsBound = new float[terrainData.alphamapLayers];
        splatWeightsBound[0] = 0f;
        splatWeightsBound[1] = 0f;
        splatWeightsBound[2] = 1f;

        bool hasBoundLine = false;
        if (Constants.roadsWithWhiteBound.Contains(roadType))
        {
            hasBoundLine = true;
        }


        int widthX = 1;
        int widthY = 1;
        if (direction == "x")
        {
            widthX = Convert.ToInt32(((roadWidthFactor) / this.tileSizeZ) / 2);
        }
        else if (direction == "z")
        {
            widthY = Convert.ToInt32(((roadWidthFactor) / this.tileSizeX) / 2);
        }

        foreach (Vector2 point in newPoints)
        {

            int tx = Convert.ToInt32(point.x);
            int ty = Convert.ToInt32(point.y);

            int boundXmin = 0;
            int boundXmax = terrainData.alphamapHeight - 1;
            int boundYmin = 0;
            int boundYmax = terrainData.alphamapHeight - 1;


            for (int i = 0; i < terrainData.alphamapLayers; i++)
            {

                for (int x = -widthX; x <= widthX; ++x)
                {
                    for (int y = -widthY; y <= widthY; ++y)
                    {
                        if (Methods.isInBounds(boundXmin, boundXmax, boundYmin, boundYmax, tx + x, ty + y) == true)
                        {

                            //Bojanje rubnika
                            if (Math.Abs(y) == widthY && direction == "z" && hasBoundLine)
                            {
                                splatmapData[tx + x, ty + y, i] = splatWeightsBound[i];
                            }
                            else if (Math.Abs(x) == widthX && direction == "x" && hasBoundLine)
                            {
                                splatmapData[tx + x, ty + y, i] = splatWeightsBound[i];
                            }

                            //Bojanje ceste
                            else
                            {
                                splatmapData[tx + x, ty + y, i] = splatWeights[i];
                            }

                        }
                    }
                }

            }


        }

    }

    //Metoda koja iz niza točaka(koordinata) boja srednju crtu na cesti
    void paintCenterRoadLinePoints(List<Vector2> points, string direction, string roadType, bool oneway, float roadWidthFactor, int lanes)
    {
        int length = 1; //Duljina crte = 1m
        int widthX = 1;
        int widthY = 1;
        float laneWidth = Constants.roadLanesWidthOptions[roadType];



        if (direction == "x")
        {
            widthX = Convert.ToInt32(((roadWidthFactor) / this.tileSizeZ) / 2);
            length = Convert.ToInt32((2f * terrainScaleFactor) / this.tileSizeZ);
        }
        else if (direction == "z")
        {
            widthY = Convert.ToInt32(((roadWidthFactor) / this.tileSizeX) / 2);
            length = Convert.ToInt32((2f * terrainScaleFactor) / this.tileSizeX);
        }



        float[] splatWeights = new float[terrainData.alphamapLayers];
        splatWeights[0] = 0f;
        splatWeights[1] = 0f;
        splatWeights[2] = 1f;

        int boundXmin = 0;
        int boundXmax = terrainData.alphamapHeight - 1;
        int boundYmin = 0;
        int boundYmax = terrainData.alphamapHeight - 1;

        int skipCounter = length;
        int counter = 0;
        //Isprekidane crte
        while (true)
        {
            if (counter >= points.Count)
            {
                break;
            }



            int tx = Convert.ToInt32(points[counter].x);
            int ty = Convert.ToInt32(points[counter].y);

            if (direction == "x")
            {

                for (int y = -widthY; y <= widthY; ++y)
                {
                    if (Methods.isInBounds(boundXmin, boundXmax, boundYmin, boundYmax, tx, ty) == true)
                    {
                        for (int i = 0; i < terrainData.alphamapLayers; i++)
                        {
                            if (lanes % 2 == 0)
                            {

                                float step = (1f / (lanes / 2f));
                                for (float l = -1f + step; l < 1f; l += step)
                                {
                                    int coef = (int)Math.Round(l * widthX);

                                    if (Methods.isInBounds(boundXmin, boundXmax, boundYmin, boundYmax, tx + coef, ty + y) == true)
                                    {
                                        splatmapData[tx + coef, ty + y, i] = splatWeights[i];
                                    }


                                }
                            }
                            else
                            {
                                float step = 2f * (1f / (lanes));
                                for (float l = -1f + step; l < 1f; l += step)
                                {
                                    int coef = (int)Math.Round(l * widthX);
                                    if (Methods.isInBounds(boundXmin, boundXmax, boundYmin, boundYmax, tx + coef, ty + y) == true)
                                    {
                                        splatmapData[tx + coef, ty + y, i] = splatWeights[i];
                                    }
                                }


                            }

                        }

                    }
                }
            }
            else
            {
                for (int x = -widthX; x <= widthX; ++x)
                {

                    for (int i = 0; i < terrainData.alphamapLayers; i++)
                    {
                        if (lanes % 2 == 0)
                        {

                            float step = (1f / (lanes / 2f));
                            for (float l = -1f + step; l < 1f; l += step)
                            {
                                int coef = (int)Math.Round(l * widthY);
                                if (Methods.isInBounds(boundXmin, boundXmax, boundYmin, boundYmax, tx + x, ty + coef) == true)
                                {
                                    splatmapData[tx + x, ty + coef, i] = splatWeights[i];
                                }


                            }
                        }
                        else
                        {
                            float step = 2f * (1f / (lanes));
                            for (float l = -1f + step; l < 1f; l += step)
                            {
                                int coef = (int)Math.Round(l * widthY);
                                if (Methods.isInBounds(boundXmin, boundXmax, boundYmin, boundYmax, tx + x, ty + coef) == true)
                                {
                                    splatmapData[tx + x, ty + coef, i] = splatWeights[i];
                                }
                            }


                        }

                    }

                }
            }


            skipCounter--;
            if (skipCounter == 0)
            {
                counter = counter + length;
                skipCounter = length;
            }
            else
            {
                counter++;
            }

        }




        //Puna crta
        if (!oneway)
        {
            foreach (Vector2 pt in points)
            {
                int tx = Convert.ToInt32(pt.x);
                int ty = Convert.ToInt32(pt.y);

                if (direction == "x")
                {

                    for (int y = -widthY; y <= widthY; ++y)
                    {

                        for (int i = 0; i < terrainData.alphamapLayers; i++)
                        {
                            if (lanes % 2 == 0)
                            {

                                if (Methods.isInBounds(boundXmin, boundXmax, boundYmin, boundYmax, tx, ty + y) == true)
                                {
                                    splatmapData[tx, ty + y, i] = splatWeights[i];
                                }

                            }
                            else
                            {
                                int coef = (int)Math.Round((1f / lanes) * widthX);
                                if (Methods.isInBounds(boundXmin, boundXmax, boundYmin, boundYmax, tx + coef, ty + y) == true)
                                {
                                    splatmapData[tx + coef, ty + y, i] = splatWeights[i];
                                }

                            }

                        }


                    }
                }
                else
                {
                    for (int x = -widthX; x <= widthX; ++x)
                    {

                        for (int i = 0; i < terrainData.alphamapLayers; i++)
                        {
                            if (lanes % 2 == 0)
                            {

                                if (Methods.isInBounds(boundXmin, boundXmax, boundYmin, boundYmax, tx + x, ty) == true)
                                {
                                    splatmapData[tx + x, ty, i] = splatWeights[i];
                                }
                            }
                            else
                            {
                                int coef = (int)Math.Round((1f / lanes) * widthY);
                                if (Methods.isInBounds(boundXmin, boundXmax, boundYmin, boundYmax, tx + x, ty + coef) == true)
                                {
                                    splatmapData[tx + x, ty + coef, i] = splatWeights[i];
                                }


                            }

                        }

                    }
                }
            }

        }



    }





    //Algoritam koji generira niz točaka koji predstavljaju liniju između dvije točke
    List<Vector2> Bresenham(Vector3 start, Vector3 end, int precision, bool integer)
    {
        int x = (int)(start.x);
        int y = (int)(start.z);
        int x2 = (int)(end.x);
        int y2 = (int)(end.z);
        float scale = 0f;
        if (!integer)
        {
            double dist = (Math.Sqrt((end.x - start.x) * (end.x - start.x) + (end.z - start.z) * (end.z - start.z)));
            scale = precision / (int)dist;
            x = (int)(start.x * scale);
            y = (int)(start.z * scale);
            x2 = (int)(end.x * scale);
            y2 = (int)(end.z * scale);
        }

        List<Vector2> pointsArray = new List<Vector2>();

        int w = x2 - x;
        int h = y2 - y;
        int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
        if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
        if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
        if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
        int longest = Math.Abs(w);
        int shortest = Math.Abs(h);
        if (!(longest > shortest))
        {
            longest = Math.Abs(h);
            shortest = Math.Abs(w);
            if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
            dx2 = 0;
        }
        int numerator = longest >> 1;
        for (int i = 0; i <= longest; i++)
        {
            if (integer)
            {
                pointsArray.Add(new Vector2((float)x, (float)y));
            }
            else
            {
                pointsArray.Add(new Vector2((float)x / scale, (float)y / scale));
            }


            numerator += shortest;
            if (!(numerator < longest))
            {
                numerator -= longest;
                x += dx1;
                y += dy1;
            }
            else
            {
                x += dx2;
                y += dy2;
            }
        }

        return pointsArray;
    }

    //Metoda za pronalazak objekta čvora na temelju njegovog id-a
    Node findNode(long nodeId)
    {
        foreach (Node n in nodes)
        {
            if (n.id == nodeId)
            {
                return n;
            }
        }
        return null;
    }





    //Vraćanje glavnih čvorova koje čine ceste
    public List<Node> getWayNodes()
    {
        return this.WayNodes;
    }




    //Metoda za bojanje terena pripadnom teksturom temeljem točaka poligona
    public void paintPolygon(MultiPolygon polygon, float[] splatWeights)
    {
        List<Vector2> terPositions = new List<Vector2>();
        foreach (long nodeID in polygon.refNodes)
        {
            Node n = findNode(nodeID);
            if (n != null)
            {
                //  InstantiateNode(n);
                terPositions.Add(new Vector2(n.terrainPosition.x, n.terrainPosition.y));
            }
        }

        List<float> boundingBox = Methods.getBoundingBox2D(terPositions);

        Int32 minX = Convert.ToInt32(boundingBox[0]);
        Int32 maxX = Convert.ToInt32(boundingBox[1]);
        Int32 minY = Convert.ToInt32(boundingBox[2]);
        Int32 maxY = Convert.ToInt32(boundingBox[3]);

        for (int i = minX; i <= maxX; ++i)
        {
            for (int j = minY; j <= maxY; ++j)
            {
                Vector2 currPos = new Vector2((float)i, (float)j);
                if (Methods.PointInPolygon2D(terPositions, currPos))
                {

                    int tx = Convert.ToInt32(currPos.x);
                    int ty = Convert.ToInt32(currPos.y);
                    if (Methods.isInBounds(minX, maxX, minY, maxY, tx, ty))
                    {
                        for (int k = 0; k < terrainData.alphamapLayers; k++)
                        {

                            splatmapData[tx, ty, k] = splatWeights[k];

                        }
                    }
                }
            }
        }
    }

    //Metoda za bojanje parkinga na terenu
    public void paintParking(MultiPolygon polygon, float[] asphaltSplatWeights, float[] lineSplatWeights, long id)
    {
        List<Vector2> terPositions = new List<Vector2>();
        foreach (long nodeID in polygon.refNodes)
        {
            Node n = findNode(nodeID);
            if (n != null)
            {
                //InstantiateNode(n, "parking "+id);
                terPositions.Add(new Vector2(n.terrainPosition.x, n.terrainPosition.y));
            }
        }

        List<int> minBoundingBox = Methods.getMinBoundingBox(terPositions, (float)terrainScaleFactor);
        int minX = minBoundingBox[0];
        int maxX = minBoundingBox[1];
        int minY = minBoundingBox[2];
        int maxY = minBoundingBox[3];

        float parkingLength = 5f * terrainScaleFactor;
        float parkingWidth = 2f * terrainScaleFactor;
        float parkingSpace = 5f * terrainScaleFactor;

        //Bojanje cijelog parkinga u asfalt
        for (int i = minX; i <= maxX; ++i)
        {
            for (int j = minY; j <= maxY; ++j)
            {

                Vector2 currPos = new Vector2((float)i, (float)j);
                int tx = Convert.ToInt32(currPos.x);
                int ty = Convert.ToInt32(currPos.y);

                if (Methods.isInBounds(minX, maxX, minY, maxY, tx, ty))
                {
                    for (int k = 0; k < terrainData.alphamapLayers; k++)
                    {
                        splatmapData[tx, ty, k] = asphaltSplatWeights[k];

                    }
                }

            }

        }

        //Bojanje horizontalnih crta između dijelova parkinga
        int passageSizeX = Convert.ToInt32((parkingLength) / tileSizeZ);
        int passageSizeZ = Convert.ToInt32((parkingLength) / tileSizeX);
        int horLineCnt = Convert.ToInt32((3 * parkingLength) / tileSizeX);
        int verticalLineCnt = Convert.ToInt32(parkingLength / tileSizeX);
        int widthCnt = Convert.ToInt32(parkingWidth / tileSizeZ);
        int spaceCnt = Convert.ToInt32(parkingSpace / tileSizeX);

        if (((maxY - passageSizeZ) - (minY + passageSizeZ)) <= horLineCnt)
        {
            passageSizeX = 0;
            passageSizeZ = 0;
        }


        for (int i = minX + passageSizeX; i <= maxX - passageSizeX; ++i)
        {
            for (int j = minY + passageSizeZ; j <= maxY - passageSizeZ; j = j + horLineCnt)
            {

                Vector2 currPos = new Vector2((float)i, (float)j);
                int tx = Convert.ToInt32(currPos.x);
                int ty = Convert.ToInt32(currPos.y);

                if (Methods.isInBounds(minX, maxX, minY, maxY, tx, ty))
                {
                    for (int k = 0; k < terrainData.alphamapLayers; k++)
                    {
                        splatmapData[tx, ty, k] = lineSplatWeights[k];


                    }
                }
            }

        }

        //Bojanje crta na parkingu  
        int tempVerticalCnt = verticalLineCnt;
        bool centralSpace = true;
        int parkingLotCounts = ((maxY - passageSizeZ) - (minY + passageSizeZ)) / horLineCnt;
        int parkingLots = 0;


        for (int i = minX + passageSizeX; i <= maxX - passageSizeX; i = i + widthCnt)
        {
            if (parkingLots == parkingLotCounts + 1) break;
            tempVerticalCnt = verticalLineCnt;
            centralSpace = true;
            for (int j = minY + passageSizeZ; j <= maxY - passageSizeZ; ++j)
            {

                Vector2 currPos = new Vector2((float)i, (float)j);

                int tx = Convert.ToInt32(currPos.x);
                int ty = Convert.ToInt32(currPos.y);

                if (Methods.isInBounds(minX, maxX, minY, maxY, tx, ty))
                {
                    for (int k = 0; k < terrainData.alphamapLayers; k++)
                    {
                        if (tempVerticalCnt > 0)
                        {
                            splatmapData[tx, ty, k] = lineSplatWeights[k];
                        }


                    }
                }

                tempVerticalCnt--;
                if (centralSpace)
                {
                    if (tempVerticalCnt == -spaceCnt)
                    {
                        tempVerticalCnt = verticalLineCnt;
                        centralSpace = false;
                    }
                }
                else
                {
                    if (tempVerticalCnt == 0)
                    {
                        tempVerticalCnt = verticalLineCnt;
                        centralSpace = true;
                    }
                }
            }


        }
        parkingLots++;

    }



    //Metoda koja saznaje nalazi li se točka unutar multi poligona
    public bool isPointInPolygon(Vector2 testVec, List<MultiPolygon> polygons)
    {
        bool inside = false;

        foreach (MultiPolygon poly in manMade)
        {
            if (poly.type == "bridge")
            {
                List<Vector2> polyPos2D = new List<Vector2>();

                foreach (long nodeID in poly.refNodes)
                {
                    Node n = findNode(nodeID);
                    if (n != null)
                    {
                        polyPos2D.Add(new Vector2(n.position.x, n.position.z));
                    }
                }
                if (Methods.PointInPolygon2D(polyPos2D, testVec))
                {
                    return true;
                }
            }
        }


        return inside;
    }

    //Dobivanje širine ceste
    public float generateRoadWidthFactor(string roadType, int roadLanes)
    {
        float laneWidth = 2.5f;
        if (Constants.roadLanesWidthOptions.ContainsKey(roadType))
        {
            laneWidth = Constants.roadLanesWidthOptions[roadType];
        }
        else if (Constants.pathWidthOptions.ContainsKey(roadType))
        {
            laneWidth = Constants.pathWidthOptions[roadType];
        }

        float roadWidthFactor = laneWidth * roadLanes * (float)terrainScaleFactor;

        return roadWidthFactor;
    }


}
