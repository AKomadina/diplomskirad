﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class StreetObjectsGenerator : MonoBehaviour
{
    private List<Node> objects = new List<Node>();
    private List<Node> nodes = new List<Node>();
    private double terrainScaleFactor;
    private List<GameObject> streetGameObjects;
    private List<Vector2> allRoadPoints;
    private List<Vector2> trafficRoadPoints;
    private List<Vector2> roadBoundPoints = new List<Vector2>();
    private List<Vector2> allRoadBoundPoints = new List<Vector2>();
    Dictionary<Vector2, float> roadNodeWidthDict = new Dictionary<Vector2, float>();

    private Terrain terrain;

    public GameObject toiletsObject;
    public GameObject atmObject;
    public GameObject clockObject;
    public GameObject postBoxObject;
    public GameObject telephoneObject;
    public GameObject parkingMachineObject;
    public GameObject fireHydrantObject;
    public GameObject bicycleParkingObject;
    public GameObject recycleGlassObject;
    public GameObject recyclePaperObject;
    public GameObject recyclePlasticObject;
    public GameObject flagPoleObjectCRO;
    public GameObject flagPoleObjectZG;
    public GameObject bollardObject;
    public GameObject streetLampObject;
    public GameObject liftGateObject;    
    public GameObject drinkingWaterObject;
    public GameObject kioskObject;
    public GameObject busStopObject;
    public GameObject memorialObject;
    public GameObject NodeObject;
    public GameObject LampObject;
    public GameObject BumpObject;
    public GameObject IslandObject;

    public GameObject trafficSignalsObject;
    public GameObject stopObject;
    public GameObject giveWayObject;
    public GameObject busSignObject;
    public GameObject addressSignObject;
    public GameObject onewaySignObject;
    public GameObject speedSignObject;
    public GameObject speedTextObject;
    public GameObject addressTextObject;
    public GameObject taxiSignObject;

    private int flagPoleCounter = 0;

    private List<Vector3> trafficObjectPositions = new List<Vector3>();

   

    public void Generate(GameObject terrain, List<Node> nodes, List<Node> objects, List<Vector2> allRoadPoints, List<Vector2> trafficRoadPoints,List<Vector2> roadBoundPoints,List<Vector2> wayBoundPoints, List<Vector2> allRoadBoundPoints,List<Way> roads, Dictionary<long, List<Vector2>> allBoundPointsDict, Dictionary<Vector2, float> roadNodeWidthDict, double terrainScaleFactor)
    {
        streetGameObjects = new List<GameObject>();
        this.objects = objects;
        this.nodes = nodes;
        this.terrainScaleFactor = terrainScaleFactor;
        this.allRoadPoints = allRoadPoints;
        this.terrain = terrain.GetComponent<Terrain>();
        this.roadBoundPoints = roadBoundPoints;
        this.allRoadBoundPoints = allRoadBoundPoints;
        this.trafficRoadPoints = trafficRoadPoints;
        this.roadNodeWidthDict = roadNodeWidthDict;


        //Stvaranje osm objekata
        foreach (Node streetObject in objects)
        {
            InstantiateObjects(streetObject);
        }

        //Stvaranje znakova na cesti za jednosmjernu ulicu, ograničenje brzine i adresu ulice
        foreach(Way w in roads)
        {
            int nodeCnt = w.refNodes.Count;
            long nodeId = 0;
            if(nodeCnt < 2)
            {
                continue;
            }
            else
            {
                nodeId = w.refNodes[nodeCnt / 2];
            }
            
            Node n = findNode(nodeId);
            if (n == null ) continue;
            Vector3 signPos = n.position;

            if (w.roadName != "unknown")
            {
                List<Vector2> roadBoundPointsTemp = new List<Vector2>();
                List<Vector2> roadBoundPointsTemp2 = new List<Vector2>();
                if (!Constants.roadsWithTrafficObjects.Contains(w.roadType))
                {
                    roadBoundPointsTemp = new List<Vector2>(wayBoundPoints);
                    roadBoundPointsTemp2 = new List<Vector2>(wayBoundPoints);


                }
                else
                {
                    roadBoundPointsTemp = new List<Vector2>(roadBoundPoints);
                    roadBoundPointsTemp2 = new List<Vector2>(roadBoundPoints);
                }

                Vector3 closestPoint = new Vector3(0f, 0f, 0f);
                while (true)
                {
                    closestPoint = Methods.ClosestRoad(signPos, roadBoundPointsTemp, this.terrain);
                    if (checkIfSpaceEmpty(closestPoint, this.trafficObjectPositions) == true)
                    {
                        roadBoundPointsTemp.Remove(new Vector2(closestPoint.x, closestPoint.z));
                        break;
                    }
                    roadBoundPointsTemp.Remove(new Vector2(closestPoint.x, closestPoint.z));
                }

                roadBoundPointsTemp2.Remove(new Vector2(closestPoint.x, closestPoint.z));
                Vector3 closestPoint2 = Methods.ClosestRoad(closestPoint, roadBoundPointsTemp2, this.terrain);
                roadBoundPointsTemp2.Remove(new Vector2(closestPoint2.x, closestPoint2.z));
                string direction = Methods.findDirectionVector(closestPoint, closestPoint2);

                string name = w.roadName;
                GameObject nodeObj = Instantiate(addressSignObject);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                nodeObjT.localPosition = closestPoint;
                nodeObjT.rotation = Quaternion.LookRotation(closestPoint2 - closestPoint, Vector3.forward);
                nodeObjT.eulerAngles = new Vector3(
                     0f,
                     nodeObjT.eulerAngles.y,
                     0f
                );
                this.trafficObjectPositions.Add(nodeObjT.position);
                nodeObj.name = "Adresni znak " + w.id;


                GameObject nodeObj2 = Instantiate(addressTextObject);
                Transform nodeObjT2 = nodeObj2.transform;
                nodeObjT2.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                nodeObjT2.localPosition = new Vector3(closestPoint.x, closestPoint.y + 1.7f * (float)terrainScaleFactor, closestPoint.z);
                nodeObjT2.eulerAngles = new Vector3(
                     0f,
                     nodeObjT.eulerAngles.y,
                     0f
                );
                nodeObjT2.position += generateTextTransVector(direction, nodeObjT.eulerAngles.y);
                TMPro.TextMeshProUGUI text = nodeObj2.GetComponent<TMPro.TextMeshProUGUI>();
                text.text = w.roadName;
                nodeObj2.name = "Address text 1 " + w.id + w.roadName;


                GameObject nodeObj3 = Instantiate(addressTextObject);
                Transform nodeObjT3 = nodeObj3.transform;
                nodeObjT3.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                nodeObjT3.localPosition = new Vector3(closestPoint.x, closestPoint.y + 1.7f * (float)terrainScaleFactor, closestPoint.z);
                nodeObjT3.eulerAngles = new Vector3(
                     0f,
                     nodeObjT.eulerAngles.y + 180f,
                     0f
                );
                nodeObjT3.position -= generateTextTransVector(direction, nodeObjT.eulerAngles.y);
                text = nodeObj3.GetComponent<TMPro.TextMeshProUGUI>();
                text.text = w.roadName;
                nodeObj3.name = "Address text 2 " + w.id + w.roadName;



            }
            if (w.roadOneWay)
            {
                List<Vector2> roadBoundPointsTemp = new List<Vector2>();
                List<Vector2> roadBoundPointsTemp2 = new List<Vector2>();
                if (!Constants.roadsWithTrafficObjects.Contains(w.roadType))
                {
                    roadBoundPointsTemp = new List<Vector2>(wayBoundPoints);
                    roadBoundPointsTemp2 = new List<Vector2>(wayBoundPoints);


                }
                else
                {
                    roadBoundPointsTemp = new List<Vector2>(roadBoundPoints);
                    roadBoundPointsTemp2 = new List<Vector2>(roadBoundPoints);
                }
               
    
                    Vector3 closestPoint = new Vector3(0f, 0f, 0f);
                    while (true)
                    {
                        closestPoint = Methods.ClosestRoad(signPos, roadBoundPointsTemp, this.terrain);
                        if (checkIfSpaceEmpty(closestPoint, this.trafficObjectPositions) == true)
                        {
                            roadBoundPointsTemp.Remove(new Vector2(closestPoint.x, closestPoint.z));
                            break;
                        }
                        roadBoundPointsTemp.Remove(new Vector2(closestPoint.x, closestPoint.z));
                    }
                    roadBoundPointsTemp2.Remove(new Vector2(closestPoint.x, closestPoint.z));
                    Vector3 closestPoint2 = Methods.ClosestRoad(closestPoint, roadBoundPointsTemp2, this.terrain);
                    roadBoundPointsTemp2.Remove(new Vector2(closestPoint2.x, closestPoint2.z));


                    GameObject nodeObj = Instantiate(onewaySignObject);
                    Transform nodeObjT = nodeObj.transform;
                    nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                    nodeObjT.localPosition = closestPoint;
                    nodeObjT.rotation = Quaternion.LookRotation(closestPoint2 - closestPoint, Vector3.forward);
                    nodeObjT.eulerAngles = new Vector3(
                         0f,
                         nodeObjT.eulerAngles.y,
                         0f
                    );
                    this.trafficObjectPositions.Add(nodeObjT.position);
                    nodeObj.name = "Jednosmjerni znak " + w.id;
                
               

            }
            if (w.roadMaxSpeed != "unknown")
            {
                List<Vector2> roadBoundPointsTemp = new List<Vector2>();
                List<Vector2> roadBoundPointsTemp2 = new List<Vector2>();
                if (!Constants.roadsWithTrafficObjects.Contains(w.roadType))
                {
                    roadBoundPointsTemp = new List<Vector2>(wayBoundPoints);
                    roadBoundPointsTemp2 = new List<Vector2>(wayBoundPoints);


                }
                else
                {
                    roadBoundPointsTemp = new List<Vector2>(roadBoundPoints);
                    roadBoundPointsTemp2 = new List<Vector2>(roadBoundPoints);
                }
              
                    
                    Vector3 closestPoint = new Vector3(0f, 0f, 0f);
                    while (true)
                    {
                        closestPoint = Methods.ClosestRoad(signPos, roadBoundPointsTemp, this.terrain);
                        if (checkIfSpaceEmpty(closestPoint, this.trafficObjectPositions) == true)
                        {
                            roadBoundPointsTemp.Remove(new Vector2(closestPoint.x, closestPoint.z));
                            break;
                        }
                        roadBoundPointsTemp.Remove(new Vector2(closestPoint.x, closestPoint.z));
                    }
                   
                    roadBoundPointsTemp2.Remove(new Vector2(closestPoint.x, closestPoint.z));
                    Vector3 closestPoint2 = Methods.ClosestRoad(closestPoint, roadBoundPointsTemp2, this.terrain);
                    roadBoundPointsTemp2.Remove(new Vector2(closestPoint2.x, closestPoint2.z));
                    string direction = Methods.findDirectionVector(closestPoint, closestPoint2);

                    int maxSpeed = Int32.Parse(w.roadMaxSpeed);
                    GameObject nodeObj = Instantiate(speedSignObject);
                    Transform nodeObjT = nodeObj.transform;
                    nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                    nodeObjT.localPosition = new Vector3(closestPoint.x, closestPoint.y, closestPoint.z);
                    nodeObjT.rotation = Quaternion.LookRotation(closestPoint2 - closestPoint, Vector3.forward);
                    nodeObjT.eulerAngles = new Vector3(
                         0f,
                         nodeObjT.eulerAngles.y,
                         0f
                    );
                    this.trafficObjectPositions.Add(nodeObjT.position);
                    nodeObj.name = "Ogranicenje znak" + w.id;

                    GameObject nodeObj2 = Instantiate(speedTextObject);
                    Transform nodeObjT2 = nodeObj2.transform;
                    nodeObjT2.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                    nodeObjT2.localPosition = new Vector3(closestPoint.x, closestPoint.y + 1.75f * (float)terrainScaleFactor, closestPoint.z);
                    nodeObjT2.eulerAngles = new Vector3(
                         0f,
                         nodeObjT.eulerAngles.y,
                         0f
                    );
                    nodeObjT2.position += generateTextTransVector(direction, nodeObjT.eulerAngles.y);
                    TMPro.TextMeshProUGUI text = nodeObj2.GetComponent<TMPro.TextMeshProUGUI>();
                    text.text = w.roadMaxSpeed;
                    nodeObj2.name = "Speed text 1 " + w.id;

                    GameObject nodeObj3 = Instantiate(speedTextObject);
                    Transform nodeObjT3 = nodeObj3.transform;
                    nodeObjT3.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                    nodeObjT3.localPosition = new Vector3(closestPoint.x, closestPoint.y + 1.75f * (float)terrainScaleFactor, closestPoint.z);
                    nodeObjT3.eulerAngles = new Vector3(
                         0f,
                         nodeObjT.eulerAngles.y + 180f,
                         0f
                    );
                    nodeObjT3.position -= generateTextTransVector(direction, nodeObjT.eulerAngles.y);
                    text = nodeObj3.GetComponent<TMPro.TextMeshProUGUI>();
                    text.text = w.roadMaxSpeed;
                    nodeObj3.name = "Speed text 2 " + w.id;
                
             

            }
           

            //Stvaranje ulične rasvjete
            if (w.roadLit)
            {
                if (!allBoundPointsDict.ContainsKey(w.id))
                {
                    continue;
                }
                int step = 5;
                if (!Constants.roadsWithTrafficObjects.Contains(w.roadType))
                {
                    step = 7;
                }
 
                List<Vector2> trafficBoundPoints = allBoundPointsDict[w.id];
                for (int i = 0; i < trafficBoundPoints.Count; i = i + step)
                {

                    Vector3 vec = new Vector3(trafficBoundPoints[i].x, 0f, trafficBoundPoints[i].y);
                    float heightY = this.terrain.SampleHeight(vec);
                    vec.y = heightY;

                    if (checkIfSpaceEmpty(vec, this.trafficObjectPositions) == false)
                    {
                        continue;
                    }


                    if (Constants.roadsWithTrafficObjects.Contains(w.roadType))
                    {
                        GameObject nodeObj = Instantiate(streetLampObject);
                        Transform nodeObjT = nodeObj.transform;
                        nodeObjT.localScale = new Vector3(1.65f * (float)terrainScaleFactor, 1.65f * (float)terrainScaleFactor, 1.65f * (float)terrainScaleFactor);
                        nodeObjT.localPosition = vec;


                        Vector3 closestRoad = Methods.ClosestRoad(nodeObjT.localPosition, this.trafficRoadPoints, this.terrain);
                        nodeObjT.rotation = Quaternion.LookRotation(closestRoad - nodeObjT.localPosition, Vector3.forward);
                        nodeObjT.eulerAngles = new Vector3(
                         nodeObjT.eulerAngles.x,
                         nodeObjT.eulerAngles.y + 90f,
                         0f
                        );
                        this.trafficObjectPositions.Add(nodeObjT.position);
                        nodeObj.name = "Velika rasvjeta";


                    }
                    else
                    {
                        GameObject nodeObj = Instantiate(LampObject);
                        Transform nodeObjT = nodeObj.transform;
                        nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                        nodeObjT.localPosition = vec;


                        Vector3 closestRoad = Methods.ClosestRoad(nodeObjT.localPosition, this.trafficRoadPoints, this.terrain);
                        nodeObjT.rotation = Quaternion.LookRotation(closestRoad - nodeObjT.localPosition, Vector3.forward);
                        nodeObjT.eulerAngles = new Vector3(
                         nodeObjT.eulerAngles.x,
                         nodeObjT.eulerAngles.y + 90f,
                         0f
                        );
                        this.trafficObjectPositions.Add(nodeObjT.position);
                        nodeObj.name = "Mala rasvjeta";
                    }
                }
            }
            

        }


        
    }



    //Metoda za stvaranje uličnih objekta
    void InstantiateObjects(Node node)
    {
        string type = node.type;

        if (type == "atm")
        {
            GameObject nodeObj = Instantiate(atmObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(0.945f * (float)terrainScaleFactor, 0.945f * (float)terrainScaleFactor, 0.945f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;

            Vector3 closestRoad = Methods.ClosestRoad(nodeObjT.localPosition, this.allRoadPoints, this.terrain);
            nodeObjT.rotation = Quaternion.LookRotation(closestRoad - nodeObjT.localPosition, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
               nodeObjT.eulerAngles.x,
               nodeObjT.eulerAngles.y,
               0f
           );

            nodeObj.name = "Bankomat " + node.id;
        }
        else if (type == "toilets")
        {
            GameObject nodeObj = Instantiate(toiletsObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            nodeObjT.localPosition = new Vector3(node.position.x , node.position.y , node.position.z );
            nodeObj.name = "WC " + node.id;
        }
        else if (type == "fire_hydrant")
        {
            GameObject nodeObj = Instantiate(fireHydrantObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(2.25f * (float)terrainScaleFactor, 2.25f * (float)terrainScaleFactor, 2.25f * (float)terrainScaleFactor);
            nodeObjT.localPosition = new Vector3(node.position.x + (float)(0.5f * 0.18f * terrainScaleFactor), node.position.y + (float)(0.5f * 0.6f * terrainScaleFactor), node.position.z + (float)(0.5f * 0.02f * terrainScaleFactor));
            nodeObj.name = "Hidrant " + node.id;
        }
        else if (type == "telephone")
        {
            GameObject nodeObj = Instantiate(telephoneObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(37f * (float)terrainScaleFactor, 37f * (float)terrainScaleFactor, 37f * (float)terrainScaleFactor);
            nodeObjT.localPosition = new Vector3(node.position.x + (float)(1.8f * 0.1f * terrainScaleFactor), node.position.y, node.position.z);

            Vector3 closestRoad = Methods.ClosestRoad(nodeObjT.localPosition, this.allRoadPoints, this.terrain);
            nodeObjT.rotation = Quaternion.LookRotation(closestRoad - nodeObjT.localPosition, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
              nodeObjT.eulerAngles.x,
              nodeObjT.eulerAngles.y + 90f,
              0f
          );

            nodeObj.name = "Telefon " + node.id;
        }
        else if (type == "parking_machine")
        {
            GameObject nodeObj = Instantiate(parkingMachineObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(0.256f * (float)terrainScaleFactor, 0.256f * (float)terrainScaleFactor, 0.256f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;

            Vector3 closestRoad = Methods.ClosestRoad(nodeObjT.localPosition, this.allRoadPoints, this.terrain);
            nodeObjT.rotation = Quaternion.LookRotation(closestRoad - nodeObjT.localPosition, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
              nodeObjT.eulerAngles.x,
              nodeObjT.eulerAngles.y + 90f,
              0f
          );

            nodeObj.name = "Parking automat " + node.id;
        }
        else if (type == "post_box")
        {
            GameObject nodeObj = Instantiate(postBoxObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(0.85f * (float)terrainScaleFactor, 0.85f * (float)terrainScaleFactor, 0.85f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Poštanski sandučić " + node.id;
        }
        else if (type == "clock")
        {
            GameObject nodeObj = Instantiate(clockObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Sat " + node.id;
        }

        else if (type == "bicycle_parking")
        {
            GameObject nodeObj = Instantiate(bicycleParkingObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(0.1f * (float)terrainScaleFactor, 0.1f * (float)terrainScaleFactor, 0.1f * (float)terrainScaleFactor);
            nodeObjT.localPosition = new Vector3(node.position.x + (float)(2.51f * terrainScaleFactor), node.position.y, node.position.z - (float)(0.621f * terrainScaleFactor));

            nodeObj.name = "Parking bicikl " + node.id;
        }
        else if (type == "recycling_plastic")
        {
            GameObject nodeObj = Instantiate(recyclePlasticObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;

            Vector3 closestRoad = Methods.ClosestRoad(nodeObjT.localPosition, this.allRoadPoints, this.terrain);
            nodeObjT.rotation = Quaternion.LookRotation(closestRoad - nodeObjT.localPosition, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
             nodeObjT.eulerAngles.x,
             nodeObjT.eulerAngles.y,
             0f
         );

            nodeObj.name = "Kanta plastika " + node.id;
        }
        else if (type == "recycling_paper")
        {
            GameObject nodeObj = Instantiate(recyclePaperObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;

            Vector3 closestRoad = Methods.ClosestRoad(nodeObjT.localPosition, this.allRoadPoints, this.terrain);
            nodeObjT.rotation = Quaternion.LookRotation(closestRoad - nodeObjT.localPosition, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
             nodeObjT.eulerAngles.x,
             nodeObjT.eulerAngles.y,
             0f
         );

            nodeObj.name = "Kanta papir " + node.id;
        }
        else if (type == "recycling_glass")
        {
            GameObject nodeObj = Instantiate(recycleGlassObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;

            Vector3 closestRoad = Methods.ClosestRoad(nodeObjT.localPosition, this.allRoadPoints, this.terrain);
            nodeObjT.rotation = Quaternion.LookRotation(closestRoad - nodeObjT.localPosition, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
             nodeObjT.eulerAngles.x,
             nodeObjT.eulerAngles.y,
             0f
         );

            nodeObj.name = "Kanta staklo " + node.id;
        }
        else if (type == "flagpole")
        {
            if (flagPoleCounter % 2 == 0)
            {
                GameObject nodeObj = Instantiate(flagPoleObjectCRO);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3(25f * (float)terrainScaleFactor, 25f * (float)terrainScaleFactor, 25f * (float)terrainScaleFactor);
                nodeObjT.localPosition = new Vector3(node.position.x + (float)(0.15f * terrainScaleFactor), node.position.y, node.position.z);
                nodeObj.name = "Zastava " + node.id;
            }
            else
            {
                GameObject nodeObj = Instantiate(flagPoleObjectZG);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3(25f * (float)terrainScaleFactor, 25f * (float)terrainScaleFactor, 25f * (float)terrainScaleFactor);
                nodeObjT.localPosition = new Vector3(node.position.x + (float)(0.15f * terrainScaleFactor), node.position.y, node.position.z);
                nodeObj.name = "Zastava " + node.id;
            }
            flagPoleCounter++;

        }
        else if (type == "bollard")
        {
            GameObject nodeObj = Instantiate(bollardObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(0.5f * (float)terrainScaleFactor, 0.5f * (float)terrainScaleFactor, 0.5f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Stupic " + node.id;
        }
        else if (type == "drinking_water")
        {
            GameObject nodeObj = Instantiate(drinkingWaterObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(0.01f * (float)terrainScaleFactor, 0.01f * (float)terrainScaleFactor, 0.01f * (float)terrainScaleFactor);
            nodeObjT.localPosition = new Vector3(node.position.x - (float)(0.02f * terrainScaleFactor), node.position.y + (float)(0.12f * terrainScaleFactor), node.position.z);
            nodeObj.name = "Zdenac " + node.id;
        }
        else if (type == "kiosk")
        {
            GameObject nodeObj = Instantiate(kioskObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(100f * (float)terrainScaleFactor, 100f * (float)terrainScaleFactor, 100f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;

            Vector3 closestRoad = Methods.ClosestRoad(nodeObjT.localPosition, this.allRoadPoints, this.terrain);
            nodeObjT.rotation = Quaternion.LookRotation(closestRoad - nodeObjT.localPosition, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
             nodeObjT.eulerAngles.x - 90f,
             nodeObjT.eulerAngles.y,
             0f
         );

            nodeObj.name = "Kiosk " + node.id;
        }
        else if (type == "memorial")
        {
            GameObject nodeObj = Instantiate(memorialObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(0.007f * (float)terrainScaleFactor, 0.007f * (float)terrainScaleFactor, 0.007f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;

            Vector3 closestRoad = Methods.ClosestRoad(nodeObjT.localPosition, allRoadPoints, this.terrain);
            nodeObjT.rotation = Quaternion.LookRotation(closestRoad - nodeObjT.localPosition, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
             nodeObjT.eulerAngles.x - 90f,
             nodeObjT.eulerAngles.y,
             0f
         );

            nodeObj.name = "Statua " + node.id;
        }
        else if (type.Contains("bus_stop"))
        {
            

            if (type == "bus_stop_yes")
            {
                List<Vector2> roadBoundPointsTemp = new List<Vector2>(allRoadBoundPoints);
                Vector3 closestPoint = Methods.ClosestRoad(node.position, roadBoundPointsTemp, this.terrain);
                roadBoundPointsTemp.Remove(new Vector2(closestPoint.x, closestPoint.z));
                Vector3 closestPoint2 = Methods.ClosestRoad(closestPoint, roadBoundPointsTemp, this.terrain);
                roadBoundPointsTemp.Remove(new Vector2(closestPoint2.x, closestPoint2.z));

                GameObject nodeObj = Instantiate(busStopObject);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3(0.8f * (float)terrainScaleFactor, 0.8f * (float)terrainScaleFactor, 0.8f * (float)terrainScaleFactor);
                nodeObjT.localPosition = node.position;
                nodeObjT.rotation = Quaternion.LookRotation(closestPoint2 - closestPoint, Vector3.forward);
                nodeObjT.eulerAngles = new Vector3(
                 0f,
                 nodeObjT.eulerAngles.y +180f,
                 0f
             );
                nodeObj.name = "Bus stop with shelter" + node.id;
            }
            else
            {
                List<Vector2> roadBoundPointsTemp = new List<Vector2>(allRoadBoundPoints);
                Vector3 closestPoint = Methods.ClosestRoad(node.position, roadBoundPointsTemp, this.terrain);
                roadBoundPointsTemp.Remove(new Vector2(closestPoint.x, closestPoint.z));
                Vector3 closestPoint2 = Methods.ClosestRoad(node.position, roadBoundPointsTemp, this.terrain);
                roadBoundPointsTemp.Remove(new Vector2(closestPoint2.x, closestPoint2.z));

                GameObject nodeObj = Instantiate(busSignObject);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                nodeObjT.localPosition = closestPoint;
                nodeObjT.rotation = Quaternion.LookRotation(closestPoint2 - closestPoint, Vector3.forward);
                nodeObjT.eulerAngles = new Vector3(
                 0f,
                 nodeObjT.eulerAngles.y,
                 0f
                );
                this.trafficObjectPositions.Add(nodeObjT.position);
                nodeObj.name = "Bus stop without shelter" + node.id;
            }
           
        }

        else if (type == "street_lamp")
        {
            GameObject nodeObj = Instantiate(streetLampObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(1.65f * (float)terrainScaleFactor, 1.65f * (float)terrainScaleFactor, 1.65f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;

            Vector3 closestRoad = Methods.ClosestRoad(nodeObjT.localPosition, this.allRoadBoundPoints, this.terrain);
            nodeObjT.rotation = Quaternion.LookRotation(closestRoad - nodeObjT.localPosition, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
             nodeObjT.eulerAngles.x,
             nodeObjT.eulerAngles.y + 90f,
             0f
         );
            this.trafficObjectPositions.Add(nodeObjT.position);
            nodeObj.name = "Ulicna rasvjeta " + node.id;
        }
        else if (type == "lift_gate")
        {

            GameObject nodeObj = Instantiate(liftGateObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            nodeObjT.localPosition = new Vector3(node.position.x, node.position.y, node.position.z);

            List<Vector2> allRoadPts = new List<Vector2>(this.allRoadPoints);
            Vector3 closestRoad = Methods.ClosestRoad(nodeObjT.localPosition, allRoadPts, this.terrain);
            allRoadPts.Remove(new Vector2(closestRoad.x, closestRoad.z));
            Vector3 closestRoad2 = Methods.SecondClosestRoad(nodeObjT.localPosition, allRoadPts, new Vector2(closestRoad.x, closestRoad.z), this.terrain, terrainScaleFactor);
            Vector3 cent = Methods.getCentroid(new Vector3[] { closestRoad, closestRoad2 });
            nodeObjT.position = new Vector3(cent.x, cent.y, cent.z);

            nodeObjT.eulerAngles = new Vector3(
                  nodeObjT.eulerAngles.x,
                  Quaternion.LookRotation(closestRoad - nodeObjT.localPosition, Vector3.forward).eulerAngles.y + 90f,
                  nodeObjT.eulerAngles.z
              );

            nodeObj.name = "Ulazna rampa " + node.id;
        }
        else if (type == "traffic_signals")
        {
            List<Vector2> roadBoundPointsTemp = new List<Vector2>(roadBoundPoints);

            for (int i = 0; i < 4; ++i)
            {
                Vector3 closestPoint = Methods.ClosestRoad(node.position, roadBoundPointsTemp, this.terrain);
                roadBoundPointsTemp.Remove(new Vector2(closestPoint.x, closestPoint.z));
                Vector3 closestPoint2 = Methods.ClosestRoad(closestPoint, roadBoundPointsTemp, this.terrain);
                roadBoundPointsTemp.Remove(new Vector2(closestPoint2.x, closestPoint2.z));


                GameObject nodeObj = Instantiate(trafficSignalsObject);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3(0.68f * (float)terrainScaleFactor, 0.68f * (float)terrainScaleFactor, 0.68f * (float)terrainScaleFactor);
                nodeObjT.localPosition = closestPoint;
                Quaternion q1 = Quaternion.LookRotation(closestPoint2 - closestPoint, Vector3.forward);
                nodeObjT.rotation = q1;
                nodeObjT.eulerAngles = new Vector3(
                 0f,
                 nodeObjT.eulerAngles.y,
                 0f
                );
                this.trafficObjectPositions.Add(nodeObjT.position);

                nodeObj.name = "Semafor " + node.id ;

            }
            


        }
        else if (type == "stop")
        {
            List<Vector2> roadBoundPointsTemp = new List<Vector2>(roadBoundPoints);
            Vector3 closestPoint = Methods.ClosestRoad(node.position, roadBoundPoints, this.terrain);
            roadBoundPointsTemp.Remove(new Vector2(closestPoint.x, closestPoint.z));
            Vector3 closestPoint2 = Methods.ClosestRoad(closestPoint, roadBoundPointsTemp, this.terrain);
            roadBoundPointsTemp.Remove(new Vector2(closestPoint2.x, closestPoint2.z));

            GameObject nodeObj = Instantiate(stopObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(100f * (float)terrainScaleFactor, 100f * (float)terrainScaleFactor, 100f * (float)terrainScaleFactor);
            nodeObjT.localPosition = closestPoint;
            nodeObjT.rotation = Quaternion.LookRotation(closestPoint2 - closestPoint, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
             0f,
             nodeObjT.eulerAngles.y,
             0f
            );
            this.trafficObjectPositions.Add(nodeObjT.position);
            nodeObj.name = "Znak STOP " + node.id;

        }
        else if (type == "give_way")
        {
            List<Vector2> roadBoundPointsTemp = new List<Vector2>(roadBoundPoints);
            Vector3 closestPoint = Methods.ClosestRoad(node.position, roadBoundPoints, this.terrain);
            roadBoundPointsTemp.Remove(new Vector2(closestPoint.x, closestPoint.z));
            Vector3 closestPoint2 = Methods.ClosestRoad(closestPoint, roadBoundPointsTemp, this.terrain);
            roadBoundPointsTemp.Remove(new Vector2(closestPoint2.x, closestPoint2.z));

            GameObject nodeObj = Instantiate(giveWayObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            nodeObjT.localPosition = closestPoint;
            nodeObjT.rotation = Quaternion.LookRotation(closestPoint2 - closestPoint, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
             0f,
             nodeObjT.eulerAngles.y,
             0f
            );
            this.trafficObjectPositions.Add(nodeObjT.position);
            nodeObj.name = "Znak Sporedno " + node.id;

        }
        else if (type == "taxi")
        {
            List<Vector2> roadBoundPointsTemp = new List<Vector2>(roadBoundPoints);
            Vector3 closestPoint = Methods.ClosestRoad(node.position, roadBoundPoints, this.terrain);
            roadBoundPointsTemp.Remove(new Vector2(closestPoint.x, closestPoint.z));
            Vector3 closestPoint2 = Methods.ClosestRoad(closestPoint, roadBoundPointsTemp, this.terrain);
            roadBoundPointsTemp.Remove(new Vector2(closestPoint2.x, closestPoint2.z));

            GameObject nodeObj = Instantiate(taxiSignObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            nodeObjT.localPosition = closestPoint;
            nodeObjT.rotation = Quaternion.LookRotation(closestPoint2 - closestPoint, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
             0f,
             nodeObjT.eulerAngles.y,
             0f
            );
            this.trafficObjectPositions.Add(nodeObjT.position);
            nodeObj.name = "Znak TAXI " + node.id;

        }
        else if (type == "bump")
        {
            List<Vector2> roadPointsTemp = new List<Vector2>(trafficRoadPoints);
            Vector3 closestPoint = Methods.ClosestRoad(node.position, roadPointsTemp, this.terrain);
            roadPointsTemp.Remove(new Vector2(closestPoint.x, closestPoint.z));
            Vector3 closestPoint2 = Methods.ClosestRoad(closestPoint, roadPointsTemp, this.terrain);
            roadPointsTemp.Remove(new Vector2(closestPoint2.x, closestPoint2.z));
            Vector3 closestRoad = Methods.ClosestRoad(node.position, new List<Vector2>(this.roadNodeWidthDict.Keys), this.terrain);
            float roadWidth = this.roadNodeWidthDict[new Vector2(closestRoad.x, closestRoad.z)] * 0.75f;
          
            
            GameObject nodeObj = Instantiate(BumpObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(roadWidth * (float)terrainScaleFactor, 3f * (float)terrainScaleFactor, 1.5f * (float)terrainScaleFactor);
            nodeObjT.localPosition = new Vector3(closestPoint.x, closestPoint.y + 0.05f*(float)terrainScaleFactor, closestPoint.z);
            nodeObjT.rotation = Quaternion.LookRotation(closestPoint2 - closestPoint, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
             0f,
             nodeObjT.eulerAngles.y,
             0f
            );
            nodeObj.name = "Ležeči policajac " + node.id;

        }
        else if (type == "hump")
        {
            List<Vector2> roadPointsTemp = new List<Vector2>(trafficRoadPoints);
            Vector3 closestPoint = Methods.ClosestRoad(node.position, roadPointsTemp, this.terrain);
            roadPointsTemp.Remove(new Vector2(closestPoint.x, closestPoint.z));
            Vector3 closestPoint2 = Methods.ClosestRoad(closestPoint, roadPointsTemp, this.terrain);
            roadPointsTemp.Remove(new Vector2(closestPoint2.x, closestPoint2.z));
            Vector3 closestRoad = Methods.ClosestRoad(node.position, new List<Vector2>(this.roadNodeWidthDict.Keys), this.terrain);
            float roadWidth = this.roadNodeWidthDict[new Vector2(closestRoad.x, closestRoad.z)] * 0.75f;


            GameObject nodeObj = Instantiate(BumpObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(roadWidth * (float)terrainScaleFactor, roadWidth * (float)terrainScaleFactor, roadWidth*  (float)terrainScaleFactor);
            nodeObjT.localPosition = closestPoint;
            nodeObjT.rotation = Quaternion.LookRotation(closestPoint2 - closestPoint, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
             0f,
             nodeObjT.eulerAngles.y,
             0f
            );
            nodeObj.name = "Ležeče brdo " + node.id;

        }
        else if (type == "island")
        {
            List<Vector2> roadPointsTemp = new List<Vector2>(trafficRoadPoints);
            Vector3 closestPoint = Methods.ClosestRoad(node.position, roadPointsTemp, this.terrain);
            roadPointsTemp.Remove(new Vector2(closestPoint.x, closestPoint.z));
            Vector3 closestPoint2 = new Vector3();
            while (true)
            {
                closestPoint2 = Methods.ClosestRoad(closestPoint, roadPointsTemp, this.terrain);
                roadPointsTemp.Remove(new Vector2(closestPoint2.x, closestPoint2.z));
                if(!Methods.VectorsSimilar(closestPoint, closestPoint2, this.terrainScaleFactor))
                {
                    break;
                }
            }
            
            Vector3 closestRoad = Methods.ClosestRoad(node.position, new List<Vector2>(this.roadNodeWidthDict.Keys), this.terrain);
            float roadWidth = this.roadNodeWidthDict[new Vector2(closestRoad.x, closestRoad.z)] * 0.75f;


            GameObject nodeObj = Instantiate(IslandObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(roadWidth/2 * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor , roadWidth / 4 * (float)terrainScaleFactor);
            closestPoint.y = closestPoint.y + 0.1f * (float)terrainScaleFactor;
            nodeObjT.localPosition = closestPoint;
            nodeObjT.rotation = Quaternion.LookRotation(closestPoint2 - closestPoint, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
             0f,
             nodeObjT.eulerAngles.y + 90f,
             0f
            );
            nodeObj.name = "Otok " + node.id;

        }

    }



    //Metoda za stvaranje objekta koji predstavlja čvor
    void InstantiateNode(Node node, string name)
    {
        GameObject nodeObj = Instantiate(NodeObject);

        Transform nodeObjT = nodeObj.transform;
        nodeObjT.localPosition = node.position;
        nodeObjT.localScale = new Vector3(1f, 1f, 1f);

        nodeObj.name = "LP " + " " + name + " " + node.ToString();

    }

    //Metoda za stvaranje objekta koji predstavlja čvor
    void InstantiateNode2(Vector3 pos, string name)
    {
        GameObject nodeObj = Instantiate(NodeObject);

        Transform nodeObjT = nodeObj.transform;
        nodeObjT.localPosition = pos;
        nodeObjT.localScale = new Vector3(1f, 1f, 1f);

        nodeObj.name = "LP " + " " + name ;

    }

    //Metoda za pronalazak objekta čvora na temelju njegovog id-a
    Node findNode(long nodeId)
    {
        foreach (Node n in this.nodes)
        {
            if (n.id == nodeId)
            {
                return n;
            }
        }
        return null;
    }

    //Provjeri nalazi li se na traženom mjestu već neki prometni objekt
    bool checkIfSpaceEmpty(Vector3 pos, List<Vector3> positions)
    {
        foreach(Vector3 p in positions)
        {
            double allowedDifference = 0.3 * terrainScaleFactor;
            double dist = Methods.Distance2D(pos, p);

            if (dist <= allowedDifference)
            {
                return false;
            }
                       
        }
        return true;
    }


    //Dohvat vektora pomaka text objekta na znaku
    public Vector3 generateTextTransVector(string direction, float orientation)
    {
        if(direction == "z")
        {
            if ((orientation > 90f && orientation < 180f) || (orientation > 180f && orientation < 270f))
            {
                return (+1f * new Vector3(0f, 0f, 1f) * 0.05f * (float)terrainScaleFactor);

            }
           
            else
            {
                return (-1f * new Vector3(0f, 0f, 1f) * 0.05f * (float)terrainScaleFactor);

            }
        }
        else
        {
            if ((orientation > 0f && orientation < 90f))
            {
                return (-1f * new Vector3(1f, 0f, 0f) * 0.05f * (float)terrainScaleFactor);

            }
            else if ((orientation > 90f && orientation < 180f))
            {
                return (-1f * new Vector3(1f, 0f, 0f) * 0.05f * (float)terrainScaleFactor);

            }
            else if ((orientation > 180f && orientation < 270f))
            {
                return (+1f * new Vector3(1f, 0f, 0f) * 0.05f * (float)terrainScaleFactor);

            }
            else
            {
                return (+1f * new Vector3(1f, 0f, 0f) * 0.05f * (float)terrainScaleFactor);

            }
        }
        
    }

}
