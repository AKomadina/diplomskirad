﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Linq;


public class MultiPolygonGenerator : MonoBehaviour
{

    public Terrain terrain;
    private double terrainScaleFactor;
    public GameObject NodeObject;
    public GameObject statueObject;
    public GameObject CarWashObject;
    public GameObject GasStationObject;
    public GameObject BicycleParkingObject;
    public GameObject ShelterObject;
    public GameObject BasketballCourtObject;
    public GameObject BoulesCourtObject;
    public GameObject DomeObject;
    public GameObject PlatformObject;
    public GameObject SportSignObject;
    public GameObject SlideDoorObject;
    public GameObject ChurchDoorObject;
    public GameObject WindowObject;
    public GameObject EmbankmentObject;
    public GameObject BridgeRoadObject;
    public GameObject BridgeWalkObject;
    public GameObject taxiSignObject;
    public GameObject greenhouseObject;
    public GameObject roofObject;


    public GameObject meshObject;
    private List<Node> nodes = new List<Node>();


    public GameObject tulipanObject;
    public GameObject narcisaObject;
    public GameObject šafranObject;
    public GameObject visibabaObject;
    public GameObject anemoneObject;
    public GameObject sadnicaObject;




    //Opcije za širinu prometnog traka ceste na temelju OSM podataka o njezinoj vrsti
    Dictionary<string, float> roadWidthOptions = new Dictionary<string, float>(){
        {"motorway", 11f},
        {"trunk", 7f},
        {"primary", 3.5f},
        {"secondary", 3f},
        {"tertiary", 2.5f},
        {"residential", 2f},
        {"unclassified", 1.5f},
        {"unknown", 1f},
        {"service", 1f},
        {"footway", 0.5f},
        {"path", 0.4f},
        {"road", 1f},
        {"living_street", 1.5f},
        {"track", 1.5f},
        {"pedestrian", 1.5f},
        {"primary_link", 1.5f},
        {"secondary_link", 1.5f},
        {"tertiary_link", 1f},
        {"steps", 0.5f},
        {"cycleway", 0.5f},
        {"", 1f}
    };

    //Koje vrste cesta imaju središnju bijelu liniju
    HashSet<string> roadsWithWhiteLine = new HashSet<string>() { "motorway", "trunk", "primary", "secondary", "tertiary", "residential" };


    //Raspored trokuta za poligon s 4 točke
    int[] quadTrianglesClockwise = new int[]
        {
            0,1,2,
            2,3,0
        };
    int[] quadTrianglesCounterClockwise = new int[]
        {
            0,3,2,
            2,1,0
        };

    public void Generate(GameObject terrain, List<Node> nodes, List<MultiPolygon> amenitys, List<MultiPolygon> historics, List<MultiPolygon> landuses, List<MultiPolygon> leisures, List<MultiPolygon> naturals, List<MultiPolygon> sports, List<Way> ways, List<MultiPolygon> manMade, List<Vector2> allRoadPoints, List<Node> wayNodes, List<Way> roads, double terrainScaleFactor)
    {
        this.terrain = terrain.GetComponent<Terrain>();
        this.terrainScaleFactor = terrainScaleFactor;
        this.nodes = nodes;

        
        //Instanciranje covjekom izradenih objekata
        foreach (MultiPolygon poly in manMade)
        {
            
       

            if (poly.type == "bridge")
            {
                List<Vector3> polyPos = new List<Vector3>();
                List<Vector2> polyPos2D = new List<Vector2>();

                foreach (long nodeID in poly.refNodes)
                {
                    Node n = findNode(nodeID);

                    if (n != null)
                    {

                        polyPos.Add(n.position);
                        polyPos2D.Add(new Vector2(n.position.x, n.position.z));
                    }
                }
                Vector3 centroid = Methods.getCentroid(polyPos.ToArray());
                float heightY = this.terrain.SampleHeight(centroid);
                List<float> boundingBox = Methods.getBoundingBox(polyPos);
                float minX = boundingBox[0];
                float maxX = boundingBox[1];
                float minZ = boundingBox[2];
                float maxZ = boundingBox[3];
                float distX = Math.Abs(maxX - minX);
                float distZ = Math.Abs(maxZ - minZ);
                Vector3 smallHalf1 = new Vector3(0f, 0f, 0f);
                Vector3 smallHalf2 = new Vector3(0f, 0f, 0f);
                Vector3 bigHalf1 = new Vector3(0f, 0f, 0f);
                Vector3 bigHalf2 = new Vector3(0f, 0f, 0f);
                if (distX <= distZ)
                {
                    smallHalf1 = new Vector3((minX + maxX) / 2, heightY, minZ);
                    smallHalf2 = new Vector3((minX + maxX) / 2, heightY, maxZ);
                    bigHalf1 = new Vector3(minX, heightY, (minZ + maxZ) / 2);
                    bigHalf2 = new Vector3(maxX, heightY, (minZ + maxZ) / 2);
                }
                else
                {
                    smallHalf1 = new Vector3(minX, heightY, (minZ + maxZ) / 2);
                    smallHalf2 = new Vector3(maxX, heightY, (minZ + maxZ) / 2);
                    bigHalf1 = new Vector3((minX + maxX) / 2, heightY, minZ);
                    bigHalf2 = new Vector3((minX + maxX) / 2, heightY, maxZ);
                }

                Node closestRoadSmallNode1 = Methods.ClosestRoadNode(smallHalf1, wayNodes);
                Node closestRoadSmallNode2 = Methods.ClosestRoadNode(smallHalf2, wayNodes);
                if (closestRoadSmallNode1 == null || closestRoadSmallNode2 == null) continue;
                Vector3 closestRoadSmall1 = closestRoadSmallNode1.position;
                Vector3 closestRoadSmall2 = closestRoadSmallNode2.position;
                Way way1 = Methods.findWayWithNode(closestRoadSmallNode1, roads);
                Way way2 = Methods.findWayWithNode(closestRoadSmallNode2, roads);

                if (way1 == null || way2 == null) continue;
                float width1 = 1f;
                float width2 = 1f;
                if (roadWidthOptions.ContainsKey(way1.roadType))
                {
                    width1 = roadWidthOptions[way1.roadType];
                }
                if (roadWidthOptions.ContainsKey(way2.roadType))
                {
                    width2 = roadWidthOptions[way2.roadType];
                }
                float meanWidth = (width1 + width2) / 2;


                Vector3 center = Methods.getCentroid(new List<Vector3>() { smallHalf1, smallHalf2 }.ToArray());
                float lengthCoef = (float)Methods.Distance3D(smallHalf1, smallHalf2) / 20f;
                float widthCoef = meanWidth * 1.4f;

                if (roadsWithWhiteLine.Contains(way1.roadType) || roadsWithWhiteLine.Contains(way2.roadType))
                {
                    GameObject nodeObj = Instantiate(BridgeRoadObject);
                    Transform nodeObjT = nodeObj.transform;
                    nodeObjT.localScale = new Vector3(lengthCoef * 0.9f, 1.25f * (float)terrainScaleFactor, widthCoef * (float)terrainScaleFactor);
                    nodeObjT.rotation = Quaternion.LookRotation(closestRoadSmall1 - closestRoadSmall2, Vector3.forward);
                    nodeObjT.eulerAngles = new Vector3(
                         0f,
                         nodeObjT.eulerAngles.y + 90f,
                         0f
                     );
                    nodeObjT.localPosition = new Vector3(center.x, heightY, center.z);
                    nodeObj.name = "Man made " + poly.id + " " + poly.type;
                }
                else
                {
                    GameObject nodeObj = Instantiate(BridgeWalkObject);
                    Transform nodeObjT = nodeObj.transform;
                    nodeObjT.localScale = new Vector3(lengthCoef * 0.9f, 0.8f * (float)terrainScaleFactor, widthCoef * (float)terrainScaleFactor);
                    nodeObjT.rotation = Quaternion.LookRotation(closestRoadSmall1 - closestRoadSmall2, Vector3.forward);
                    nodeObjT.eulerAngles = new Vector3(
                         0f,
                         nodeObjT.eulerAngles.y + 90f,
                         0f
                     );
                    nodeObjT.localPosition = new Vector3(center.x, heightY, center.z);
                    nodeObj.name = "Man made " + poly.id + " " + poly.type;
                }


            }
            else if (poly.type == "embankment")
            {
                List<Vector3> polyPos = new List<Vector3>();
                List<Vector2> polyPos2D = new List<Vector2>();

                foreach (long nodeID in poly.refNodes)
                {
                    Node n = findNode(nodeID);

                    if (n != null)
                    {
                        polyPos.Add(n.position);
                        polyPos2D.Add(new Vector2(n.position.x, n.position.z));
                    }
                }

                List<int> boundingBox = Methods.getMinBoundingBox(polyPos2D, (float)terrainScaleFactor);
                int minX = boundingBox[0];
                int maxX = boundingBox[1];
                int minZ = boundingBox[2];
                int maxZ = boundingBox[3];
                int distX = Math.Abs(maxX - minX);
                int distZ = Math.Abs(maxZ - minZ);
                float widthCoef = 0f;
                if (distX <= distZ)
                {
                    widthCoef = distX / 12f;
                }
                else
                {
                    widthCoef = distZ / 12f;
                }

                Vector3 centroid = Methods.getCentroid(polyPos.ToArray());
                List<Vector3> maxPair = Methods.getFarthestVectors(polyPos);
                double dist = Methods.Distance3D(maxPair[1], maxPair[0]);
                float lengthCoef = (float)dist / 8f;
                float heightCoef = widthCoef;

                GameObject nodeObj = Instantiate(EmbankmentObject);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3(lengthCoef * 0.7f, heightCoef * (float)terrainScaleFactor, widthCoef * (float)terrainScaleFactor);


                nodeObjT.rotation = Methods.findDirectionAngle(polyPos.ToArray());
                nodeObjT.eulerAngles = new Vector3(
                     nodeObjT.eulerAngles.x,
                     nodeObjT.eulerAngles.y + 90f,
                    nodeObjT.eulerAngles.z
                  );

                float heightY = this.terrain.SampleHeight(centroid);
                nodeObjT.localPosition = new Vector3(centroid.x, heightY, centroid.z);
                nodeObj.name = "Man made " + poly.id + " " + poly.type;
            }



        }

        //Instanciranje sportskog sadržaja
        foreach (MultiPolygon sport in sports)
        {

            if (sport.type == "basketball")
            {
                List<Vector3> sportPos = new List<Vector3>();

                foreach (long nodeID in sport.refNodes)
                {
                    Node n = findNode(nodeID);
                    if (n != null)
                    {
                        
                        sportPos.Add(n.position);
                    }
                }

                Vector3 centroid = Methods.getCentroid(sportPos.ToArray());
                GameObject nodeObj = Instantiate(BasketballCourtObject);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                float heightY = this.terrain.SampleHeight(centroid);
                nodeObjT.localPosition = new Vector3(centroid.x, heightY + (float)(0.5f * terrainScaleFactor), centroid.z);
                nodeObjT.rotation = Methods.findDirectionAngle(sportPos.ToArray());
                nodeObjT.eulerAngles = new Vector3(
                     nodeObjT.eulerAngles.x,
                     nodeObjT.eulerAngles.y + 90f,
                    nodeObjT.eulerAngles.z
                  );

                nodeObj.name = "Sport " + sport.id + " " + sport.type;

            }
            else if (sport.type == "boules")
            {
                List<Vector3> sportPos = new List<Vector3>();
                foreach (long nodeID in sport.refNodes)
                {
                    Node n = findNode(nodeID);
                    if (n != null)
                    {
                        sportPos.Add(n.position);
                    }
                }

                Vector3 centroid = Methods.getCentroid(sportPos.ToArray());
                GameObject nodeObj = Instantiate(BoulesCourtObject);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                float heightY = this.terrain.SampleHeight(centroid);
                nodeObjT.localPosition = new Vector3(centroid.x, heightY + (float)(0.5f * terrainScaleFactor), centroid.z);


                nodeObjT.rotation = Methods.findDirectionAngle(sportPos.ToArray());

                nodeObjT.eulerAngles = new Vector3(
                      nodeObjT.eulerAngles.x,
                     nodeObjT.eulerAngles.y + 90f,
                    nodeObjT.eulerAngles.z
                  );
                nodeObj.name = "Sport " + sport.id + " " + sport.type;


            }

        }

        //Instanciranje jezera kao poligon mesha za zadanom teksturom
        foreach (MultiPolygon natural in naturals)
        {
            if (natural.type == "pond")
            {
                List<Vector3> naturalPos = new List<Vector3>();

                foreach (long nodeID in natural.refNodes)
                {
                    Node n = findNode(nodeID);
                    if (n != null)
                    {
                        naturalPos.Add(n.position);
                    }
                }
                double area = Math.Abs(Methods.signedAreaOfPolygon(naturalPos));
                double uvDivider = Math.Sqrt(area) / 2;
                Material mat = Resources.Load("FountainWaterMat") as Material;

                createMultiPolygon(naturalPos, mat, natural, "Natural", (float)uvDivider);
            }
        }


        //Instanciranje sportskog centra
        foreach (MultiPolygon leisure in leisures)
        {
            if (leisure.type == "sports_centre")
            {
                float height = 6f * (float)terrainScaleFactor;
                List<Vector3> leisurePos = new List<Vector3>();
                foreach (long nodeID in leisure.refNodes)
                {
                    Node n = findNode(nodeID);
                    if (n != null)
                    {
                       

                        leisurePos.Add(n.position);
                    }

                }
                Vector3[] vertices = leisurePos.ToArray();
                bool clockWise = false;
                if (Methods.isPolygonClockwise(new List<Vector3>(vertices))) clockWise = true;

                double largest1 = double.MinValue;
                double largest2 = double.MinValue;
                int largestInd1 = 0;
                int largestInd2 = 0;
                double closest1 = double.MaxValue;
                double closest2 = double.MaxValue;
                int closestInd1 = 0;
                int closestInd2 = 0;
                List<Vector3[]> normals = new List<Vector3[]>();
                List<Vector3[]> wallVertices = new List<Vector3[]>();
                for (int j = 0; j < vertices.Length; ++j)
                {

                    Vector3[] sideVertices = new Vector3[4];
                    sideVertices[0] = vertices[j];
                    sideVertices[0].y = this.terrain.SampleHeight(vertices[j]);
                    sideVertices[1] = vertices[(j + 1) % vertices.Length];
                    sideVertices[1].y = this.terrain.SampleHeight(vertices[(j + 1) % vertices.Length]);
                    sideVertices[2] = vertices[(j + 1) % vertices.Length];
                    sideVertices[2].y = this.terrain.SampleHeight(vertices[(j + 1) % vertices.Length]) + (float)height;
                    sideVertices[3] = vertices[j];
                    sideVertices[3].y = this.terrain.SampleHeight(vertices[j]) + (float)height;


                    double length = Methods.Distance3D(sideVertices[0], sideVertices[1]);
                    if (length > largest1)
                    {
                        largest2 = largest1;
                        largestInd2 = largestInd1;
                        largest1 = length;
                        largestInd1 = j;

                    }
                    else if (length > largest2)
                    {
                        largest2 = length;
                        largestInd2 = j;
                    }
                    double distance = Methods.DistanceToWay(sideVertices, wayNodes);
                    if (distance < closest1)
                    {
                        closest2 = closest1;
                        closestInd2 = closestInd1;
                        closest1 = distance;
                        closestInd1 = j;

                    }
                    else if (distance < closest2)
                    {
                        closest2 = distance;
                        closestInd2 = j;
                    }
                    normals.Add(CreateWall(sideVertices, clockWise, leisure.id));
                    wallVertices.Add(sideVertices);
                }
                int doorInd = Methods.getDoorIndex(largestInd1, largestInd2, closestInd1, closestInd2);

                for (int j = 0; j < vertices.Length; ++j)
                {
                    if (doorInd == j)
                    {
                        addDoor(wallVertices[doorInd], normals[doorInd],  leisure.id, leisure.type);
                        addWindows(wallVertices[j], normals[j],  leisure.id, true, leisure.type);
                    }
                    else
                    {
                        addWindows(wallVertices[j], normals[j],  leisure.id, false, leisure.type);
                    }
                }


                Material mat = Resources.Load("PVCRoofMat") as Material;
                double area = Math.Abs(Methods.signedAreaOfPolygon(leisurePos));
                double uvDivider = 5000f / Math.Sqrt(area);
                CreateFlatRoof(leisurePos, height, "Roof sport centre", mat, (float)uvDivider);


                Vector3 centroid = Methods.getCentroid(leisurePos.ToArray());
                GameObject nodeObj = Instantiate(SportSignObject);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3((float)Math.Sqrt(area) / 10f * (float)terrainScaleFactor, (float)Math.Sqrt(area) / 13f * (float)terrainScaleFactor, (float)Math.Sqrt(area) / 10f * (float)terrainScaleFactor);
                float heightY = this.terrain.SampleHeight(centroid);
                nodeObjT.localPosition = new Vector3(centroid.x, heightY + height, centroid.z);

                nodeObjT.rotation = Methods.findDirectionAngle(leisurePos.ToArray());
                nodeObjT.eulerAngles = new Vector3(
                     nodeObjT.eulerAngles.x,
                     nodeObjT.eulerAngles.y ,
                    nodeObjT.eulerAngles.z
                  );
               
            }
        }

        foreach (MultiPolygon amenity in amenitys)
        {
            //Instanciranje auto praonice
            if (amenity.type == "car_wash")
            {
                List<Vector3> amenityPos = new List<Vector3>();
                foreach (long nodeID in amenity.refNodes)
                {
                    Node n = findNode(nodeID);
                    if (n != null)
                    {
                     
                        amenityPos.Add(n.position);
                    }

                }
                List<float> boundingBox = Methods.getBoundingBox(amenityPos);
                float minX = boundingBox[0];
                float maxX = boundingBox[1];
                float minZ = boundingBox[2];
                float maxZ = boundingBox[3];
                float distX = maxX - minX;
                float distZ = maxZ - minZ;
                float scale = 0f;
                if (distX < distZ)
                {
                    scale = distX / 6f;
                }
                else
                {
                    scale = distZ / 6f;
                }
                if (scale >= 0.5)
                {
                    Vector3 centroid = Methods.getCentroid(amenityPos.ToArray());
                    GameObject nodeObj = Instantiate(CarWashObject);
                    Transform nodeObjT = nodeObj.transform;
                    nodeObjT.localScale = new Vector3(0.5f * scale * (float)terrainScaleFactor, 0.5f * scale * (float)terrainScaleFactor, 0.5f * scale * (float)terrainScaleFactor);
                    float heightY = this.terrain.SampleHeight(centroid);
                    nodeObjT.localPosition = new Vector3(centroid.x, heightY, centroid.z);
                    nodeObjT.rotation = Methods.findDirectionAngle(amenityPos.ToArray());
                    nodeObjT.eulerAngles = new Vector3(
                         nodeObjT.eulerAngles.x,
                         nodeObjT.eulerAngles.y + 90f,
                        nodeObjT.eulerAngles.z
                      );
                    nodeObj.name = "Amenity " + amenity.id + " " + amenity.type;
                }

            }
            //Instanciranje pumpe za gorivo
            else if (amenity.type == "fuel")
            {
                List<Vector3> amenityPos = new List<Vector3>();
                foreach (long nodeID in amenity.refNodes)
                {
                    Node n = findNode(nodeID);
                    if (n != null)
                    {
                     
                        amenityPos.Add(n.position);
                    }

                }

                Vector3 centroid = Methods.getCentroid(amenityPos.ToArray());
                GameObject nodeObj = Instantiate(GasStationObject);
                Transform nodeObjT = nodeObj.transform;

                nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                float heightY = this.terrain.SampleHeight(centroid);
                nodeObjT.localPosition = new Vector3(centroid.x, heightY, centroid.z);
                nodeObjT.rotation = Methods.findDirectionAngle(amenityPos.ToArray());
                nodeObjT.eulerAngles = new Vector3(
                     nodeObjT.eulerAngles.x,
                     nodeObjT.eulerAngles.y + 90f,
                    nodeObjT.eulerAngles.z
                  );

                nodeObj.name = "Amenity " + amenity.id + " " + amenity.type;



            }
            //Instanciranje staklenika
            else if (amenity.type == "greenhouse")
            {
                List<Vector3> amenityPos = new List<Vector3>();
                foreach (long nodeID in amenity.refNodes)
                {
                    Node n = findNode(nodeID);
                    if (n != null)
                    {
                       
                        amenityPos.Add(n.position);

                    }

                }
                List<float> boundingBox = Methods.getBoundingBox(amenityPos);
                float minX = boundingBox[0];
                float maxX = boundingBox[1];
                float minZ = boundingBox[2];
                float maxZ = boundingBox[3];
                float distX = maxX - minX;
                float distZ = maxZ - minZ;

                Vector3 centroid = Methods.getCentroid(amenityPos.ToArray());
                GameObject nodeObj = Instantiate(greenhouseObject);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3((distX / 4f) * 0.85f, 1 * (float)terrainScaleFactor, (distZ / 4f) * 0.85f);
     
                float heightY = this.terrain.SampleHeight(centroid);
                nodeObjT.localPosition = new Vector3(centroid.x, heightY + 0.2f * (float)terrainScaleFactor, centroid.z);
                nodeObjT.rotation = Methods.findDirectionAngle(amenityPos.ToArray());
                nodeObjT.eulerAngles = new Vector3(
                     nodeObjT.eulerAngles.x,
                     nodeObjT.eulerAngles.y + 90f,
                    nodeObjT.eulerAngles.z
                  );
                nodeObj.name = "Amenity " + amenity.id + " " + amenity.type;
            }

            //Instanciranje krova
            else if (amenity.type == "roof")
            {
                List<Vector3> amenityPos = new List<Vector3>();
                foreach (long nodeID in amenity.refNodes)
                {
                    Node n = findNode(nodeID);
                    if (n != null)
                    {
                       
                        amenityPos.Add(n.position);
                    }

                }
                List<float> boundingBox = Methods.getBoundingBox(amenityPos);
                float minX = boundingBox[0];
                float maxX = boundingBox[1];
                float minZ = boundingBox[2];
                float maxZ = boundingBox[3];
                float distX = maxX - minX;
                float distZ = maxZ - minZ;
                double area = Math.Abs(Methods.signedAreaOfPolygon(amenityPos));
                if (area == 0)
                {
                    continue;
                }

                Vector3 centroid = Methods.getCentroid(amenityPos.ToArray());
                GameObject nodeObj = Instantiate(roofObject);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3(distX * 0.8f, 5f * (float)terrainScaleFactor, distZ * 0.8f);
               
                float heightY = this.terrain.SampleHeight(centroid);
                nodeObjT.localPosition = new Vector3(centroid.x, heightY + 0.1f * (float)terrainScaleFactor, centroid.z   );
                nodeObjT.rotation = Methods.findDirectionAngle(amenityPos.ToArray());
                nodeObjT.eulerAngles = new Vector3(
                     nodeObjT.eulerAngles.x,
                     nodeObjT.eulerAngles.y + 90f,
                    nodeObjT.eulerAngles.z
                  );
                nodeObj.name = "Amenity " + amenity.id + " " + amenity.type;

            }


            //Instanciranje sjenice
            else if (amenity.type == "shelter")
            {
                List<Vector3> amenityPos = new List<Vector3>();
                foreach (long nodeID in amenity.refNodes)
                {
                    Node n = findNode(nodeID);
                    if (n != null)
                    {
                        amenityPos.Add(n.position);

                    }

                }
                Vector3 centroid = Methods.getCentroid(amenityPos.ToArray());
                GameObject nodeObj = Instantiate(ShelterObject);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localScale = new Vector3(100f * (float)terrainScaleFactor, 100f * (float)terrainScaleFactor, 100f * (float)terrainScaleFactor);
                float heightY = this.terrain.SampleHeight(centroid);
                nodeObjT.localPosition = new Vector3(centroid.x, heightY, centroid.z);
                nodeObj.name = "Amenity " + amenity.id + " " + amenity.type;

            }
            //Instanciranje parkinga za bicikle
            else if (amenity.type == "bicycle_parking")
            {

                List<Vector3> amenityPos = new List<Vector3>();
                foreach (long nodeID in amenity.refNodes)
                {
                    Node n = findNode(nodeID);
                    if (n != null)
                    {
                        amenityPos.Add(n.position);
 
                    }

                }
                List<float> boundingBox = Methods.getBoundingBox(amenityPos);
                float minX = boundingBox[0];
                float maxX = boundingBox[1];
                float minZ = boundingBox[2];
                float maxZ = boundingBox[3];
                float distX = maxX - minX;
                float distZ = maxZ - minZ;


                float stepX = 0f;
                float stepZ = 0f;
                float bound = (float)(1f * terrainScaleFactor);
                if (distX > distZ)
                {
                    stepX = 1.5f * (float)terrainScaleFactor;
                    stepZ = 3f * (float)terrainScaleFactor;
                }
                else
                {
                    stepX = 3f * (float)terrainScaleFactor;
                    stepZ = 1.5f * (float)terrainScaleFactor;
                }

                for (float i = minX + bound; i < maxX - bound; i = i + stepX)
                {
                    for (float j = minZ + bound; j < maxZ - bound; j = j + stepZ)
                    {

                        Vector3 currPos = new Vector3(i, 0f, j);
                        currPos.y = this.terrain.SampleHeight(currPos);
                        if (Methods.PointInPolygon(amenityPos, currPos))
                        {
                           
                            
                            GameObject nodeObj = Instantiate(BicycleParkingObject);
                            Transform nodeObjT = nodeObj.transform;
                            if (distX > distZ)
                            {
                                nodeObjT.localPosition = new Vector3(currPos.x + (float)(2.51f * terrainScaleFactor), currPos.y, currPos.z - (float)(0.621f * terrainScaleFactor));
                            }
                            else
                            {
                                nodeObjT.localPosition = new Vector3(currPos.x - (float)(0.63f * terrainScaleFactor), currPos.y, currPos.z - (float)(2.5f * terrainScaleFactor));
                            }
                            nodeObjT.localScale = new Vector3(0.1f * (float)terrainScaleFactor, 0.1f * (float)terrainScaleFactor, 0.1f * (float)terrainScaleFactor);

                            if (distX > distZ)
                            {
                                nodeObjT.eulerAngles = new Vector3(
                                                     nodeObjT.eulerAngles.x,
                                                     nodeObjT.eulerAngles.y,
                                                     nodeObjT.eulerAngles.z
                                 );
                            }
                            else
                            {
                                nodeObjT.eulerAngles = new Vector3(
                                 nodeObjT.eulerAngles.x,
                                 nodeObjT.eulerAngles.y + 90,
                                 nodeObjT.eulerAngles.z
                               );
                            }
                            nodeObj.name = "Parking za bicikle " + amenity.id;
                        }


                    }
                }
            }
        }


        //Instanciranje spomenika na centroidu historic poligona
        foreach (MultiPolygon historic in historics)
        {
            List<Vector3> historicPos = new List<Vector3>();
            foreach (long nodeID in historic.refNodes)
            {
                Node n = findNode(nodeID);
                if (n != null)
                {
                    historicPos.Add(n.position);
                }

            }
            if (historicPos.Count < 2)
            {
                continue;
            }
            Vector3 centroid = Methods.getCentroid(historicPos.ToArray());
            GameObject nodeObj = Instantiate(statueObject);
            Transform nodeObjT = nodeObj.transform;
            float heightY = this.terrain.SampleHeight(centroid);
            nodeObjT.localPosition = new Vector3(centroid.x, heightY, centroid.z);
            nodeObjT.localScale = new Vector3(0.007f * (float)terrainScaleFactor, 0.007f * (float)terrainScaleFactor, 0.007f * (float)terrainScaleFactor);
            Vector3 closestRoad = Methods.ClosestRoad(nodeObjT.localPosition, allRoadPoints, this.terrain);
            nodeObjT.rotation = Quaternion.LookRotation(closestRoad - nodeObjT.localPosition, Vector3.forward);
            nodeObjT.eulerAngles = new Vector3(
                nodeObjT.eulerAngles.x - 90,
                nodeObjT.eulerAngles.y - 90,
                nodeObjT.eulerAngles.z
            );

            nodeObj.name = "Statua " + historic.id + " " + historic.type;
        }

        //Instanciranje tipova površina kao poligon mesha za zadanom teksturom
        foreach (MultiPolygon landuse in landuses)
        {



            //Polje cvijeća
            if (landuse.type == "flowerbed")
            {
                List<Vector3> flowerPos = new List<Vector3>();

                foreach (long nodeID in landuse.refNodes)
                {
                    Node n = findNode(nodeID);
                    if (n != null)
                    {
                        flowerPos.Add(n.position);
                    }
                }

                //Dobivanje bounding boxa poligona unutar kojeg raste cvijeće
                List<float> boundingBox = Methods.getBoundingBox(flowerPos);
                float minX = boundingBox[0];
                float maxX = boundingBox[1];
                float minZ = boundingBox[2];
                float maxZ = boundingBox[3];

                //Stvaranje cvijeća unutar bounding boxa po redu
                float step = 0.3f * (float)terrainScaleFactor;
                for (float i = minX; i < maxX ; i = i + step)
                {
                    System.Random rnd = new System.Random();
                    int randomType = rnd.Next(0, Constants.flowerOptions.Count);
                    string flowerType = Constants.flowerOptions[randomType];
                    for (float j = minZ ; j < maxZ ; j = j + step)
                    {
                        Vector3 currPos = new Vector3(i, 0f, j);
                        currPos.y = this.terrain.SampleHeight(currPos);

                        if (Methods.PointInPolygon(flowerPos, currPos))
                        {
                           
                            InstantiateFlower(currPos, landuse.id, flowerType);
                        }
                    }
                }

            }


            //Polje sadnica
            else if (landuse.type == "plant_nursery")
            {
                List<Vector3> flowerPos = new List<Vector3>();

                foreach (long nodeID in landuse.refNodes)
                {
                    Node n = findNode(nodeID);
                    if (n != null)
                    {
                        flowerPos.Add(n.position);
                    }
                }

                //Dobivanje bounding boxa poligona unutar kojeg raste cvijeće
                List<float> boundingBox = Methods.getBoundingBox(flowerPos);
                float minX = boundingBox[0];
                float maxX = boundingBox[1];
                float minZ = boundingBox[2];
                float maxZ = boundingBox[3];

                //Stvaranje cvijeća unutar bounding boxa po redu
                float step = 1.25f * (float)terrainScaleFactor;
                for (float i = minX ; i < maxX ; i = i + step)
                {
                    for (float j = minZ ; j < maxZ; j = j + step)
                    {

                        Vector3 currPos = new Vector3(i, 0f, j);
                        currPos.y = this.terrain.SampleHeight(currPos);

                        if (Methods.PointInPolygon(flowerPos, currPos))
                        {
                            
                            GameObject nodeObj = Instantiate(sadnicaObject);
                            Transform nodeObjT = nodeObj.transform;
                            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
                            nodeObjT.localPosition = new Vector3(currPos.x, currPos.y - (float)(0.025f * terrainScaleFactor), currPos.z);
                            nodeObj.name = "Sadnica " + landuse.id;
                        }


                    }
                }

            }

        }

    }

    //Stvaranje poligona kao mesh objekta
    void createMultiPolygon(List<Vector3> positions, Material mat, MultiPolygon n, string name, float uvDivider)
    {


        GameObject meshObj = Instantiate(meshObject);
        Mesh mesh = new Mesh();
        meshObj.GetComponent<MeshFilter>().mesh = mesh;
        mesh.Clear();
        mesh.vertices = positions.ToArray();
        mesh.triangles = Methods.makeTriangles(positions);
        meshObj.GetComponent<MeshRenderer>().material = mat;


        //Podizanje mesh objekta
        Transform meshObjT = meshObj.transform;
        Vector3 position = meshObjT.localPosition;
        position.y = 0.1f * (float)terrainScaleFactor;
        meshObjT.position = position;

        mesh.uv = Methods.generateUVs(positions.ToArray(), uvDivider);

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        meshObj.name = name + " " + n.id + " " + n.type;

    }

    //Stvaranje ravnog krova
    void CreateFlatRoof(List<Vector3> vertices,  float height, string name, Material mat, float uvDivider)
    {
        List<Vector3> newVert = new List<Vector3>();
        foreach (Vector3 v in vertices)
        {
            Vector3 newV = new Vector3(v.x, this.terrain.SampleHeight(v)+height, v.z);
            newVert.Add(newV);
        }

        GameObject meshObj = Instantiate(meshObject);
        Mesh mesh = new Mesh();
        meshObj.GetComponent<MeshFilter>().mesh = mesh;
        mesh.Clear();
        mesh.vertices = newVert.ToArray();
        mesh.triangles = Methods.makeTriangles(vertices);
        Transform nodeObjT = meshObj.transform;
        //nodeObjT.localPosition = new Vector3(nodeObjT.localPosition.x, height, nodeObjT.localPosition.z);
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.uv = Methods.generateUVs(vertices.ToArray(), uvDivider);
        meshObj.GetComponent<MeshRenderer>().material = mat;
        meshObj.name = name;

    }






    //Metoda za pronalazak objekta čvora na temelju njegovog id-a
    Node findNode(long nodeId)
    {
        foreach (Node n in nodes)
        {
            if (n.id == nodeId)
            {
                return n;
            }
        }
        return null;
    }

    //Metoda za stvaranje objekta koji predstavlja čvor
    void InstantiateNode(Node node, string name)
    {
        GameObject nodeObj = Instantiate(NodeObject);

        Transform nodeObjT = nodeObj.transform;
        nodeObjT.localPosition = node.position;

        nodeObj.name = "MP " + " " + name + " " + node.ToString();

    }


    //Metoda za instanciranje cvijeta
    public void InstantiateFlower(Vector3 position, long id, string type)
    {
        if (type == "tulipan")
        {
            GameObject nodeObj = Instantiate(tulipanObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(2.7f * (float)terrainScaleFactor, 2.7f * (float)terrainScaleFactor, 2.7f * (float)terrainScaleFactor);
            nodeObjT.localPosition = new Vector3(position.x, position.y - (float)(0.04f * terrainScaleFactor), position.z);
            nodeObj.name = "Tulipan " + id;
        }
        else if (type == "narcisa")
        {
            GameObject nodeObj = Instantiate(narcisaObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(5f * (float)terrainScaleFactor, 5f * (float)terrainScaleFactor, 5f * (float)terrainScaleFactor);
            nodeObjT.localPosition = position;
            nodeObj.name = "Narcisa " + id;
        }
        else if (type == "šafran")
        {
            GameObject nodeObj = Instantiate(šafranObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(1.2f * (float)terrainScaleFactor, 1.2f * (float)terrainScaleFactor, 1.2f * (float)terrainScaleFactor);
            nodeObjT.localPosition = position;
            nodeObj.name = "Šafran " + id;
        }
        else if (type == "visibaba")
        {
            GameObject nodeObj = Instantiate(visibabaObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(5.5f * (float)terrainScaleFactor, 5.5f * (float)terrainScaleFactor, 5.5f * (float)terrainScaleFactor);
            nodeObjT.localPosition = position;
            nodeObj.name = "Visibaba " + id;
        }
        else if (type == "anemone")
        {
            GameObject nodeObj = Instantiate(anemoneObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(1.5f * (float)terrainScaleFactor, 1.5f * (float)terrainScaleFactor, 1.5f * (float)terrainScaleFactor);
            nodeObjT.localPosition = new Vector3(position.x, position.y - (float)(0.02f * terrainScaleFactor), position.z);
            nodeObj.name = "Anemone " + id;
        }

    }





    //Stvaranje mesh objekta koji predstavlja jedan zid zgrade
    Vector3[] CreateWall(Vector3[] vertices,  bool clockWise, double id)
    {
        int[] triangles;


        if (clockWise)
        {
            triangles = quadTrianglesClockwise;
        }
        else
        {
            triangles = quadTrianglesCounterClockwise;
        }


        //Stvaranje mesh objekta
        GameObject meshObj = Instantiate(meshObject);
        Mesh mesh = new Mesh();
        meshObj.GetComponent<MeshFilter>().mesh = mesh;
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();


        mesh.uv = Methods.generateQuadUVs(vertices, 6f);
        Material mat = Resources.Load("facadeMat") as Material;
        meshObj.GetComponent<MeshRenderer>().material = mat;

        meshObj.name = "Amenity wall: " + id.ToString();

        return mesh.normals;

    }



    //Stvaranje vrata na zgradi
    void addDoor(Vector3[] vertices, Vector3[] normals, double id, string type)
    {

        if (normals.Length > 0)
        {
            Vector3 centroid = Methods.getCentroid(vertices);
            GameObject obj = Instantiate(SlideDoorObject);
            Transform objT = obj.transform;

            Vector3 pos = new Vector3(centroid.x, 0f, centroid.z);
            pos.y = this.terrain.SampleHeight(pos);
            objT.localPosition = pos;
            objT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            objT.rotation = Quaternion.LookRotation(normals[0]);
            objT.position -= Vector3.Normalize(normals[0]) * 0.85f;

            obj.name = "Door " + type + id;
         

        }
    }
    //Stvaranje prozora na zgradi
    void addWindows(Vector3[] vertices, Vector3[] normals,  double id, bool main, string type)
    {
        //Duljina zida
        double lengthX = vertices[1].x - vertices[0].x;
        double lengthZ = vertices[1].z - vertices[0].z;
        double length = Methods.Distance2D(vertices[1], vertices[0]);


        if (normals.Length > 0)
        {
            Vector3 centroid = Methods.getCentroid(vertices);
            int columnsNum = 0;
            columnsNum = (int)((length - 2 * terrainScaleFactor) / (0.8f * terrainScaleFactor + 3f * terrainScaleFactor));

            if (columnsNum <= 1 && length >= 2.5 && !main)
            {
                GameObject obj = Instantiate(WindowObject);
                Transform objT = obj.transform;
                objT.localScale = new Vector3(1.15f * (float)terrainScaleFactor, 0.85f * (float)terrainScaleFactor, 0.85f * (float)terrainScaleFactor);
                Vector3 pos = new Vector3(centroid.x, 0f, centroid.z);             
                pos.y = this.terrain.SampleHeight(pos) + (float)(1.5f * terrainScaleFactor) + (float)(0.75f * terrainScaleFactor);
              
                objT.localPosition = pos;
                objT.rotation = Quaternion.LookRotation(normals[0]);

                obj.name = "Window " + type + id;
            }
            else
            {
                //Razmaci po svakoj osi
                double incX = lengthX / columnsNum;
                double incY = 3f * terrainScaleFactor;
                double incZ = lengthZ / columnsNum;


                //Postavi prozore
                for (int i = 0; i < columnsNum - 1; ++i)
                {
                    if (main)
                    {
                        if (columnsNum % 2 == 0 && i == (columnsNum - 1) / 2)
                        {
                            continue;
                        }
                        else if (columnsNum % 2 != 0 && (i == (columnsNum - 1) / 2 || i == (columnsNum - 1) / 2 - 1))
                        {
                            continue;
                        }
                    }
                    float posX = vertices[0].x + (float)(i * incX) + (float)incX;
                    float posZ = vertices[0].z + (float)(i * incZ) + (float)incZ;
                    Vector3 pos = new Vector3(posX, 0f, posZ);
                    pos.y = this.terrain.SampleHeight(pos) + (float)(1.5f * terrainScaleFactor) + (float)(0.75f * terrainScaleFactor);


                    GameObject obj = Instantiate(WindowObject);
                    Transform objT = obj.transform;
                    objT.localScale = new Vector3(1.15f * (float)terrainScaleFactor, 0.85f * (float)terrainScaleFactor, 0.85f * (float)terrainScaleFactor);
           
                    objT.rotation = Quaternion.LookRotation(normals[0]);


                    objT.localPosition = pos;

                    obj.name = "Window " + type + id;
                }

            }



        }
    }

    //Metoda za stvaranje objekta koji predstavlja čvor
    void InstantiateNode2(Vector3 pos, string name)
    {
        GameObject nodeObj = Instantiate(NodeObject);

        Transform nodeObjT = nodeObj.transform;
        nodeObjT.localPosition = pos;
        nodeObjT.localScale = new Vector3(1f, 1f, 1f);

        nodeObj.name = "LP " + " " + name;

    }

    


}
