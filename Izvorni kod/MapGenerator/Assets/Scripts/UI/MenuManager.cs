﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MenuManager : MonoBehaviour
{
    private double maxLatitude;
    private double minLatitude;
    private double maxLongitude;
    private double minLongitude;
    private double terrainScaleFactor;
    private bool sateliteImage = false;
    private int preset = 0;
    public GameObject maxLatInputText;
    public GameObject minLatInputText;
    public GameObject maxLonInputText;
    public GameObject minLonInputText;
    public GameObject scaleFactorInputText;

    public GameObject maxLatInput;
    public GameObject minLatInput;
    public GameObject maxLonInput;
    public GameObject minLonInput;
    public GameObject Buttons;
    public GameObject OptionsText;

    public GameObject canvas;

    public void sateliteToggleFunc()
    {
        this.sateliteImage = !this.sateliteImage;
    }

    public void changePreset(int value)
    {
        this.preset = value;
    }

    public void setMaxLatitude()
    {
        try
        {
            this.maxLatitude = Convert.ToDouble(maxLatInputText.GetComponent<Text>().text);
        }
        catch (FormatException e)
        {
            this.maxLatitude = 0;
            maxLatInputText.GetComponent<Text>().text = "";
        }
        
    }

    public void setMinLatitude()
    {
        try
        {
            this.minLatitude = Convert.ToDouble(minLatInputText.GetComponent<Text>().text);
        }
        catch (FormatException e)
        {
            this.minLatitude = 0;
            minLatInputText.GetComponent<Text>().text = "";
        }

    }

    public void setMaxLongitude()
    {
        try
        {
            this.maxLongitude = Convert.ToDouble(maxLonInputText.GetComponent<Text>().text);
        }
        catch (FormatException e)
        {
            this.maxLongitude = 0;
            maxLonInputText.GetComponent<Text>().text = "";
        }

    }

    public void setMinLongitude()
    {
        try
        {
            this.minLongitude = Convert.ToDouble(minLonInputText.GetComponent<Text>().text);
        }
        catch (FormatException e)
        {
            this.minLongitude = 0;
            minLonInputText.GetComponent<Text>().text = "";
        }

    }

    public void setTerrainScaleFactor()
    {
        try
        {
            this.terrainScaleFactor = Convert.ToDouble(scaleFactorInputText.GetComponent<Text>().text);
        }
        catch (FormatException e)
        {
            this.terrainScaleFactor = 0;
            scaleFactorInputText.GetComponent<Text>().text = "";
        }

    }

    public void Generate()
    {
      
        GameObject scriptRunnerGameObj = GameObject.Find("ScriptRunner");
        ScriptRunner scriptRunner = (ScriptRunner)scriptRunnerGameObj.GetComponent(typeof(ScriptRunner));

       
        

        

 

        if(preset == 0)
        {
            scriptRunner.Run(this.getGeoInfo(), terrainScaleFactor, sateliteImage);
        }
        else if (preset == 1)
        {
            //Zagreb centar
            List<double> geoInfTest = new List<double>();
            geoInfTest.Add(45.81);
            geoInfTest.Add(45.8);
            geoInfTest.Add(15.98);
            geoInfTest.Add(15.97);
            scriptRunner.Run(geoInfTest, terrainScaleFactor, sateliteImage);
        }
        else if (preset == 2)
        {
            //Zagreb zapad
            List<double> geoInfTest = new List<double>();
            geoInfTest.Add(45.85);
            geoInfTest.Add(45.82);
            geoInfTest.Add(15.9);
            geoInfTest.Add(15.87);
            scriptRunner.Run(geoInfTest, terrainScaleFactor, sateliteImage);
        }
        else if (preset == 3)
        {
            //Zagreb istok
            List<double> geoInfTest = new List<double>();
            geoInfTest.Add(45.812);
            geoInfTest.Add(45.805);
            geoInfTest.Add(16.03);
            geoInfTest.Add(16.015);
            scriptRunner.Run(geoInfTest, terrainScaleFactor, sateliteImage);
        }
        else if (preset == 4)
        {
            //New York
            List<double> geoInfTest = new List<double>();
            geoInfTest.Add(40.63);
            geoInfTest.Add(40.62);
            geoInfTest.Add(-73.95);
            geoInfTest.Add(-73.96);
            scriptRunner.Run(geoInfTest, terrainScaleFactor, sateliteImage);
        }
        else if (preset == 5)
        {
            //New York
            List<double> geoInfTest = new List<double>();
            geoInfTest.Add(45.82);
            geoInfTest.Add(45.80);
            geoInfTest.Add(15.99);
            geoInfTest.Add(15.97);
            scriptRunner.Run(geoInfTest, terrainScaleFactor, sateliteImage);
        }

        Time.timeScale = 1;

    }

    public void Quit()
    {
        Application.Quit();
    }

    //Vraćanje informacija o geografskoj širini i dužini
    public List<double> getGeoInfo()
    {
        List<double> geoInf = new List<double>();
        geoInf.Add(maxLatitude);
        geoInf.Add(minLatitude);
        geoInf.Add(maxLongitude);
        geoInf.Add(minLongitude);
        return geoInf;

    }



    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
   
    }
}
