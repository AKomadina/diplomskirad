﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;
using UnityEditor;

public class SceneCamera : MonoBehaviour
{

    float mainSpeed = 50.0f;
    float shiftAdd = 100.0f;
    float maxShift = 1000.0f;
    float camSens = 0.25f;
    private Vector3 lastMouse = new Vector3(255, 255, 255);
    private float totalRun = 1.0f;

    private float nextActionTime = 0f;
    public float period = 0.0005f;

    public GameObject terrain;
    private int currX = 0;
    private int currZ = 0;
    public float sizeX = 0;
    public float sizeY = 0;
    public float sizeZ = 0;
    private double maxLatitude;
    private double minLatitude;
    private double maxLongitude;
    private double minLongitude;
    private double diffLat;
    private double diffLon;

    private bool startFlag = false;
    float avg = 0f;
    List<float> fpsValues = new List<float>();
    public GameObject coordinateText;

    public bool appStarted = false;

    public void Run(GameObject terrain, List<double> geoInfo)
    {

        this.terrain = terrain;
        Vector3 size = terrain.GetComponent<Terrain>().terrainData.size;
        this.sizeX = size.x;
        this.sizeY = size.y + 50f;
        this.sizeZ = size.z;
        this.startFlag = true;
        transform.eulerAngles = new Vector3(90f, 0f, 0f);
        this.appStarted = true;
        this.maxLatitude = geoInfo[0];
        this.minLatitude = geoInfo[1];
        this.maxLongitude = geoInfo[2];
        this.minLongitude = geoInfo[3];
        this.diffLat = maxLatitude - minLatitude;
        this.diffLon = maxLongitude - minLongitude;

    }

    void Update()
    {
        if (appStarted)
        {
            Vector3 cameraPosition = transform.position;
            float x = cameraPosition.x;
            float z = cameraPosition.z;
            double y = System.Math.Round(cameraPosition.y,2);
            string latitude = "/";
            string longitude = "/";
            if (x <= this.sizeX && x >= 0)
            {
                float dx = x / (this.sizeX / (float)this.diffLon);
                longitude = (dx + (float)this.minLongitude).ToString();
            }
            if (z <= this.sizeZ && z >= 0)
            {
                float dz = z / (this.sizeZ / (float)this.diffLat);
                latitude = (dz + (float)this.minLatitude).ToString();
            }
            var text = coordinateText.GetComponent<TMPro.TextMeshProUGUI>();
            text.text = "Latitude: " + latitude + " °\nLongitude: " + longitude + " °\nHeight: " + y + " m";
        }


        lastMouse = Input.mousePosition - lastMouse;
        lastMouse = new Vector3(-lastMouse.y * camSens, lastMouse.x * camSens, 0);
        lastMouse = new Vector3(transform.eulerAngles.x + lastMouse.x, transform.eulerAngles.y + lastMouse.y, 0);
        if (appStarted)
        {
            transform.eulerAngles = lastMouse;
        }

        lastMouse = Input.mousePosition;

        Vector3 p = GetBaseInput();
        if (Input.GetKey(KeyCode.LeftShift))
        {
            totalRun += Time.deltaTime;
            p = p * totalRun * shiftAdd;
            p.x = Mathf.Clamp(p.x, -maxShift, maxShift);
            p.y = Mathf.Clamp(p.y, -maxShift, maxShift);
            p.z = Mathf.Clamp(p.z, -maxShift, maxShift);
        }
        else
        {
            totalRun = Mathf.Clamp(totalRun * 0.5f, 1f, 1000f);
            p = p * mainSpeed;
        }

        p = p * Time.deltaTime;
        Vector3 newPosition = transform.position;
        if (appStarted)
        {
            transform.Translate(p);
        }



        /*

          if (startFlag)
          {
              float fps = 1f / Time.deltaTime;
              fpsValues.Add(fps);

              if (Time.time > nextActionTime)
              {
                  nextActionTime += period;
                  Vector3 newPosition = new Vector3(currX, sizeY, currZ);
                  transform.position = newPosition;

                  currX+=5;
                  if (currX >= sizeX)
                  {
                      currX = 0;
                      currZ+=50;
                  }
                  if(currZ >= sizeZ)
                  {
                      startFlag = false;

                      StreamWriter writer = new StreamWriter("MyPath.txt", true);
                      string text = "";
                      foreach(float value in fpsValues)
                      {
                          text += value.ToString() + "\n";
                      }
                      writer.Write(text);
                      writer.Close();

                  }
              }
          }

         */

    }




    private Vector3 GetBaseInput()
    {
        Vector3 velocity = new Vector3();
        if (Input.GetKey(KeyCode.W))
        {
            velocity += new Vector3(0, 0, 1);
        }
        if (Input.GetKey(KeyCode.S))
        {
            velocity += new Vector3(0, 0, -1);
        }
        if (Input.GetKey(KeyCode.A))
        {
            velocity += new Vector3(-1, 0, 0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            velocity += new Vector3(1, 0, 0);
        }
        return velocity;
    }
}