﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;

public class DataFetcher : MonoBehaviour
{

    private double maxLatitude;
    private double minLatitude;
    private double maxLongitude;
    private double minLongitude;

    


    //Spremanje informacija o geografskoj širini i dužini
    public void setGeoInfo(List<double> geoInf)
    {
        maxLatitude = geoInf[0];
        minLatitude = geoInf[1];
        maxLongitude = geoInf[2];
        minLongitude = geoInf[3];

    }


    public string Fetch(string externalDir, int terrainResolution, List<double> geoInfo)
    {
        setGeoInfo(geoInfo);
        string osmUrl = "https://www.openstreetmap.org/api/0.6/map?bbox=" + minLongitude + "%2C" + minLatitude + "%2C" + maxLongitude + "%2C" + maxLatitude;
        var response = new WebClient().DownloadString(osmUrl);
        return response;

    }

    //Vraćanje informacija o geografskoj širini i dužini
    public List<double> getGeoInfo()
    {
        List<double> geoInf = new List<double>();
        geoInf.Add(maxLatitude);
        geoInf.Add(minLatitude);
        geoInf.Add(maxLongitude);
        geoInf.Add(minLongitude);
        return geoInf;

    }





}
