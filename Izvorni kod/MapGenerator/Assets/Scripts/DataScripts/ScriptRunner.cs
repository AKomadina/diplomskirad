﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Net;

public class ScriptRunner : MonoBehaviour
{
    public float terrainScaleFactor;  // 1 metar u stvarnosti = terrainScaleFactor jedinica u virtualnom prostoru
    public double maxLatitude;
    public double minLatitude;
    public double maxLongitude;
    public double minLongitude;

    //Strukture za čuvanje podataka
    List<Vector2> trafficBoundPoints = new List<Vector2>();
    List<Vector2> wayBoundPoints = new List<Vector2>();
    List<Vector2> allBoundPoints = new List<Vector2>();
    List<Vector2> allRoadPoints = new List<Vector2>();
    List<Vector2> trafficRoadPoints = new List<Vector2>();
    List<Node> WayNodes = new List<Node>();
    Dictionary<Vector2, float> roadNodeWidthDict = new Dictionary<Vector2, float>();
    Dictionary<long, List<Vector2>> trafficBoundPointsDict = new Dictionary<long, List<Vector2>>();
    Dictionary<long, List<Vector2>> allBoundPointsDict = new Dictionary<long, List<Vector2>>();

    List<Node> nodes = new List<Node>();
    List<Node> plants = new List<Node>();
    List<Node> equipment = new List<Node>();
    List<Node> streetObjects = new List<Node>();

    List<Way> roads = new List<Way>();
    List<Way> railways = new List<Way>();
    List<Way> buildings = new List<Way>();

    List<MultiPolygon> shrubs = new List<MultiPolygon>();
    List<MultiPolygon> amenitys = new List<MultiPolygon>();
    List<MultiPolygon> barriers = new List<MultiPolygon>();
    List<MultiPolygon> historics = new List<MultiPolygon>();
    List<MultiPolygon> landuses = new List<MultiPolygon>();
    List<MultiPolygon> leisures = new List<MultiPolygon>();
    List<MultiPolygon> naturals = new List<MultiPolygon>();
    List<MultiPolygon> sports = new List<MultiPolygon>();
    List<MultiPolygon> manMade = new List<MultiPolygon>();

    //Vraćanje informacija o geografskoj širini i dužini
    public List<double> getGeoInfo()
    {
        List<double> geoInf = new List<double>();
        geoInf.Add(maxLatitude);
        geoInf.Add(minLatitude);
        geoInf.Add(maxLongitude);
        geoInf.Add(minLongitude);
        return geoInf;

    }

    //Spremanje informacija o geografskoj širini i dužini
    public void setGeoInfo(List<double> geoInf)
    {
        maxLatitude = geoInf[0];
        minLatitude = geoInf[1];
        maxLongitude = geoInf[2];
        minLongitude = geoInf[3];

    }

    public void Run(List<double> geoInfo, double scaleFactor, bool sateliteImage)
    {
        this.setGeoInfo(geoInfo);
        this.terrainScaleFactor = (float)scaleFactor;

        //Direktorij sa vanjskim komponentama
        string externalDir = Directory.GetCurrentDirectory() + "\\External";

        float startTotalTime = Time.realtimeSinceStartup;
        float startTime = Time.realtimeSinceStartup;

        //Pozivanje generatora terena
        GameObject terrainGeneratorGameObj = GameObject.Find("TerrainGenerator");
        TerrainGenerator terrainGen = (TerrainGenerator)terrainGeneratorGameObj.GetComponent(typeof(TerrainGenerator));
        GameObject terrain = terrainGen.BuildTerrain(externalDir, getGeoInfo(), terrainScaleFactor, sateliteImage);

        float endTime = Time.realtimeSinceStartup;
        Debug.Log("Terrain generated " + (endTime - startTime));

        

         startTime = Time.realtimeSinceStartup;
         //Pozivanje fetchera podataka
         GameObject scriptRunnerGameObj = GameObject.Find("ScriptRunner");
         DataFetcher fetcher = (DataFetcher)scriptRunnerGameObj.GetComponent(typeof(DataFetcher));
         string osmData = fetcher.Fetch(externalDir, terrain.GetComponent<Terrain>().terrainData.heightmapResolution, getGeoInfo());
         endTime = Time.realtimeSinceStartup;
         Debug.Log("Data fetched " + (endTime - startTime));


         startTime = Time.realtimeSinceStartup;
         //Pozivanje parsera podataka
         DataParser parser = (DataParser)scriptRunnerGameObj.GetComponent(typeof(DataParser));
         parser.setGeoInfo(fetcher.getGeoInfo());
         parser.Parse(osmData, terrain);
         nodes = parser.getNodes("osm");
         streetObjects = parser.getNodes("street");
         equipment = parser.getNodes("equipment");
         plants = parser.getNodes("plants");
         roads = parser.getWays("roads");
         buildings = parser.getWays("buildings");
         railways = parser.getWays("railways");
         barriers = parser.getPolygons("barrier");
         amenitys = parser.getPolygons("amenity");
         landuses = parser.getPolygons("landuse");
         leisures = parser.getPolygons("leisure");
         manMade = parser.getPolygons("man_made");
         sports = parser.getPolygons("sport");
         naturals = parser.getPolygons("natural");
         historics = parser.getPolygons("historic");
         shrubs = parser.getPolygons("shrubs");
         endTime = Time.realtimeSinceStartup;
         Debug.Log("Data parsed " + (endTime - startTime));



         startTime = Time.realtimeSinceStartup;
         //Pozivanje slikača terena
         GameObject terrainPaint = GameObject.Find("TerrainPainter");
         TerrainPainter terrainPainter = (TerrainPainter)terrainPaint.GetComponent(typeof(TerrainPainter));
         terrainPainter.Generate(terrain, terrainScaleFactor, nodes, roads, amenitys, landuses, leisures, manMade, sateliteImage);
         trafficBoundPoints = terrainPainter.getRoadBoundPoints("traffic");
         allBoundPoints = terrainPainter.getRoadBoundPoints("all");
         trafficRoadPoints = terrainPainter.getRoadPoints("traffic");
         allRoadPoints = terrainPainter.getRoadPoints("all");
         allBoundPointsDict = terrainPainter.getRoadBoundPointsDict("all");
         wayBoundPoints = terrainPainter.getRoadBoundPoints("rest");
         trafficBoundPointsDict = terrainPainter.getRoadBoundPointsDict("traffic");
         roadNodeWidthDict = terrainPainter.getNodesWidth();
         WayNodes = terrainPainter.getWayNodes();

         endTime = Time.realtimeSinceStartup;
         Debug.Log("Terrain painted " + (endTime - startTime));

        
        
         startTime = Time.realtimeSinceStartup;
         //Pozivanje generatora zgrada
         GameObject buildGen = GameObject.Find("BuildingGenerator");
         BuildingGenerator buildGenerator = (BuildingGenerator)buildGen.GetComponent(typeof(BuildingGenerator));
         buildGenerator.Generate(terrain, nodes, buildings, terrainScaleFactor, WayNodes);
         endTime = Time.realtimeSinceStartup;
         Debug.Log("Building generated " + (endTime - startTime));
        

        

         startTime = Time.realtimeSinceStartup;
         //Pozivanje generatora multi poligona
         GameObject polyGen = GameObject.Find("MultiPolygonGenerator");
         MultiPolygonGenerator polyGenerator = (MultiPolygonGenerator)polyGen.GetComponent(typeof(MultiPolygonGenerator));
         polyGenerator.Generate(terrain, nodes, amenitys, historics, landuses, leisures, naturals, sports, roads, manMade, allRoadPoints, terrainPainter.getWayNodes(), roads, terrainScaleFactor);
         endTime = Time.realtimeSinceStartup;
         Debug.Log("Multi polygon generated " + (endTime - startTime));

        
        
        
         startTime = Time.realtimeSinceStartup;
         //Pozivanje generatora OSM objekata
         GameObject streetObjGen = GameObject.Find("StreetObjectsGenerator");
         StreetObjectsGenerator streetObjGenerator = (StreetObjectsGenerator)streetObjGen.GetComponent(typeof(StreetObjectsGenerator));
         streetObjGenerator.Generate(terrain, nodes, streetObjects, allRoadPoints, trafficRoadPoints, trafficBoundPoints,wayBoundPoints ,allBoundPoints, roads, allBoundPointsDict, roadNodeWidthDict, terrainScaleFactor);
         endTime = Time.realtimeSinceStartup;
         Debug.Log("Street objects generated " + (endTime - startTime));
        
        
        
        
         startTime = Time.realtimeSinceStartup;
         //Pozivanje generatora linija
         GameObject lineGen = GameObject.Find("LineGenerator");
         LineGenerator lineGenerator = (LineGenerator)lineGen.GetComponent(typeof(LineGenerator));
         lineGenerator.Generate(terrain, nodes, streetObjects, barriers, railways,manMade, roads, terrainScaleFactor);
         endTime = Time.realtimeSinceStartup;
         Debug.Log("Lines generated " + (endTime - startTime));
        
        

         startTime = Time.realtimeSinceStartup;
         //Pozivanje generatora grmlja
         GameObject shrubGen = GameObject.Find("ShrubGenerator");
         ShrubGenerator shrubGenerator = (ShrubGenerator)shrubGen.GetComponent(typeof(ShrubGenerator));
         shrubGenerator.Generate(terrain, shrubs, terrainScaleFactor);
         endTime = Time.realtimeSinceStartup;
         Debug.Log("Shrubs generated " + (endTime - startTime));
        

       
         startTime = Time.realtimeSinceStartup;
         //Pozivanje generatora stabala
         GameObject treeGen = GameObject.Find("TreeGenerator");
         TreeGenerator treeGenerator = (TreeGenerator)treeGen.GetComponent(typeof(TreeGenerator));
         treeGenerator.Generate(terrain, plants, terrainScaleFactor);
         endTime = Time.realtimeSinceStartup;
         Debug.Log("Trees generated " + (endTime - startTime));


        
        startTime = Time.realtimeSinceStartup;
        //Pozivanje generatora javne opreme
        GameObject equipGen = GameObject.Find("EquipmentGenerator");
        EquipmentGenerator equipmentGenerator = (EquipmentGenerator)equipGen.GetComponent(typeof(EquipmentGenerator));
        equipmentGenerator.Generate(terrain, nodes, equipment, terrainScaleFactor, allRoadPoints);
        endTime = Time.realtimeSinceStartup;
        Debug.Log("Equipment generated " + (endTime - startTime));

    

        //Sakrij glavni izbornik
        GameObject canvasGameObj = GameObject.Find("Canvas");
        canvasGameObj.SetActive(false);

        float endTotalTime = Time.realtimeSinceStartup;
        Debug.Log("Total time " + (endTotalTime - startTotalTime)+" sec");

        
        GameObject sceneCameraGameObj = GameObject.Find("SceneCamera2");
        SceneCamera sceneCam = (SceneCamera)sceneCameraGameObj.GetComponent(typeof(SceneCamera));
        sceneCam.Run(terrain, this.getGeoInfo());
      
      
    }


}
