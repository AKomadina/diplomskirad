﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;


public class ShrubGenerator : MonoBehaviour
{

    private List<MultiPolygon> shrubs = new List<MultiPolygon>();
    private double terrainScaleFactor;
    public GameObject shrubObject;
    public GameObject NodeObject;
    public Terrain terrain;

    public void Generate(GameObject terrain, List<MultiPolygon> polygons, double terrainScaleFactor)
    {
        this.shrubs = polygons;
        this.terrainScaleFactor = terrainScaleFactor;
        this.terrain = terrain.GetComponent<Terrain>();


        //Stvaranje grmlja
        foreach (MultiPolygon shrub in polygons)
        {
            if(shrub.positions.Count != 0)
            {
                //Dobivanje bounding boxa poligona unutar kojeg raste grmlje
                List<float> boundingBox = Methods.getBoundingBox(shrub.positions);
                float minX = boundingBox[0];
                float maxX = boundingBox[1];
                float minZ = boundingBox[2];
                float maxZ = boundingBox[3];

                Vector3 centroid = Methods.getCentroid(shrub.positions.ToArray());

                
                //Stvaranje grmlja unutar bounding boxa po redu
                float step = 2f * (float)terrainScaleFactor;
                bool hasBeenInstantiated = false;
                for (float i = minX ; i < maxX ; i = i + step)
                {
                    for (float j = minZ ; j < maxZ; j = j + step)
                    {
                        Vector3 currPos = new Vector3(i, 0f, j);
                        currPos.y = this.terrain.SampleHeight(currPos);
                        if (Methods.PointInPolygon(shrub.positions, currPos))
                        {
                            InstantiateShrub(currPos, shrub.id);
                            hasBeenInstantiated = true;
                        }
                       
                    }
                }
                //Ako nije dovoljno velik prostor za raspored instanciraj samo jednog u centoridu
                if (!hasBeenInstantiated)
                {
                    InstantiateShrub(centroid, shrub.id);
                }

                
            }
           
        }
     

    }

    //Metoda za instanciranje grma
    public void InstantiateShrub(Vector3 position, long id)
    {
        GameObject nodeObj = Instantiate(shrubObject);
        Transform nodeObjT = nodeObj.transform;
        nodeObjT.localScale = new Vector3(40f * (float)terrainScaleFactor, 40f * (float)terrainScaleFactor, 40f * (float)terrainScaleFactor);
        nodeObjT.localPosition = position;
        nodeObj.name = "Grm " + id;
    }


  



}
