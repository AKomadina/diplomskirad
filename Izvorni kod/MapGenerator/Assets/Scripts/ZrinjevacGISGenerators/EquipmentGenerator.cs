﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;


public class EquipmentGenerator : MonoBehaviour
{
    public GameObject trashObject;
    public GameObject tableObject;
    public GameObject benchObject;
    public GameObject bench2Object;
    public GameObject fountainObject;
    public GameObject slideObject;
    public GameObject carouselObject;
    public GameObject swingObject;
    public GameObject springObject;
    public GameObject climberObject;
    public GameObject sandObject;
    public GameObject seesawObject;
    public GameObject foldObject;
    public GameObject dipObject;
    public GameObject NodeObject;

    private List<Node> equipment = new List<Node>();
    private double terrainScaleFactor;
    private List<GameObject> equipGameObjects;

    private Terrain terrain;

    private List<Node> nodes;
    private List<Vector2> allRoadPoints;

    List<GameObject> benchObjects = new List<GameObject>();
    List<Node> tableObjects = new List<Node>();

    public void Generate(GameObject terrain, List<Node> allNodes, List<Node> EquipNodes, double terrainScaleFactor,  List<Vector2> allRoadPoints)
    {
        this.nodes = allNodes;
        equipGameObjects = new List<GameObject>();
        this.equipment = EquipNodes;
        this.terrainScaleFactor = terrainScaleFactor;
        this.allRoadPoints = allRoadPoints;
        this.terrain = terrain.GetComponent<Terrain>();

        //Traženje stolova
        foreach (Node equip in equipment)
        {
            if (equip.type.Contains("stol"))
            {
                tableObjects.Add(equip);
            }
        }

        //Stvaranje javne opreme
        foreach (Node equip in equipment)
        {

            InstantiateEquipment(equip);

        }

    }

    //Metoda za stvaranje objekta koji predstavljaju opremu
    void InstantiateEquipment(Node node)
    {
        
        if (node.type.Contains("kosarica"))
        {
            GameObject nodeObj = Instantiate(trashObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localPosition = new Vector3(node.position.x, node.position.y - (float)(0.75f*0.25f*terrainScaleFactor) , node.position.z);
            nodeObjT.localScale = new Vector3(0.8f * 0.25f * (float)terrainScaleFactor, 0.8f * 0.25f * (float)terrainScaleFactor, 0.8f * 0.25f * (float)terrainScaleFactor);
            nodeObj.name = "Kosarica "+node.id;
        }
        else if (node.type.Contains("stol"))
        {
            GameObject nodeObj = Instantiate(tableObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localPosition = new Vector3(node.position.x - (float)(0.25f * 1f * terrainScaleFactor), node.position.y, node.position.z);
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 2f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            nodeObjT.eulerAngles = new Vector3(
                nodeObjT.eulerAngles.x ,
                nodeObjT.eulerAngles.y ,
                nodeObjT.eulerAngles.z
            );
            nodeObj.name = "Stol " + node.id;
        }
        else if (node.type.Contains("klupa"))
        {
            
            GameObject nodeObj;
            //Ako se nalazi uza stol
            if (Methods.hasBeenNodeInRange(node, tableObjects, 1 * terrainScaleFactor))
            {
                nodeObj = Instantiate(bench2Object);
                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localPosition = new Vector3(node.position.x , node.position.y, node.position.z);
                nodeObjT.localScale = new Vector3(0.8f * (float)terrainScaleFactor, 0.8f * (float)terrainScaleFactor, 0.8f * (float)terrainScaleFactor);
                nodeObjT.eulerAngles = new Vector3(
                 nodeObjT.eulerAngles.x,
                 nodeObjT.eulerAngles.y +90f,
                 nodeObjT.eulerAngles.z
               );
                benchObjects.Add(nodeObj);
            }
            else
            {

                if (node.type.Contains("bez"))
                {
                    nodeObj = Instantiate(bench2Object);
                }
                else
                {
                    nodeObj = Instantiate(benchObject);
                }

                Transform nodeObjT = nodeObj.transform;
                nodeObjT.localPosition = new Vector3(node.position.x , node.position.y, node.position.z );
                nodeObjT.localScale = new Vector3(0.8f * (float)terrainScaleFactor, 0.8f * (float)terrainScaleFactor, 0.8f * (float)terrainScaleFactor);

                Vector3 closestRoad = Methods.ClosestRoad(nodeObjT.localPosition, this.allRoadPoints, this.terrain);


                nodeObjT.eulerAngles = new Vector3(
                  nodeObjT.eulerAngles.x,
                  Quaternion.LookRotation(closestRoad - nodeObjT.localPosition, Vector3.forward).eulerAngles.y,
                  nodeObjT.eulerAngles.z
                );

                //Ako postoji neka klupa da se preklapaju uništi ju
                if (Methods.hasBeenObjectInRange(nodeObj, benchObjects, 2 * terrainScaleFactor))
                {
                    Destroy(nodeObj);
                }
                else
                {
                    benchObjects.Add(nodeObj);
                }

            }
           
            nodeObj.name = "Klupa " + node.id;
  

        }
        else if (node.type.Contains("fontana"))
        {
            GameObject nodeObj = Instantiate(fountainObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localPosition = node.position;
            nodeObjT.localScale = new Vector3(0.67f * (float)terrainScaleFactor, 0.67f * (float)terrainScaleFactor, 0.67f * (float)terrainScaleFactor);
            nodeObj.name = "Fontana " + node.id;

            
        }
        else if (node.type.Contains("tobogan"))
        {
            GameObject nodeObj = Instantiate(slideObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localPosition = node.position;
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            nodeObj.name = "Tobogan " + node.id;
        }
        else if (node.type.Contains("vrtuljak"))
        {
            GameObject nodeObj = Instantiate(carouselObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localPosition = node.position;
            nodeObjT.localScale = new Vector3(0.016f * (float)terrainScaleFactor, 0.016f * (float)terrainScaleFactor, 0.016f * (float)terrainScaleFactor);
            nodeObj.name = "Vrtuljak " + node.id;
        }
        else if (node.type.Contains("ljuljacka"))
        {
            GameObject nodeObj = Instantiate(swingObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localPosition = node.position;
            nodeObjT.localScale = new Vector3(0.85f * (float)terrainScaleFactor, 0.85f * (float)terrainScaleFactor, 0.85f * (float)terrainScaleFactor);
            nodeObj.name = "Ljuljacka " + node.id;
        }
        else if (node.type.Contains("opruzi"))
        {
            GameObject nodeObj = Instantiate(springObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localPosition = new Vector3(node.position.x + (float)(0.3f * 1.5f * terrainScaleFactor), node.position.y + (float)(1.03f * 1.5f * terrainScaleFactor), node.position.z - (float)(0.335f * 1.5f * terrainScaleFactor));
            nodeObjT.localScale = new Vector3(1.5f * (float)terrainScaleFactor, 1.5f * (float)terrainScaleFactor, 1.5f * (float)terrainScaleFactor);
            nodeObj.name = "Igracka na opruzi " + node.id;
        }
        else if (node.type.Contains("penjalica"))
        {
            GameObject nodeObj = Instantiate(climberObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localPosition = node.position;
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            nodeObj.name = "Penjalica " + node.id;
        }
        else if (node.type.Contains("pjescanik"))
        {
            GameObject nodeObj = Instantiate(sandObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localPosition = node.position;
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            nodeObj.name = "Pjescanik " + node.id;
        }
        else if (node.type.Contains("klackalica"))
        {
            GameObject nodeObj = Instantiate(seesawObject);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localPosition = node.position;
            nodeObjT.localScale = new Vector3(0.2f * (float)terrainScaleFactor, 0.2f * (float)terrainScaleFactor, 0.2f * (float)terrainScaleFactor);
            nodeObj.name = "Klackalica " + node.id;
        }
        else if (node.type.Contains("sprava"))
        {
            System.Random rnd = new System.Random();
            int randomType = rnd.Next(0, 2);
            GameObject nodeObj;
            if (randomType == 0)
            {
                nodeObj = Instantiate(foldObject);
            }
            else
            {
                nodeObj = Instantiate(dipObject);
            }
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localPosition = node.position;
            nodeObjT.localScale = new Vector3(1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor, 1f * (float)terrainScaleFactor);
            nodeObj.name = "Sprava " + node.id;
        }


    }

   
    //Metoda za pronalazak objekta čvora na temelju njegovog id-a
    Node findNode(long nodeId)
    {
        foreach (Node n in nodes)
        {
            if (n.id == nodeId)
            {
                return n;
            }
        }
        return null;
    }
    
 


}
