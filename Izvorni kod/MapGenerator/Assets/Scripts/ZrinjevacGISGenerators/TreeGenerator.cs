﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;


public class TreeGenerator : MonoBehaviour
{

    private List<Node> plants = new List<Node>();
    private double terrainScaleFactor;
    private List<GameObject> plantGameObjects;

    public GameObject breza;
    public GameObject platana;
    public GameObject cempres;
    public GameObject kesten;
    public GameObject hrast;
    public GameObject bor;
    public GameObject grab;
    public GameObject javor;
    public GameObject smreka;
    public GameObject jasen;
    public GameObject jablan;
    public GameObject brijest;
    public GameObject lijeska;
    public GameObject tuja;
    public GameObject breskva;
    public GameObject jabuka;
    public GameObject kruska;



    public void Generate(GameObject terrain, List<Node> nodes, double terrainScaleFactor)
    {
        plantGameObjects = new List<GameObject>();
        this.plants = nodes;
        this.terrainScaleFactor = terrainScaleFactor;


        //Stvaranje vegetacije
        foreach (Node plant in plants)
        {

            InstantiateTree(plant);
        }

    }

    //Metoda za stvaranje objekta koji predstavlja stablo
    void InstantiateTree(Node node)
    {
        float height = 0f;
        try
        {
            height = float.Parse(node.height);
        }
        catch
        {
            
        }

        bool flagType = false;
        foreach(string type in Constants.treeTypes)
        {
            if (node.type.ToLower().Contains(type))
            {
                flagType = true;
                break;
            }
        }

        string treeType = "";
        if(flagType == false)
        {
            System.Random rnd = new System.Random();
            int randomType = rnd.Next(0, Constants.treeTypes.Count);
            treeType = Constants.treeTypes[randomType];
        }
        else
        {
            treeType = node.type.ToLower();
        }

        if (treeType.Contains("breza"))
        {
            GameObject nodeObj = Instantiate(breza);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height*10f *(float)terrainScaleFactor, height * 10f * (float)terrainScaleFactor, height * 10f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Breza " + node.id;
        }
        else if(treeType.Contains("platana"))
        {
            GameObject nodeObj = Instantiate(platana);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 0.04f *(float)terrainScaleFactor, height * 0.04f * (float)terrainScaleFactor, height * 0.04f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Platana " + node.id;
        }
        else if (treeType.Contains("čempres"))
        {
            GameObject nodeObj = Instantiate(cempres);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 0.15f *(float)terrainScaleFactor, height * 0.15f * (float)terrainScaleFactor, height * 0.15f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Čempres " + node.id;
        }
        else if (treeType.Contains("kesten"))
        {
            GameObject nodeObj = Instantiate(kesten);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 0.11f *(float)terrainScaleFactor, height * 0.11f * (float)terrainScaleFactor, height * 0.11f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Kesten " + node.id;
        }
        else if (treeType.Contains("hrast"))
        {
            GameObject nodeObj = Instantiate(hrast);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 0.23f *(float)terrainScaleFactor, height * 0.23f * (float)terrainScaleFactor, height * 0.23f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Hrast " + node.id;

        }
        else if (treeType.Contains("bor") || treeType.Contains("cedar"))
        {
            GameObject nodeObj = Instantiate(bor);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 20f *(float)terrainScaleFactor, height * 20f * (float)terrainScaleFactor, height * 20f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Bor " + node.id;
        }
        else if (treeType.Contains("grab"))
        {
            GameObject nodeObj = Instantiate(grab);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 0.4f *(float)terrainScaleFactor, height * 0.4f * (float)terrainScaleFactor, height * 0.4f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Grab " + node.id;
        }
        else if (treeType.Contains("javor") || treeType.Contains("klen"))
        {
            GameObject nodeObj = Instantiate(javor);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 0.08f * (float)terrainScaleFactor, height * 0.08f * (float)terrainScaleFactor, height * 0.08f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Javor " + node.id;
        }
        else if (treeType.Contains("smreka") || treeType.Contains("omorika"))
        {
            GameObject nodeObj = Instantiate(smreka);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 0.04f * (float)terrainScaleFactor, height * 0.04f * (float)terrainScaleFactor, height * 0.04f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Smreka " + node.id;
        }
        else if (treeType.Contains("jasen"))
        {
            GameObject nodeObj = Instantiate(jasen);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 7f * (float)terrainScaleFactor, height * 7f * (float)terrainScaleFactor, height * 7f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Jasen " + node.id;
        }
        else if (treeType.Contains("jablan") || treeType.Contains("topola"))
        {
            GameObject nodeObj = Instantiate(jablan);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 9f * (float)terrainScaleFactor, height * 9f * (float)terrainScaleFactor, height * 9f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Jablan " + node.id;

        }
        else if (treeType.Contains("brijest"))
        {
            GameObject nodeObj = Instantiate(brijest);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 1.1f * (float)terrainScaleFactor, height * 1.1f * (float)terrainScaleFactor, height * 3.6f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Brijest " + node.id;
        }
        else if (treeType.Contains("lijeska"))
        {
            GameObject nodeObj = Instantiate(lijeska);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 0.055f * (float)terrainScaleFactor, height * 0.055f * (float)terrainScaleFactor, height * 0.055f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Lijeska " + node.id;
        }
        else if (treeType.Contains("tuja"))
        {
            GameObject nodeObj = Instantiate(tuja);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 0.06f * (float)terrainScaleFactor, height * 0.06f * (float)terrainScaleFactor, height * 0.06f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Tuja " + node.id;
        }
        else if (treeType.Contains("breskva"))
        {
            GameObject nodeObj = Instantiate(breskva);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 1.4f * (float)terrainScaleFactor, height * 1.4f * (float)terrainScaleFactor, height * 1.4f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Breskva " + node.id;
        }
        else if (treeType.Contains("jabuka"))
        {
            GameObject nodeObj = Instantiate(jabuka);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 1.4f * (float)terrainScaleFactor, height * 1.4f * (float)terrainScaleFactor, height * 1.4f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Jabuka " + node.id;

        }
        else if (treeType.Contains("kruška"))
        {
            GameObject nodeObj = Instantiate(kruska);
            Transform nodeObjT = nodeObj.transform;
            nodeObjT.localScale = new Vector3(height * 1.4f * (float)terrainScaleFactor, height * 1.4f * (float)terrainScaleFactor, height * 1.4f * (float)terrainScaleFactor);
            nodeObjT.localPosition = node.position;
            nodeObj.name = "Kruška " + node.id;
        }
               
    }




}
