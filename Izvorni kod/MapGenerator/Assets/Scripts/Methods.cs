﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Net;
using System.Linq;

public static class Methods
{

    //Metoda za pretvorbu EPSG4326 koordinata u EPSG3857
    public static double[] transformCoordinatesEPSG4326To3857(double longitude, double latitude)
    {
        double x = longitude * 20037508.34 / 180;
        double y = Math.Log(Math.Tan((90 + latitude) * Math.PI / 360)) / (Math.PI / 180);
        y = y * 20037508.34 / 180;
        return new double[] { x, y };
    }



    //Metoda za traženje bounding boxa od niza točaka poligona
    public static List<float> getBoundingBox(List<Vector3> points)
    {
        float maxX = float.MinValue;
        float minX = float.MaxValue;
        float maxZ = float.MinValue;
        float minZ = float.MaxValue;

        for (int i = 0; i < points.Count; ++i)
        {
            float currX = points[i].x;
            float currZ = points[i].z;

            if (currX <= minX)
            {
                minX = currX;
            }
            if (currX >= maxX)
            {
                maxX = currX;
            }
            if (currZ <= minZ)
            {
                minZ = currZ;
            }
            if (currZ >= maxZ)
            {
                maxZ = currZ;
            }

        }

        List<float> boundingBox = new List<float>();
        boundingBox.Add(minX);
        boundingBox.Add(maxX);
        boundingBox.Add(minZ);
        boundingBox.Add(maxZ);
        return boundingBox;

    }

    //Metoda za traženje bounding box vektora od niza točaka poligona
    public static List<Vector3> getBoundingBoxVectors(List<Vector3> points)
    {
        float maxX = float.MinValue;
        float minX = float.MaxValue;
        float maxZ = float.MinValue;
        float minZ = float.MaxValue;

        for (int i = 0; i < points.Count; ++i)
        {
            float currX = points[i].x;
            float currZ = points[i].z;

            if (currX <= minX)
            {
                minX = currX;
            }
            if (currX >= maxX)
            {
                maxX = currX;
            }
            if (currZ <= minZ)
            {
                minZ = currZ;
            }
            if (currZ >= maxZ)
            {
                maxZ = currZ;
            }

        }

        List<Vector3> boundingBox = new List<Vector3>();
        boundingBox.Add(new Vector3(minX, 0f, minZ));
        boundingBox.Add(new Vector3(minX, 0f, maxZ));
        boundingBox.Add(new Vector3(maxX, 0f, maxZ));
        boundingBox.Add(new Vector3(maxX, 0f, minZ));
        return boundingBox;

    }


    //Metoda za generianje UV koordinata za multi poligone
    public static Vector2[] generateUVs(Vector3[] vertices, float multiplier)
    {
        if (vertices.Length == 3)
        {

            double area = Math.Abs(Methods.signedAreaOfPolygon(new List<Vector3>(vertices)));
            float scale = (float)Math.Sqrt(area) / multiplier;
            return new Vector2[] { new Vector2(0, 0), new Vector2(0, scale), new Vector2(scale, scale) };


        }

        Vector2[] uv = new Vector2[vertices.Length];
        List<float> boundingBox = getBoundingBox(new List<Vector3>(vertices));
        float minX = boundingBox[0];
        float maxX = boundingBox[1];
        float minZ = boundingBox[2];
        float maxZ = boundingBox[3];

        for (int i = 0; i < vertices.Length; ++i)
        {
            uv[i] = new Vector2(multiplier * vertices[i].x / ((float)maxX), multiplier * vertices[i].z / ((float)maxZ));

        }
        return uv;
    }


    //Metoda za traženje centroida iz niza točaka
    public static Vector3 getCentroid(Vector3[] points)
    {
        int len = points.Length;
        float sumX = 0;
        float sumY = 0;
        float sumZ = 0;
        for (int i = 0; i < len; ++i)
        {
            sumX += points[i].x;
            sumY += points[i].y;
            sumZ += points[i].z;
        }
        Vector3 centroid = new Vector3(sumX / len, sumY / len, sumZ / len);
        return centroid;
    }

    //Metoda za traženje centroida iz niza točaka 2d
    public static Vector3 getCentroid2D(Vector2[] points)
    {
        int len = points.Length;
        float sumX = 0;
        float sumZ = 0;
        for (int i = 0; i < len; ++i)
        {
            sumX += points[i].x;
            sumZ += points[i].y;
        }
        Vector2 centroid = new Vector3(sumX / len,  sumZ / len);
        return centroid;
    }





    //Stvaranje redoslijeda trokuta u poligonu
    public static int[] makeTriangles(List<Vector3> vertices3D)
    {
        if(isPolygonConvex(vertices3D, true))
        {
            List<int> triangles = new List<int>();
            if(isPolygonClockwise(vertices3D ))
            {
                for (int i = 2; i < vertices3D.Count; i++)
                {
                    triangles.Add(0);
                    triangles.Add((i - 1)% vertices3D.Count);
                    triangles.Add((i) % vertices3D.Count);
                }
            }
            else
            {
                for (int i = 2; i < vertices3D.Count; i++)
                {
                    triangles.Add(0);
                    triangles.Add((i) % vertices3D.Count);
                    triangles.Add((i - 1) % vertices3D.Count);            
                }
            }
            return triangles.ToArray();
        }
        else
        {
            Vector2[] vertices2D = new Vector2[vertices3D.Count];
            for (int i = 0; i < vertices3D.Count; ++i)
            {

                Vector2 point = new Vector2(vertices3D[i].x, vertices3D[i].z);
                vertices2D[i] = point;
            }

            Triangulator tr = new Triangulator(vertices2D);
            int[] triangles = tr.Triangulate();
            return triangles;
        }
    }

   



    //Udaljenost 2 točke u 2D prosotru
    public static double Distance2D(Vector3 vert1, Vector3 vert2)
    {
        return Math.Sqrt((vert1.x - vert2.x) * (vert1.x - vert2.x) + (vert1.z - vert2.z) * (vert1.z - vert2.z));
    }

    //Udaljenost 2 točke u 3D prosotru
    public static double Distance3D(Vector3 vert1, Vector3 vert2)
    {
        return Math.Sqrt((vert1.x - vert2.x) * (vert1.x - vert2.x) + (vert1.y - vert2.y) * (vert1.y - vert2.y) + (vert1.z - vert2.z) * (vert1.z - vert2.z));
    }

    //Udaljenost 2 točke u prosotru
    public static Vector3 getMiddlePoint(Vector3 vert1, Vector3 vert2)
    {
        float newX = (vert1.x + vert2.x) / 2;
        float newZ = (vert1.z + vert2.z) / 2;
        return new Vector3(newX, vert1.y, newZ);
    }




    //Stvaranje UV za teksturiranje quad mesha
    public static Vector2[] generateQuadUVs(Vector3[] vertices, float divider)
    {
        float maxX = vertices[0].x;
        float minX = vertices[0].x;
        float maxY = vertices[0].y;
        float minY = vertices[0].y;
        float maxZ = vertices[0].z;
        float minZ = vertices[0].z;
        for (int i = 0; i < vertices.Length; ++i)
        {
            float x = vertices[i].x;
            float y = vertices[i].y;
            float z = vertices[i].z;
            if (x > maxX)
            {
                maxX = x;
            }
            else if (x < minX)
            {
                minX = x;
            }
            if (y > maxY)
            {
                maxY = y;
            }
            else if (y < minY)
            {
                minY = y;
            }
            if (z > maxZ)
            {
                maxZ = z;
            }
            else if (z < minZ)
            {
                minZ = z;
            }

        }
        float distX = maxX - minX;
        float distY = maxY - minY;
        float distZ = maxZ - minZ;
        List<float> dist = new List<float>();
        dist.Add(distX);
        dist.Add(distZ);
        float max2 = dist.Max();

        return new Vector2[] { new Vector2(0, 0), new Vector2(0, (float)max2 / divider), new Vector2((float)distY / divider, (float)max2 / divider), new Vector2((float)distY / divider, 0) };
    }


    //Stvaranje UV za teksturiranje triangle mesha
    public static Vector2[] generateTriUVs(Vector3[] vertices, float divider)
    {
        double area = Math.Abs( Methods.signedAreaOfPolygon(new List<Vector3> (vertices)) );
        float scale = (float)Math.Sqrt(area) / divider;
        return new Vector2[] { new Vector2(0, 0), new Vector2(0, scale), new Vector2(scale, scale) };

    }

    //Pretvorba stupnjeva u radijane
    public static double toRad(double x)
    {
        return x * Math.PI / 180;
    }

    //Udaljenost 2 točke u prosotoru na temelju geo. dužine i širine (u m)
    public static double SphericalCosinus(double lat1, double lon1, double lat2, double lon2)
    {

        double R = 6371.0;
        double dLon = toRad(lon2 - lon1);
        lat1 = toRad(lat1);
        lat2 = toRad(lat2);
        double distance = Math.Acos(Math.Sin(lat1) * Math.Sin(lat2) + Math.Cos(lat1) * Math.Cos(lat2) * Math.Cos(dLon)) * R;
        return distance * 1000;
    }


    //Provjeri nalazi li se neka točka u zadanom rasponu(granicama)
    public static bool isInBounds(int boundXmin, int boundXmax, int boundYmin, int boundYmax, int x, int y)
    {
        if (x < boundXmin || x > boundXmax)
        {
            return false;
        }
        if (y < boundYmin || y > boundYmax)
        {
            return false;
        }
        return true;
    }

    //Provjeri nalazi li se neka točka u zadanom rasponu(granicama) float
    public static bool isInBoundsFloat(float boundXmin, float boundXmax, float boundYmin, float boundYmax, float x, float y)
    {
        if (x < boundXmin || x > boundXmax)
        {
            return false;
        }
        if (y < boundYmin || y > boundYmax)
        {
            return false;
        }
        return true;
    }

    //Metoda za traženje bounding boxa od niza točaka poligona 2D
    public static List<float> getBoundingBox2D(List<Vector2> points)
    {
        float maxX = float.MinValue;
        float minX = float.MaxValue;
        float maxY = float.MinValue;
        float minY = float.MaxValue;

        for (int i = 0; i < points.Count; ++i)
        {
            float currX = points[i].x;
            float currY = points[i].y;

            if (currX <= minX)
            {
                minX = currX;
            }
            if (currX >= maxX)
            {
                maxX = currX;
            }
            if (currY <= minY)
            {
                minY = currY;
            }
            if (currY >= maxY)
            {
                maxY = currY;
            }

        }

        List<float> boundingBox = new List<float>();
        boundingBox.Add(minX);
        boundingBox.Add(maxX);
        boundingBox.Add(minY);
        boundingBox.Add(maxY);
        return boundingBox;

    }

    //Ako je površina manja od 0 onda je clockwise

    //Metoda za izračun površine poligona
    public static double signedAreaOfPolygon(List<Vector3> vertices)
    {
        double sum = 0;
        if (vertices.Count < 3)
        {
            return 0;
        }
        for (int k = 0; k < vertices.Count; ++k)
        {
            Vector2 v1 = new Vector2(vertices[k].x, vertices[k].z);
            Vector2 v2 = new Vector2(vertices[(k + 1) % vertices.Count].x, vertices[(k + 1) % vertices.Count].z);
            double crossProd = CrossProduct2D(v1, v2);
            sum += crossProd;
        }
        return sum / 2;
    }

    public static double CrossProduct2D(Vector2 v1, Vector2 v2)
    {
        return ((v1.x * v2.y) - (v1.y * v2.x));
    }

    public static bool isPolygonClockwise(List<Vector3> vertices)
    {
        if (signedAreaOfPolygon(vertices) < 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

 // For each set of three adjacent points A, B, C,
        // find the cross product AB · BC. If the sign of
        // all the cross products is the same, the angles
        // are all positive or negative (depending on the
        // order in which we visit them) so the polygon
        // is convex.
    //Metoda za određivanje konveksnosti poligona
    public static bool isPolygonConvex(List<Vector3> vertices, bool semi)
    {
        bool negative = false;
        bool positive = false;
        for (int k = 0; k < vertices.Count; ++k)
        {
            Vector2 v1 = new Vector2(vertices[k].x, vertices[k].z);
            Vector2 v2 = new Vector2(vertices[(k + 1) % vertices.Count].x, vertices[(k + 1) % vertices.Count].z);
            Vector2 v3 = new Vector2(vertices[(k + 2) % vertices.Count].x, vertices[(k + 2) % vertices.Count].z);
            Vector2 dir1 = v1 - v2;
            Vector2 dir2 = v3 - v2;
            //AKo je clockwise produkt je > 0
            double crossProd = CrossProduct2D(dir1.normalized, dir2.normalized);
            if (semi)
            {
                if (Math.Abs(crossProd) < 0.015) {
                    continue;
                }
            }
            if (crossProd < 0)
            {
                negative = true;
            }
            else if (crossProd > 0)
            {
                positive = true;
            }
            if (negative && positive)
            {
                return false;
            }
        }
        return true;
    }

    //Metoda koja provjerava nalazi li se točka unutar poligona 
    public static bool PointInPolygon(List<Vector3> vertices, Vector3 point)
    {
        double minX = vertices[0].x;
        double maxX = vertices[0].x;
        double minZ = vertices[0].z;
        double maxZ = vertices[0].z;
        for (int i = 1; i < vertices.Count; i++)
        {
            Vector3 p = vertices[i];
            minX = Math.Min(p.x, minX);
            maxX = Math.Max(p.x, maxX);
            minZ = Math.Min(p.z, minZ);
            maxZ = Math.Max(p.z, maxZ);
        }

        if (point.x < minX || point.x > maxX || point.z < minZ || point.z > maxZ)
        {
            return false;
        }

    
        bool inside = false;
        for (int i = 0, j = vertices.Count - 1; i < vertices.Count; j = i++)
        {
            if ((vertices[i].z > point.z) != (vertices[j].z > point.z) &&
                point.x < (vertices[j].x - vertices[i].x) * (point.z - vertices[i].z) / (vertices[j].z - vertices[i].z) + vertices[i].x)
            {
                inside = !inside;
            }
        }

        return inside;
    
    }

    //Metoda koja provjerava nalazi li se točka unutar poligona 2D
    public static bool PointInPolygon2D(List<Vector2> vertices, Vector2 point)
    {
        double minX = vertices[0].x;
        double maxX = vertices[0].x;
        double minZ = vertices[0].y;
        double maxZ = vertices[0].y;
        for (int i = 1; i < vertices.Count; i++)
        {
            Vector3 p = vertices[i];
            minX = Math.Min(p.x, minX);
            maxX = Math.Max(p.x, maxX);
            minZ = Math.Min(p.y, minZ);
            maxZ = Math.Max(p.y, maxZ);
        }

        if (point.x < minX || point.x > maxX || point.y < minZ || point.y > maxZ)
        {
            return false;
        }


        bool inside = false;
        for (int i = 0, j = vertices.Count - 1; i < vertices.Count; j = i++)
        {
            if ((vertices[i].y > point.y) != (vertices[j].y > point.y) &&
                point.x < (vertices[j].x - vertices[i].x) * (point.y - vertices[i].y) / (vertices[j].y - vertices[i].y) + vertices[i].x)
            {
                inside = !inside;
            }
        }

        return inside;

    }




    //Metoda linearne interpolacije točaka
    public static List<Vector2> Lerp(Vector3 start, Vector3 end, int precision)
    {


        List<Vector2> pointsArray = new List<Vector2>();

        for (float t = 0; t <= precision; t++)
        {
            float T = (float)(t / precision);

            Vector3 interpolated = Vector3.Lerp(start, end, T);

            pointsArray.Add(new Vector2(interpolated.x, interpolated.z));

        }


        return pointsArray;
    }




    //Metoda koja određuje jesu li vektori slični
    public static bool VectorsSimilar(Vector3 me, Vector3 other, double terrainScaleFactor)
    {
        double allowedDifference = 0.05 * terrainScaleFactor;
        double dist = Distance3D(me, other);

        if (dist <= allowedDifference)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Metoda koja određuje jesu li vektori slični
    public static bool QuaternionSimilar(Quaternion me, Quaternion other, double terrainScaleFactor)
    {
        double allowedDifference = 0.01 * terrainScaleFactor;
        return Quaternion.Dot(me, other) > 1f - allowedDifference;
    }


    //Pronalazak vektora smjera između 2 susjedna čvora
    public static string findDirection(Node current, Node prev)
    {
        Vector3 currPos = current.position;
        Vector3 prevPos = prev.position;
        Vector3 dir = currPos - prevPos;

        float angle = Vector3.Angle(dir, new Vector3(1f, 0f, 0f));
        if (angle >= 45f && angle <= 135f)
        {
            return "z";
        }
        else
        {
            return "x";
        }
    }

    //Pronalazak vektora smjera između 2 susjedna vektora
    public static string findDirectionVector(Vector3 vec1, Vector3 vec2)
    {
 
        Vector3 dir = vec1 - vec2;

        float angle = Vector3.Angle(dir, new Vector3(1f, 0f, 0f));
        if (angle >= 45f && angle <= 135f)
        {
            return "z";
        }
        else
        {
            return "x";
        }
    }

    //Pronalazak vektora smjera između 2 susjedna čvora
    public static string findRoofDirection(Vector3[] vertices)
    {
        Vector3 dir = vertices[0] - vertices[1];

        float angle = Vector3.Angle(dir, new Vector3(1f, 0f, 0f));
        if (angle >= 45f && angle <= 135f)
        {
            return "z";
        }
        else
        {
            return "x";
        }
    }

  

    //Metoda koja random izabire boja zida
    public static Color getWallColor()
    {
        System.Random rand = new System.Random();
        double val = rand.NextDouble();
        Color newColor;

        //28%
        if (val < 0.28)
        {
            newColor = Color.white;
        }
        //22%
        else if (val >= 0.28 && val < 0.50)
        {
            newColor = Color.gray;
        }
        //10%
        else if (val >= 0.50 && val < 0.60)
        {
            newColor = Color.green;
        }
        //8%
        else if (val >= 0.60 && val < 0.68)
        {
            newColor = Color.red;
        }
        //18%
        else if (val >= 0.68 && val < 0.86)
        {
            newColor = Color.yellow;
        }
        //5%
        else if (val >= 0.86 && val < 0.91)
        {
            newColor = Color.blue;
        }
        //3%
        else if (val >= 0.91 && val < 0.94)
        {
            newColor = Color.magenta;
        }
        //3%
        else if (val >= 0.94 && val < 0.97)
        {
            newColor = Color.cyan;
        }
        //2%
        else if (val >= 0.97 && val < 1.0)
        {
            newColor = Color.black;
        }
        else
        {
            newColor = Color.white;
        }
        newColor.a = 0.2f;
        return newColor;


    }

    //Metoda za određivanje udaljenosti centroida do najbližeg glavnog čvora ceste
    public static double DistanceToWay(Vector3[] vertices, List<Node> wayNodes)
    {
        Vector3 centroid = getCentroid(vertices);
        double minDist = Double.MaxValue;
        foreach (Node n in wayNodes)
        {
            double dist = Distance3D(centroid, n.position);
            if (dist < minDist)
            {
                minDist = dist;
            }
        }
        return minDist;

    }

    //Metoda za dobivanje lokacije najbliže ceste
    public static Vector3 ClosestRoad(Vector3 objectLoc, List<Vector2> allRoadPoints, Terrain terrain)
    {
   
        double minDist = Double.MaxValue;
        Vector3 minDistVector = new Vector3(0f, 0f, 0f);
        foreach (Vector2 vec in allRoadPoints)
        {
            Vector3 testVector = new Vector3(vec.x, 0f, vec.y);
            float heightY = terrain.SampleHeight(testVector);
            testVector.y = heightY;
            double dist = Distance3D(objectLoc, testVector);
            if (dist < minDist)
            {
                minDist = dist;
                minDistVector = testVector;
            }
        }
        return minDistVector;

    }

    //Metoda za dobivanje lokacije najbližeg čvora ceste
    public static Node ClosestRoadNode(Vector3 objectLoc, List<Node> allRoadNodes)
    {

        double minDist = Double.MaxValue;
        Node minNode = null;
        foreach (Node testNode in allRoadNodes)
        {
            Vector3 testVector = testNode.position;
            double dist = Distance3D(objectLoc, testVector);
            if (dist < minDist)
            {
                minDist = dist;
                minNode = testNode;
            }
        }
        return minNode;

    }

    //Metoda za traženje ceste kojoj pripada čvor
    public static Way findWayWithNode(Node n,List<Way> roads)
    {
        foreach (Way road in roads)
        {
            foreach(long testId in road.refNodes)
            {
                if( testId == n.id)
                {
                    return road; 
                }
            }
          
        }
        return null;
     

    }

    //Metoda za dobivanje lokacije druge najbliže ceste
    public static Vector3 SecondClosestRoad(Vector3 objectLoc, List<Vector2> allRoadPoints,Vector2 closestPoint, Terrain terrain, double terrainScaleFactor)
    {

        double minDist = Double.MaxValue;
        Vector3 minDistVector = new Vector3(0f, 0f, 0f);
        while (true)
        {
            foreach (Vector2 vec in allRoadPoints)
            {
                Vector3 testVector = new Vector3(vec.x, 0f, vec.y);
                float heightY = terrain.SampleHeight(testVector);
                testVector.y = heightY;
                double dist = Distance3D(objectLoc, testVector);
                double distToClosest = Distance2D(closestPoint, vec);
                if (dist < minDist && distToClosest > 0.3 * terrainScaleFactor)
                {
                    minDist = dist;
                    minDistVector = testVector;
                }
            }
            return minDistVector;
        }
      

    }




    //Metoda za određivanje indexa zida na kojem ce biti vrata
    public static int getDoorIndex(int largestInd1, int largestInd2, int closestInd1, int closestInd2)
    {
        if (largestInd1 == closestInd1)
        {
            return largestInd1;
        }
        else if (closestInd1 == largestInd2)
        {
            return closestInd1;
        }
        else
        {
            return largestInd1;
        }

    }

    //Metoda koja nam daje do znanja da u zadanom radiusu vec postoji neki objekt
    public static bool hasBeenObjectInRange(GameObject obj, List<GameObject> objects, double radius)
    {
        foreach(GameObject testObj in objects)
        {
            double compareDist = Distance3D(testObj.transform.position, obj.transform.position);
            if(compareDist < radius)
            {
                return true;
            }
        }
        return false;

    }
    //Metoda koja nam daje do znanja da u zadanom radiusu vec postoji neki čvor
    public static bool hasBeenNodeInRange(Node node, List<Node> nodes, double radius)
    {
        foreach (Node testNode in nodes)
        {
            double compareDist = Distance3D(testNode.position, node.position);
            if (compareDist < radius)
            {
                return true;
            }
        }
        return false;

    }
    //Metoda koja nam daje do znanja da u zadanom radiusu vec postoji neki vektor
    public static bool hasBeenVectorInRange(Vector3 vec, List<Vector3> vectors, double radius)
    {
        foreach (Vector3 testVec in vectors)
        {
            double compareDist = Distance3D(vec, testVec);
            if (compareDist < radius)
            {
                return true;
            }
        }
        return false;

    }

    //Metoda koja vraća najveći pravokutnik koji se nalazi cijeli unutar poligona
    public static List<int> getMinBoundingBox(List<Vector2> polVertices, float terrainScaleFactor)
    {
        List<float> boundingBox = getBoundingBox2D(polVertices);
        float minX = boundingBox[0];
        float maxX = boundingBox[1];
        float minZ = boundingBox[2];
        float maxZ = boundingBox[3];

        float step = 10f;

        while (true)
        {

            if (isBoxInPolygon(polVertices, new List<float> { minX, maxX, minZ, maxZ }, step))
            {
                break;
            }

            List<float> boundingBox1 = new List<float> {minX, maxX, maxZ-1, maxZ };
            List<float> boundingBox2 = new List<float> { minX, maxX, minZ, minZ + 1 };
            List<float> boundingBox3 = new List<float> { minX, minX+1, minZ, maxZ };
            List<float> boundingBox4 = new List<float> { maxX-1, maxX, minZ, maxZ };

            float area1 = getPolygonAreaInBox(polVertices, boundingBox1, step);
            float area2 = getPolygonAreaInBox(polVertices, boundingBox2, step);
            float area3 = getPolygonAreaInBox(polVertices, boundingBox3, step);
            float area4 = getPolygonAreaInBox(polVertices, boundingBox4, step);

            List<float> areas = new List<float> { area1, area2, area3, area4 };
            float minArea = areas.Min();
            int index = areas.IndexOf(minArea);
           

            if (index == 0)
            {
                maxZ = maxZ - step;
            }
            else if(index == 1)
            {
                minZ = minZ + step;
            }
            else if (index == 2)
            {
                minX = minX + step;
            }
            else if (index == 3)
            {
                maxX = maxX - step;
            }

        }

        
        return new List<int> { Convert.ToInt32(minX), Convert.ToInt32(maxX), Convert.ToInt32(minZ), Convert.ToInt32(maxZ) };

    }

    //Metoda koja vraća površinu poligona u pravokutniku
    public static float getPolygonAreaInBox(List<Vector2> polVertices, List<float> boundingBox, float step)
    {
        float minX = boundingBox[0];
        float maxX = boundingBox[1];
        float minZ = boundingBox[2];
        float maxZ = boundingBox[3];
        float area = 0;

        for(float i = minX; i<=maxX; i=i+step)
        {
            for(float j = minZ; j<=maxZ; j=j+step)
            {
                Vector2 testPoint = new Vector2(i,j) ;
                if(PointInPolygon2D(polVertices, testPoint))
                {
                    area++;
                }
            }
        }
        return area;
    }

    //Metoda koja govori nalazi li se cijeli pravokutnik u poligonu
    public static bool isBoxInPolygon(List<Vector2> polVertices, List<float> boundingBox, float step)
    {
        float minX = boundingBox[0];
        float maxX = boundingBox[1];
        float minZ = boundingBox[2];
        float maxZ = boundingBox[3];
        bool flag = true;

        for (float i = minX; i <= maxX; i = i + step)
        {
            for (float j = minZ; j <= maxZ; j = j + step)
            {
                Vector2 testPoint = new Vector2(i, j);
                if (!PointInPolygon2D(polVertices, testPoint))
                {
                    flag = false;
                    break;
                }
            }
        }
        return flag;
    }



    //Metoda koja traži dva najudaljenija čvora u skupu čvorova
    public static List<Vector3> getFarthestVectors(List<Vector3> vectors)
    {
        List<Vector3> vectors2 = new List<Vector3>(vectors);
        var map = new Dictionary<List<Vector3>, double>();
        foreach(Vector3 vec1 in vectors)
        {
            foreach(Vector3 vec2 in vectors2)
            {            
                List<Vector3> pair = new List<Vector3>();
                pair.Add(vec1);
                pair.Add(vec2);
                double dist = Distance3D(vec1, vec2);
                map.Add(pair, dist);
            }
        }
        List<Vector3> maxPair = map.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;
        return maxPair;
    }
    


    //Metoda koja pronalazi rubne točke ceste
    public static List<Vector3> generateBoundPoints(List<Vector3> points, float width, double terrainScaleFactor, Terrain terrain, string roadType)
    {
        List<Vector3> allPoints = new List<Vector3>();
        List<Vector3> directions = new List<Vector3>();
        List<int> segmentNumber = new List<int>();
        HashSet<Vector3> marginal = new HashSet<Vector3>();

        //Izračunaj veličinu pojedinog segementa
        for (int i = 0; i < points.Count - 1; i = i + 1)
        {
            Vector3 currPos = points[i];
            Vector3 nextPos = points[i + 1];
            float dist = Vector3.Distance(currPos, nextPos);
            double realDist = dist / terrainScaleFactor;
            int size = 0;
            if (!Constants.roadsWithTrafficObjects.Contains(roadType))
            {
                float koef = width * 0.7f;
                size = (int)(realDist / koef);
            }
            else
            {
                size = (int)(realDist / 2);
            }
            
            if (size <= 0)
            {
                segmentNumber.Add(1);
            }
            else
            {

                segmentNumber.Add(size);
            }
        }

       

        //Stvori točke koje predstavljaju međutočke između glavnih točaka
        for (int i = 0; i < points.Count-1; i++)
        {

                int segNum = segmentNumber[i];
                Vector3 currPos = points[i];
                Vector3 nextPos = points[i + 1];
                Vector3 direction = nextPos - currPos;

                List<Vector2> midPoints = new List<Vector2>();
                midPoints = Lerp(points[i], points[i + 1], segNum);

                foreach (Vector2 p in midPoints)
                {
                    directions.Add(direction);
                    Vector3 terrPos = new Vector3(p.x, 0f, p.y);
                    terrPos.y = terrain.SampleHeight(terrPos);
                    allPoints.Add(terrPos);


                }
               
        }

        //Stvori točke koje predstavljaju rubove ceste
        List<Vector3> boundPoints = new List<Vector3>(); 

        for (int i = 0; i < allPoints.Count; ++i)
        {
            Vector3 dir = new Vector3();
            Vector3 currPos = new Vector3(allPoints[i].x, allPoints[i].y, allPoints[i].z);
            if (i < allPoints.Count - 1)
            {
                Vector3 nextPos = new Vector3(allPoints[i + 1].x, allPoints[i].y, allPoints[i + 1].z);
                dir = nextPos - currPos;
            }

            else
            {
                Vector3 prevPos = new Vector3(allPoints[i - 1].x, allPoints[i].y, allPoints[i - 1].z);
                dir = currPos - prevPos;
            }
            dir = directions[i];
            dir.Normalize();

            //Točka s lijevo
            Vector3 dirLeft = Quaternion.Euler(0, 90f, 0) * dir;
            dirLeft = currPos + (dirLeft * width/2);
            double radius = width / 2 * terrainScaleFactor;

            boundPoints.Add(dirLeft);
           

            //Točka s desno
            Vector3 dirRight = Quaternion.Euler(0, -90f, 0) * dir;
            dirRight = currPos + (dirRight * width/2);
            boundPoints.Add(dirRight);


        }
       
       
        return boundPoints;
    }

    public static Quaternion findDirectionAngle(Vector3[] vertices)
    {
        double maxLen = 0;
        Vector3 firstPoint = new Vector3();
        Vector3 secondPoint = new Vector3();
        for(int i = 0;i<vertices.Length; ++i)
        {
            double len = Distance3D(vertices[i], vertices[(i+1)%vertices.Length]);
            if(len > maxLen)
            {
                maxLen = len;
                firstPoint = vertices[i];
                secondPoint = vertices[(i + 1) % vertices.Length];
            }
        }

        Vector3 directionVector = secondPoint - firstPoint;
        return Quaternion.LookRotation(directionVector);
    }

    

}
