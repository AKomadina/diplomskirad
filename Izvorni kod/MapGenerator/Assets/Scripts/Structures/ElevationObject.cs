﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevationObject 
{
    public double latitude;
    public double longitude;
    public double elevation;

    public ElevationObject()
    {
       
    }

    public ElevationObject(double elevation, double latitude, double longitude)
    {
        this.elevation = elevation;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public override string ToString()
    {
        return "ElevationObj=> Elevation:" + elevation + " Lat:" + latitude + " Lon:" + longitude;
    }

    public double getElevation()
    {
        return this.elevation;
    }
}
