﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Vegetation
{
    public long id;
    public double coordinateX;
    public double coordinateY;
    public double longitude;
    public double latitude;

    public GameObject vegetationObject;
    public Vector3 position;


    public Vegetation(long id, double coordinateX, double coordinateY)
    {
        this.id = id;
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }

    public override string ToString()
    {
        return "Plant id:" + id + " Lat:" + latitude + " Lon:" + longitude + " Posx:" + position.x + " Posy:" + position.y + " Posz:" + position.z;
    }

    public void addObject(GameObject obj)
    {
        this.vegetationObject = obj;
    }

    public Vector3 getObjectPosition()
    {
        Transform objT = vegetationObject.transform;
        return objT.localPosition;
    }

    public void setPosition(Vector3 pos)
    {
        this.position = pos;
    }

    public void transformCoordinatesEPSG3857To4326()
    {
        float e = 2.7182818284f;
        float x = 20037508.34f;
        double longitude = (coordinateX * 180) / x;
        double latitude = (coordinateY / (x / 180));
        latitude = ((Math.Atan(Math.Pow(e, ((Math.PI / 180) * latitude)))) / (Math.PI / 360)) - 90;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    

}
