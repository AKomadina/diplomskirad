﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Way
{
    //Osnovno
    public List<long> refNodes;
    public long id;

    //Building, Road, ...
    public string type;

    //Za zgrade
    public string buildingType;
    public string buildingStreet;
    public string buildingHouseNumber;
    public string buildingLevels;
    public string buildingRoof;
    public string buildingHeight;
    public string buildingName;

    //Za puteve
    public string roadType;
    public string roadName;
    public string roadSurface;
    public string roadMaxSpeed;
    public string roadLanes;
    public bool roadOneWay = false;
    public bool roadBridge = false;
    public bool roadLit = false;

    //Za zeljeznice
    public string railType;
    public string railName;
    public float railGauge;
    public string railElectrified;
    public int railPassenger_lines;

    //Za vodene površine
    public string waterType;


    //Konstruktori
    public Way(long id)
    {
        this.id = id;
    }
    public Way()
    {

    }

    public Way(long id, List<long> refNodes)
    {
        this.id = id;
        this.refNodes = refNodes;
    }
    public Way(long id, List<long> refNodes, string type)
    {
        this.id = id;
        this.refNodes = refNodes;
        this.type = type;
    }

    //Setteri
    public void setType(string type)
    {
        this.type = type;
    }

    public void setBuildingType(string buildingType)
    {
        this.buildingType = buildingType;
    }
    public void setBuildingStreet(string buildingStreet)
    {
        this.buildingStreet = buildingStreet;
    }
    public void setBuildingHouseNumber(string buildingHouseNumber)
    {
        this.buildingHouseNumber = buildingHouseNumber;
    }
    public void setBuildingLevels(string buildingLevels)
    {
        this.buildingLevels = buildingLevels;
    }
    public void setBuildingHeight(string buildingHeight)
    {
        this.buildingHeight = buildingHeight;
    }
    public void setBuildingRoof(string buildingRoof)
    {
        this.buildingRoof = buildingRoof;
    }
    public void setBuildingName(string buildingName)
    {
        this.buildingName = buildingName;
    }


    public void setRoadType(string roadType)
    {
        this.roadType = roadType;
    }
    public void setRoadName(string roadName)
    {
        this.roadName = roadName;
    }

    public void setRoadSurface(string roadSurface)
    {
        this.roadSurface = roadSurface;
    }
    public void setRoadMaxSpeed(string roadMaxSpeed)
    {
        this.roadMaxSpeed = roadMaxSpeed;
    }
    public void setRoadLanes(string roadLanes)
    {
        this.roadLanes = roadLanes;
    }
    public void isOneWay()
    {
        this.roadOneWay = true;
    }

    public void isBridge()
    {
        this.roadBridge = true;
    }

    public void isLit()
    {
        this.roadLit = true;
    }

    public void setRailType(string railType)
    {
        this.railType = railType;
    }
    public void setRailName(string railName)
    {
        this.railName = railName;
    }
    public void setRailGauge(float gauge)
    {
        this.railGauge = gauge;
    }
    public void setRailElectrified(string electrified)
    {
        this.railElectrified = electrified;
    }
    public void setRailPassenger_lines(int passenger_lines)
    {
        this.railPassenger_lines = passenger_lines;
    }





    public void setWaterType(string waterType)
    {
        this.waterType = waterType;
    }

    public void addNodes(List<long> refNodes)
    {
        this.refNodes = refNodes;

    }

    
    public override string ToString()
    {
        /*
        string refs = "[";
        foreach(long r in refNodes)
        {
            refs = refs + r + ", ";
        }
        return "Way id: "+id+"\n{"+  this.name +","+ this.surface+","+this.type+"}\n" +" References: "+ refs + " ]" ;
        */
        if(this.type == "building")
        {
            return "Building id: " + this.id + ", type: " + this.buildingType + ", street: " + this.buildingStreet + ", houseNumber: " + this.buildingHouseNumber + ", height: " + this.buildingHeight + ", levels: " + this.buildingLevels + ", roof: " + this.buildingRoof;
        }
        else if(this.type == "road")
        {
            return "Road id: " + this.id + ", name: " + this.roadName + ", type: " + this.roadType + ", surface: " + this.roadSurface;
        }
        else
        {
            return "Unknown type";
        }

    }

    

}
