﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;

public class Node
{

    public long id;
    public double latitude;
    public double longitude;

    public double coordinateX;
    public double coordinateY;

    public GameObject nodeObject;
    public Vector3 position;
    public Vector2 terrainPosition;

    //Za javnu opremu, stabla i objekte na cesti
    public string type;

    //Za stabla
    public string treetopDiameter;
    public string trunkDiameter;
    public string height;

    public Node()
    {
        
    }

    public Node(long id)
    {
        this.id = id;
       
    }

    public Node(long id, double latitude, double longitude)
    {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public void setCoordinates(double cordX, double cordY)
    {
        this.coordinateX = cordX;
        this.coordinateY = cordY;
    }

    public override string ToString()
    {
        return "Node id:" + id +" Lat:"+latitude+" Lon:"+longitude+" Posx:"+position.x+ " Posy:" + position.y + " Posz:" + position.z + "Tip: "+type;
    }

    public void addObject(GameObject obj)
    {
        this.nodeObject = obj;
    }

    public Vector3 getObjectPosition()
    {
        Transform objT = nodeObject.transform;
        return objT.localPosition;
    }

    public void setPosition(Vector3 pos)
    {
        this.position = pos;
    }

   
    public void generateTerrainPosition(double scaleFactorX, double scaleFactorZ)
    {
        
        Vector2 terPos = new Vector2((float)(this.position.z * scaleFactorZ), (float)(this.position.x * scaleFactorX ));
        this.terrainPosition = terPos;
    }

    public void setTerrainPosition(Vector2 position)
    {
        this.terrainPosition = position;
    }

    public void setType(string type)
    {
        this.type = type;
    }

    public void setPlantDetails(string type, string height, string trunkDiameter, string treetopDiameter)
    {
        this.type = type;
        this.treetopDiameter = treetopDiameter;
        this.trunkDiameter = trunkDiameter;
        this.height = height;

    }


    public void transformCoordinatesEPSG3857To4326()
    {
        float e = 2.7182818284f;
        float x = 20037508.34f;
        double longitude = (coordinateX * 180) / x;
        double latitude = (coordinateY / (x / 180));
        latitude = ((Math.Atan(Math.Pow(e, ((Math.PI / 180) * latitude)))) / (Math.PI / 360)) - 90;
        this.latitude = latitude;
        this.longitude = longitude;
    }


}
