﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pair
{
    public Pair()
    {
    }

    public Pair(double first, double second)
    {
        this.First = first;
        this.Second = second;
    }

    public double First { get; set; }
    public double Second { get; set; }

    public override string ToString()
    {
        return First + " " + Second ;
    }

};