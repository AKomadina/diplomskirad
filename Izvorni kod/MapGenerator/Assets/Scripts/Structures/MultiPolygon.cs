﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;



public class MultiPolygon
{
    public long id;
    public List<Pair> coordinates = new List<Pair>();
    public List<Vector3> positions = new List<Vector3>();
    public List<long> refNodes;

    public string type;

    public MultiPolygon()
    {

    }

    public MultiPolygon(long id)
    {
        this.id = id;

    }

    public MultiPolygon(long id, List<Pair> coordinates)
    {
        this.id = id;
        this.coordinates = coordinates;

    }

    public MultiPolygon(long id, List<long> refNodes)
    {
        this.id = id;
        this.refNodes = refNodes;
    }
    public MultiPolygon(long id, List<long> refNodes, string type)
    {
        this.id = id;
        this.refNodes = refNodes;
        this.type = type;
    }


    public override string ToString()
    {
        return "Multipolygon id:" + id + " "+type;
    }


    public void setPosition(List<Vector3> positions)
    {
        this.positions = positions;
    }

    public void transformCoordinatesEPSG3857To4326()
    {
        float e = 2.7182818284f;
        float x = 20037508.34f;
        List<Pair> newCoordinates = new List<Pair>();
        foreach (Pair coord in coordinates)
        {
            double longitude = (coord.First * 180) / x;
            double latitude = (coord.Second / (x / 180));
            latitude = ((Math.Atan(Math.Pow(e, ((Math.PI / 180) * latitude)))) / (Math.PI / 360)) - 90;
            Pair newCoord = new Pair(latitude, longitude);         
            newCoordinates.Add(newCoord);
        }
        coordinates = newCoordinates;
        
       
    }

}
